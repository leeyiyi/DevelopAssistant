﻿ 
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Gui.CompletionWindow;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Service.TSQLIntellisense
{
    /// <summary>
    /// Data provider for code completion.
    /// </summary>
    public class TSQLCompletionDataProvider : ICompletionDataProvider 
    {
        private string contentText = String.Empty;
        private string lastwordText = String.Empty;
        private string dataBaseType = String.Empty;
        private string uncompletedText = String.Empty;       

        private ImageList imageList;

        public TSQLCompletionDataProvider(ImageList imageList)
        {
            this.imageList = imageList;
        }

        public TSQLCompletionDataProvider(string dataBaseType, ImageList imageList)
            : this(imageList)
        {
            this.dataBaseType = dataBaseType;
        }

        public ImageList ImageList
        {
            get
            {
                return imageList;
            }
        }

        public string PreSelection
        {
            get
            {
                return null;
            }
        }

        public int DefaultIndex
        {
            get
            {
                return -1;
            }
        }

        public CompletionDataProviderKeyResult ProcessKey(char key)
        {            
            if (char.IsLetterOrDigit(key) || key == '_')
            {
                return CompletionDataProviderKeyResult.NormalKey;
            }
            return CompletionDataProviderKeyResult.InsertionKey;
        }

        public CompletionDataProviderKeyResult ProcessKey(TextEditorControl editor, char key)
        {            
            if (char.IsLetterOrDigit(key) || key == '_')
            {
                return CompletionDataProviderKeyResult.NormalKey;
            }
            return CompletionDataProviderKeyResult.InsertionKey;
        }

        /// <summary>
        /// Called when entry should be inserted. Forward to the insertion action of the completion data.
        /// </summary>
        public bool InsertAction(ICompletionData data, TextArea textArea, int insertionOffset, char key)
        {           
            textArea.Caret.Position = textArea.Document.OffsetToPosition(
                Math.Min(insertionOffset, textArea.Document.TextLength)
                );
            return data.InsertAction(textArea, key);
        }

        public ICompletionData[] GenerateCompletionData(string fileName, TextArea textArea, char charTyped)
        {
            #region 不再使用
            //ArrayList CompletionData = new ArrayList();
            //return (ICompletionData[])CompletionData.ToArray(typeof(ICompletionData));
            #endregion

            contentText = textArea.Document.TextContent;
            lastwordText = contentText.Substring(0, textArea.Caret.Offset) + charTyped;
            return Utility.GetItems(lastwordText, SqlPromptType.Keyword);

        }
    }
}

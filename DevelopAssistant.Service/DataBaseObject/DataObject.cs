﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class DataObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Alias { get; set; }       
       
    }

    public class DataObjectEnumerable : DataObject
    {
        public List<Column> Columns { get; set; }
    }
}

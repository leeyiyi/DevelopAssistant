﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class Procedure
    {
        public string Name { get; set; }
        public string Describtion { get; set; }
        public List<SqlParameter> Parameters { get; set; }
    }
}

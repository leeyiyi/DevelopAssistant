﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class Keyword : DataObject
    {
        public string Key { get; set; }
        public string Text { get; set; }     
        public string DataBaseType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace DevelopAssistant.Service
{
    [Serializable]
    public class SqlParameter
    {
        public string Name { get; set; }

        public string DataType { get; set; }

        public ParameterDirection Direction { get; set; }

        public bool Nullable { get; set; }

        public int Size { get; set; }

        public byte Precision { get; set; }

        public byte Scale { get; set; }

        public SqlParameter()
        {
        }

        public SqlParameter(DbParameter dbParm)
        {
            this.Name = dbParm.ParameterName;
            this.Size = dbParm.Size;
            this.Nullable = dbParm.IsNullable;
            this.Direction = dbParm.Direction;
        }
    }
}

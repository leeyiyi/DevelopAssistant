﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopAssistant.Service
{
    [Serializable]
    public class Column : DataObject
    {
        public bool Pk_Identify { get; set; }       
        public string DataType { get; set; }
        public string Len { get; set; }
        public bool Auto_Identify { get; set; }
        public string IsNull { get; set; }
        public object Value { get; set; }       

    }
}

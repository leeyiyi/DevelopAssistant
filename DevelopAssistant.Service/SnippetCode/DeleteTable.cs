﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class DeleteTable : SnippetBase
    {
        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer)
        {
            StringPlus sp = new StringPlus();

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                switch (DatabaseServer.ProviderName)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":
                        sp.Append("DELETE FROM "+ getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        sp.Append("--TRUNCATE  TABLE  " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                    case "System.Data.Mysql":
                        sp.Append("TRUNCATE  TABLE  " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                    default:
                        sp.Append("DELETE FROM " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                }
                sp.Append("GO");
            }
            else
            {                
                switch (DatabaseServer.ProviderName)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":
                        sp.Append("truncate  table  " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                    case "System.Data.Mysql":
                        sp.Append("truncate  table  " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                    default:
                        sp.Append("delete from " + getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
                        break;
                }
                sp.Append("go");
            }

            return sp.Value;
        }
    }
}

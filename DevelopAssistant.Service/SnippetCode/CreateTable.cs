﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DevelopAssistant.Service
{
    public class CreateTable : SnippetBase
    {
        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer)
        {
            StringPlus sp = new StringPlus();

            if(AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("CREATE TABLE ");
            }
            else
            {
                sp.Append("create table ");
            }
            sp.Append("" + getObject(TableName,DatabaseServer.ProviderName) + "");
            sp.Append(" (");

            List<string> pks = new List<string>();

            using (var db = Utility.GetAdohelper(DatabaseServer))
            {
                DataTable dt = db.GetTableObject(TableName);

                for (int i = 0, len = dt.Rows.Count; i < len; i++)
                {                    
                    string column_name = dt.Rows[i]["ColumnName"] + "";
                    string column_type = dt.Rows[i]["TypeName"] + "";
                    string column_cisnull = dt.Rows[i]["CisNull"] + "";
                    string column_len = dt.Rows[i]["Length"] + "";
                    string column_pesc = "";
                    string column_isnull = "";
                    string column_description = dt.Rows[i]["Describ"] + "";

                    column_name = getObject(column_name, DatabaseServer.ProviderName);
                    column_isnull = column_cisnull.Replace("pk", "")
                        .Replace("identity", "")
                        .Replace(",","").Trim();

                    //主键
                    if (column_cisnull.Contains("pk"))
                    {
                        pks.Add(column_name);

                        if (DatabaseServer.ProviderName == "System.Data.Sqlite")
                        {
                            if (AppSettings.EditorSettings.KeywordsCase)
                            {
                                column_pesc = "PRIMARY KEY AUTOINCREMENT";
                            }
                            else
                            {
                                column_pesc = "primark key autoincrement";
                            }
                        }

                    }
                    if (column_cisnull.Contains("identity"))
                    {
                        column_type = column_type + " " + "identity(1,1) ";
                    }                   

                    column_type = getObjectType(column_type, column_len, DatabaseServer.ProviderName);

                    sp.Append("\r\n  " + column_name + " " + column_type + " " + column_isnull + " " + column_pesc + ",");

                    if (!string.IsNullOrEmpty(column_description))
                    {
                        sp.Append(" --" + column_description + "");
                    }

                }

                if (pks.Count > 0)
                {
                    for (int j = 0; j < pks.Count; j++)
                    {
                        string pk = pks[j];
                        sp.Append(constraintSnippet(DatabaseServer.ProviderName, TableName, pk, j));
                    }
                    string value = sp.Value.TrimEnd(',');
                    if (pks.Count < 2)
                    {
                        value = value.Replace("PK_" + TableName + "1", "PK_" + TableName + "");
                    }
                    sp = new StringPlus();
                    sp.Append(value);
                }
                else
                {
                    string value = sp.Value.TrimEnd(',') + "";
                    sp = new StringPlus();
                    sp.Append(value);
                }

            }

            sp.Append("\r\n ");

            if (pks.Count > 0)
            {
                sp.Append(withSnippet(DatabaseServer.ProviderName));
            }
            else
            {
                sp.Append(")");
            }

            return sp.Value;
        }

        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer,DataTable Datatable)
        {
            StringPlus sp = new StringPlus();

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("CREATE TABLE ");
            }
            else
            {
                sp.Append("create table ");
            }
            sp.Append("" + getObject(TableName, DatabaseServer.ProviderName) + "");
            sp.Append(" (");

            List<string> pks = new List<string>();

            for (int i = 0, len = Datatable.Rows.Count; i < len; i++)
            {
                string column_name = Datatable.Rows[i]["ColumnName"] + "";
                string column_type = Datatable.Rows[i]["TypeName"] + "";
                string column_cisnull = Datatable.Rows[i]["CisNull"] + "";
                string column_len = Datatable.Rows[i]["Length"] + "";
                string column_pesc = "";
                string column_isnull = "";

                column_name = getObject(column_name, DatabaseServer.ProviderName);
                column_isnull = column_cisnull.Replace("pk", "")
                    .Replace("identity", "")
                    .Replace(",", "").Trim();

                //主键
                if (column_cisnull.Contains("pk"))
                {
                    pks.Add(column_name);

                    if (DatabaseServer.ProviderName == "System.Data.Sqlite")
                    {
                        if (AppSettings.EditorSettings.KeywordsCase)
                        {
                            column_pesc = "PRIMARY KEY AUTOINCREMENT";
                        }
                        else
                        {
                            column_pesc = "primark key autoincrement";
                        }
                    }

                }
                if (column_cisnull.Contains("identity"))
                {
                    column_type = column_type + " " + "identity(1,1) ";
                }

                column_type = getObjectType(column_type, column_len, DatabaseServer.ProviderName);

                sp.Append("\r\n  " + column_name + " " + column_type + " " + column_isnull + " " + column_pesc + ",");

            }

            if (pks.Count > 0)
            {
                for (int j = 0; j < pks.Count; j++)
                {
                    string pk = pks[j];
                    sp.Append(constraintSnippet(DatabaseServer.ProviderName, TableName, pk, j));
                }
                string value = sp.Value.TrimEnd(',').Replace("pk_" + TableName + "1", "pk_" + TableName + "");
                sp = new StringPlus();
                sp.Append(value);
            }
            else
            {
                string value = sp.Value.TrimEnd(',') + "";
                sp = new StringPlus();
                sp.Append(value);
            }

            sp.Append("\r\n ");

            if (pks.Count > 0)
            {
                sp.Append(withSnippet(DatabaseServer.ProviderName));
            }
            else
            {
                sp.Append(")");
            }

            return sp.Value;
        }
    }
}

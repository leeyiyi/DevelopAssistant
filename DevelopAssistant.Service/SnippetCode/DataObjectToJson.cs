﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text; 

namespace DevelopAssistant.Service
{
    public class DataObjectToJson : SnippetBase
    {
        public static string Json(string TableName, object DataSource, string dateFormat, DataBaseServer DatabaseServer, System.Windows.Forms.ProgressBar ProgressBar = null)
        {
            StringBuilder jsonString = new StringBuilder();
            jsonString.Append("[").Append(System.Environment.NewLine);

            DataTable dataTable = (DataTable)DataSource;

            int rowIndex = 0;
            foreach (System.Data.DataRow row in dataTable.Rows)
            {              
                if (rowIndex == 0)
                {
                    jsonString.Append("{");
                    int colIndex = 0;
                    foreach (System.Data.DataColumn col in dataTable.Columns)
                    {
                        string value = row[col] + "";
                        string type = col.DataType.Name.ToLower();
                        if (value != null)
                        {
                            switch (type)
                            {                                 
                                case "time": 
                                case "guid":
                                case "string":
                                    if((value.Trim().StartsWith("{") && value.Trim().EndsWith("}"))||
                                        (value.Trim().StartsWith("[") && value.Trim().EndsWith("]")))
                                        value = "" + value + "";
                                    else
                                        value = "\"" + value + "\"";
                                    break;
                                case "date":
                                case "datetime":
                                    if (!string.IsNullOrEmpty(value))
                                        value = Convert.ToDateTime(value).ToString(dateFormat);
                                    value = "\"" + value + "\"";
                                    break;
                                case "byte":
                                case "byte[]":
                                case "image":
                                case "binary":
                                    value = "\"二进制流\"";
                                    break;                             
                                case "bool":
                                case "boolean":
                                    value = value.ToLower();
                                    break;
                            }
                        }
                        else
                        {
                            value = "null";
                        }

                        if (colIndex == 0)
                        {
                            jsonString.Append("\"" + col.ColumnName + "\":" + value + "");
                        }
                        else
                        {
                            jsonString.Append(",\"" + col.ColumnName + "\":" + value + "");
                        }

                        colIndex++;
                    }
                    jsonString.Append("}").Append(System.Environment.NewLine);
                }
                else
                {
                    jsonString.Append(",{");
                    int colIndex = 0;
                    foreach (System.Data.DataColumn col in dataTable.Columns)
                    {
                        string value = row[col] + "";
                        string type = col.DataType.Name.ToLower();
                        if (value != null)
                        {
                            switch (type)
                            {
                                case "time":
                                case "guid":
                                case "string":
                                    if ((value.Trim().StartsWith("{") && value.Trim().EndsWith("}")) ||
                                        (value.Trim().StartsWith("[") && value.Trim().EndsWith("]")))
                                        value = "" + value + "";
                                    else
                                        value = "\"" + value + "\"";
                                    break;
                                case "date":
                                case "datetime":
                                    if (!string.IsNullOrEmpty(value))
                                        value = Convert.ToDateTime(value).ToString(dateFormat);
                                    value = "\"" + value + "\"";
                                    break;
                                case "byte":
                                case "byte[]":
                                case "image":
                                case "binary":
                                    value = "\"二进制流\"";
                                    break;
                                case "bool":
                                case "boolean":
                                    value = value.ToLower();
                                    break;
                            }
                        }
                        else
                        {
                            value = "null";
                        }

                        if (colIndex == 0)
                        {
                            jsonString.Append("\"" + col.ColumnName + "\":" + value + "");
                        }
                        else
                        {
                            jsonString.Append(",\"" + col.ColumnName + "\":" + value + "");
                        }

                        colIndex++;
                    }
                    jsonString.Append("}").Append(System.Environment.NewLine);
                }
                rowIndex++;

                var ProgressValue = (int)(rowIndex * 1.0 / dataTable.Rows.Count * 100);
                ProgressBar.Invoke(new System.Windows.Forms.MethodInvoker(() => { ProgressBar.Value = ProgressValue; }));
                ProgressBar.Value = ProgressValue;
                System.Threading.Thread.Sleep(50);

            }

            jsonString.Append("]");

            return jsonString.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevelopAssistant.Service
{
    public class SnippetBase
    {
        public static string GetColumnName(object objectName, string ProviderName)
        {
            return getObject(objectName,ProviderName);
        }

        public static string GetTableName(object objectName, string ProviderName)
        {
            return getObject(objectName, ProviderName);
        }

        protected static string getObject(object objectName, string ProviderName)
        {
            if (AppSettings.EditorSettings.AutoSupplementary)
            {
                switch (ProviderName)
                {
                    case "System.Data.SQL":
                    case "System.Data.Sql": objectName = "[" + objectName + "]"; break;
                    case "System.Data.Sqlite": objectName = "[" + objectName + "]"; break;
                    case "System.Data.OleDb": objectName = "[" + objectName + "]"; break;
                    case "System.Data.Postgresql":
                    case "System.Data.PostgreSql": objectName = "\"" + objectName + "\""; break;
                    case "System.Data.MySql": objectName = "`" + objectName + "`"; break;
                    case "System.Data.Oracle": objectName = "[" + objectName + "]"; break;
                    default: break;
                }
            }
            return "" + objectName + "";
        }

        protected static string getObjectType(object typeName,string len, string ProviderName)
        {            
            switch (ProviderName)
            {
                case "System.Data.SQL":
                case "System.Data.Sql": typeName = getSqlDataType(typeName,len); break;
                case "System.Data.Sqlite": typeName = getSqlDataType(typeName, len); break;
                case "System.Data.OleDb": typeName = getSqlDataType(typeName, len); break;
                case "System.Data.Postgresql":
                case "System.Data.PostgreSql": typeName = getPostgreSqlType(typeName, len); break;
                case "System.Data.MySql": typeName = getSqlDataType(typeName, len); break;
                case "System.Data.Oracle": typeName = getSqlDataType(typeName, len); break;
                default: break;
            }
            return typeName + "";

        }

        public static string getObjectValue(object value, string dateFormat, string ProviderName)
        {
            string rvl = "NULL";
            if (value != null && !DBNull.Value.Equals(value))
            {
                string valueType = value.GetType().Name;
                switch (valueType.ToLower())
                {
                    case "string": rvl = "'" + value + "'"; break;
                    case "datetime":
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            rvl= "'" + Convert.ToDateTime(value).ToString(dateFormat) + "'";
                        }
                        else
                        {
                            rvl = "'" + value + "'";
                        }                        
                        break;
                    case "image": rvl = "'" + value + "'"; break;
                    case "char": rvl = "'" + value + "'"; break;
                    case "int": rvl = "'" + value + "'"; break;
                    case "decimal": rvl = "'" + value + "'"; break;
                    case "double": rvl = "'" + value + "'"; break;
                    case "boolean": rvl = "'" + value + "'"; break;
                    case "guid": rvl = "'" + value + "'"; break;
                    case "byte": rvl = "'" + value + "'"; break;
                    case "byte[]": rvl = "'" + value + "'"; break; 
                    default: rvl = value.ToString(); break;
                }
            }
            return rvl;
        }

        private static string getSqlDataType(object typeName, string len)
        {
            string rvl = string.Empty;

            if (!string.IsNullOrEmpty(len))
            {
                if ((typeName + "").ToUpper().StartsWith("INT"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("TEXT"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("DATE"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("IMAGE"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("DATETIME"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("UNIQUEIDENTIFIER"))
                    rvl = typeName + "";
                else if ((typeName + "").ToUpper().StartsWith("BIT"))
                    rvl = typeName + "";
                else
                    rvl = typeName + "(" + len + ")";
            }
            else
            {
                rvl = typeName+"";
            }

            return rvl;
        }

        private static string getPostgreSqlType(object typeName, string len)
        {
            string rvl = string.Empty;

            if (!string.IsNullOrEmpty(len))
            {
                if (len != "0")
                    rvl = typeName + "(" + len + ")";
                else
                    rvl = typeName + "";
            }
            else
            {
                rvl = typeName + "";
            }

            return rvl;
        }

        protected static string withSnippet(string ProviderName)
        {
            string rvl = string.Empty;

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                switch (ProviderName)
                {
                    case "System.Data.SQL":
                    case "System.Data.Sql":
                        rvl = " WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON ) ON [PRIMARY] ";
                        rvl += "\r\n) ON [PRIMARY]";
                        break;
                    case "System.Data.Sqlite":
                        rvl = ")";
                        break;
                    case "System.Data.OleDb":; break;
                    case "System.Data.Postgresql":
                    case "System.Data.PostgreSql":
                        rvl = "WITH (\r\n OIDS=FALSE\r\n);";
                        break;
                    case "System.Data.MySql":
                        rvl = ") DEFAULT CHARSET=utf8;";
                        break;
                    case "System.Data.Oracle": rvl = ")"; break;
                    default: break;
                }
            }
            else
            {
                switch (ProviderName)
                {
                    case "System.Data.SQL":
                    case "System.Data.Sql":
                        rvl = "with ( pad_index  = off, \r\n    statistics_norecompute  = off, \r\n    ignore_dup_key = off, \r\n    allow_row_locks  = on, \r\n    allow_page_locks  = on ) \r\non [primary] ";
                        rvl += "\r\n) on [primary]";
                        break;
                    case "System.Data.Sqlite":
                        rvl = ")";
                        break;
                    case "System.Data.OleDb":; break;
                    case "System.Data.Postgresql":
                    case "System.Data.PostgreSql":
                        rvl = "with (\r\n oids=false\r\n);";
                        break;
                    case "System.Data.MySql":
                        rvl = ") default charset=utf8;";
                        break;
                    case "System.Data.Oracle": rvl = ")"; break;
                    default: break;
                }
            }

            return rvl;
        }

        protected static string constraintSnippet(string ProviderName, string TableName, string PrimaryKey,int Index)
        {
            string rvl = string.Empty;

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                switch (ProviderName)
                {
                    case "System.Data.SQL":
                    case "System.Data.Sql":
                        rvl = "\r\n  " + "CONSTRAINT [PK_" + TableName + "" + (Index + 1) + "] PRIMARY KEY CLUSTERED (" + PrimaryKey + " ASC ),";
                        break;
                    case "System.Data.Sqlite":
                        break;
                    case "System.Data.OleDb":; break;
                    case "System.Data.Postgresql":
                    case "System.Data.PostgreSql":
                        rvl = "\r\n  " + "CONSTRAINT \"PK_" + TableName + "" + (Index + 1) + "\" PRIMARY KEY (" + PrimaryKey + ")\r\n)";
                        break;
                    case "System.Data.MySql":
                        rvl = "\r\n  " + "PRIMARY KEY (" + PrimaryKey + ")";
                        break;
                    case "System.Data.Oracle":; break;
                    default: break;
                }
            }
            else
            {
                switch (ProviderName)
                {
                    case "System.Data.SQL":
                    case "System.Data.Sql":
                        rvl = "\r\n  " + "constraint [PK_" + TableName + "" + (Index + 1) + "] primary key clustered (" + PrimaryKey + " asc ),";
                        break;
                    case "System.Data.Sqlite":
                        break;
                    case "System.Data.OleDb":; break;
                    case "System.Data.Postgresql":
                    case "System.Data.PostgreSql":
                        rvl = "\r\n  " + "constraint \"PK_" + TableName + "" + (Index + 1) + "\" primary key (" + PrimaryKey + ")\r\n)";
                        break;
                    case "System.Data.MySql":
                        rvl = "\r\n  " + "primary key (" + PrimaryKey + ")";
                        break;
                    case "System.Data.Oracle":; break;
                    default: break;
                }
            }

            return rvl;
        }

        public static string getDataBaseDataType(string typeName, string len,string providerName)
        {
            return getObjectType(typeName,len,providerName);
        }
    }
}

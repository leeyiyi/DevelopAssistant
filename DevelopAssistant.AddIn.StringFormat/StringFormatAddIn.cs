﻿using DevelopAssistant.AddIn;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopAssistant.AddIn.StringFormat
{
    using DevelopAssistant.AddIn.StringFormat.Properties;
    public class StringFormatAddIn : DockContentAddIn
    {
        public StringFormatAddIn()
        {
            this.IdentityID = "c32c8a2e-a283-463b-a972-16246452c985";
            this.Name = "文本格式化";
            this.Text = "文本格式化";
            this.Tooltip = "文本格式化";
            this.Icon = Resources.plugin;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm(Parameter[2].ToString(), Parameter[3].ToString());
            return f;
        }

    }
}

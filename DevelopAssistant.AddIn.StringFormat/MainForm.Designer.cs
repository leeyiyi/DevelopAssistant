﻿namespace DevelopAssistant.AddIn.StringFormat
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.InputText = new ICSharpCode.TextEditor.TextEditorControl();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolButtonCompress = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolButtonFormat = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolButtonCombox = new System.Windows.Forms.ToolStripComboBox();
            this.groupBox2 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.OutText = new ICSharpCode.TextEditor.TextEditorControl();
            this.toolStrip2 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolButtonSave = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelTotalChars = new System.Windows.Forms.ToolStripLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.toolStrip1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 20, 4, 6);
            this.groupBox1.Size = new System.Drawing.Size(885, 272);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.InputText);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 20);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 2);
            this.panel1.Size = new System.Drawing.Size(877, 218);
            this.panel1.TabIndex = 2;
            // 
            // InputText
            // 
            this.InputText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.InputText.BookMarkEnableToggle = true;
            this.InputText.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.InputText.ComparisonState = false;
            this.InputText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InputText.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InputText.IsReadOnly = false;
            this.InputText.Location = new System.Drawing.Point(6, 6);
            this.InputText.Name = "InputText";
            this.InputText.ShowGuidelines = false;
            this.InputText.Size = new System.Drawing.Size(865, 210);
            this.InputText.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonCompress,
            this.toolButtonFormat,
            this.toolStripLabel1,
            this.toolButtonCombox});
            this.toolStrip1.Location = new System.Drawing.Point(4, 238);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(877, 28);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolButtonCompress
            // 
            this.toolButtonCompress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonCompress.Image = global::DevelopAssistant.AddIn.StringFormat.Properties.Resources.controlPanel;
            this.toolButtonCompress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonCompress.Name = "toolButtonCompress";
            this.toolButtonCompress.Size = new System.Drawing.Size(76, 25);
            this.toolButtonCompress.Text = "压缩文本";
            this.toolButtonCompress.Click += new System.EventHandler(this.toolButtonCompress_Click);
            // 
            // toolButtonFormat
            // 
            this.toolButtonFormat.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonFormat.Image = global::DevelopAssistant.AddIn.StringFormat.Properties.Resources.document_editing;
            this.toolButtonFormat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonFormat.Name = "toolButtonFormat";
            this.toolButtonFormat.Size = new System.Drawing.Size(88, 25);
            this.toolButtonFormat.Text = "格式化文本";
            this.toolButtonFormat.Click += new System.EventHandler(this.toolButtonFormat_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 25);
            this.toolStripLabel1.Text = "选择:";
            // 
            // toolButtonCombox
            // 
            this.toolButtonCombox.BackColor = System.Drawing.SystemColors.Window;
            this.toolButtonCombox.Items.AddRange(new object[] {
            "SQL脚本",
            "Javascript",
            "JSON",
            "CSS"});
            this.toolButtonCombox.Name = "toolButtonCombox";
            this.toolButtonCombox.Size = new System.Drawing.Size(121, 28);
            this.toolButtonCombox.SelectedIndexChanged += new System.EventHandler(this.toolButtonCombox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox2.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(6, 282);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.groupBox2.MarginWidth = 0;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 20, 4, 6);
            this.groupBox2.Size = new System.Drawing.Size(885, 312);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Title = "";
            this.groupBox2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.OutText);
            this.panel2.Controls.Add(this.toolStrip2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 20);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(6, 6, 6, 0);
            this.panel2.Size = new System.Drawing.Size(877, 286);
            this.panel2.TabIndex = 3;
            // 
            // OutText
            // 
            this.OutText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.OutText.BookMarkEnableToggle = true;
            this.OutText.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.OutText.ComparisonState = false;
            this.OutText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutText.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OutText.IsReadOnly = false;
            this.OutText.Location = new System.Drawing.Point(6, 6);
            this.OutText.Name = "OutText";
            this.OutText.ShowGuidelines = false;
            this.OutText.Size = new System.Drawing.Size(865, 252);
            this.OutText.TabIndex = 1;
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStrip2.CanOverflow = false;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonSave,
            this.toolStripLabel2,
            this.toolStripLabelTotalChars});
            this.toolStrip2.Location = new System.Drawing.Point(6, 258);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.ShowItemToolTips = false;
            this.toolStrip2.Size = new System.Drawing.Size(865, 28);
            this.toolStrip2.Stretch = true;
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolButtonSave
            // 
            this.toolButtonSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonSave.Image = global::DevelopAssistant.AddIn.StringFormat.Properties.Resources.save;
            this.toolButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonSave.Name = "toolButtonSave";
            this.toolButtonSave.Size = new System.Drawing.Size(88, 25);
            this.toolButtonSave.Text = "保存到本文";
            this.toolButtonSave.Click += new System.EventHandler(this.toolButtonSave_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(35, 25);
            this.toolStripLabel2.Text = "信息:";
            // 
            // toolStripLabelTotalChars
            // 
            this.toolStripLabelTotalChars.Name = "toolStripLabelTotalChars";
            this.toolStripLabelTotalChars.Size = new System.Drawing.Size(83, 25);
            this.toolStripLabelTotalChars.Text = "共计 0 个字符";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.Control;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(6, 278);
            this.splitter1.Margin = new System.Windows.Forms.Padding(0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(885, 4);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 600);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox2;
        private System.Windows.Forms.Splitter splitter1;
        private ICSharpCode.TextEditor.TextEditorControl InputText;
        private ICSharpCode.TextEditor.TextEditorControl OutText;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonFormat;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox toolButtonCombox;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonSave;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabelTotalChars;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonCompress;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}
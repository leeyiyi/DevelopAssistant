﻿using DevelopAssistant.Service;
using ICSharpCode.TextEditor.Document;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.StringFormat
{
    public partial class MainForm : DockContent
    {
        private string _windowTheme;
        private string _editorTheme;

        public MainForm(string windowTheme,string editorTheme)
        {
            InitializeComponent();
           
            _windowTheme = windowTheme;
            _editorTheme = editorTheme;

            InitializeControls(windowTheme, editorTheme);
        }

        private void InitializeControls(string windowTheme, string editorTheme)
        {
            this.toolButtonCombox.Items.Clear();
            HighlightingItem item = new HighlightingItem();
            item.Text = "SQL脚本";
            item.Value = "TSQL";
            this.toolButtonCombox.Items.Add(item);
            item = new HighlightingItem();
            item.Text = "Javascript";
            item.Value = "JavaScript";
            this.toolButtonCombox.Items.Add(item);
            item = new HighlightingItem();
            item.Text = "JSON";
            item.Value = "JavaScript";
            this.toolButtonCombox.Items.Add(item);
            item = new HighlightingItem();
            item.Text = "CSS样式";
            item.Value = "CSS";
            this.toolButtonCombox.Items.Add(item);
            item = new HighlightingItem();
            item.Text = "XML文件";
            item.Value = "XML";
            this.toolButtonCombox.Items.Add(item);

            this.toolButtonCombox.SelectedIndex = 0;
            //var font = this.OutText.Font;

            Color foreColor = SystemColors.ControlText;
            Color backColor = SystemColors.Control;

            switch (editorTheme) {

                case "Default":
                    InputText.LineViewerStyle = LineViewerStyle.None;                    
                    InputText.BackgroundImage = null;
                    OutText.LineViewerStyle = LineViewerStyle.None;                    
                    OutText.BackgroundImage = null;
                    break;
                case "DefaultAndBackImage":
                    InputText.LineViewerStyle = LineViewerStyle.None;                  
                    InputText.BackgroundImage = null;
                    OutText.LineViewerStyle = LineViewerStyle.None;                  
                    OutText.BackgroundImage = null;
                    break;
                case "Black":
                    InputText.LineViewerStyle = LineViewerStyle.FullRow;                    
                    InputText.BackgroundImage = null;
                    OutText.LineViewerStyle = LineViewerStyle.FullRow;                  
                    OutText.BackgroundImage = null;

                    foreColor = Color.FromArgb(240, 240, 248);// Color.White;
                    backColor = Color.FromArgb(255, 050, 050, 050);           

                    break;
                case "BlackAndBackImage":
                    InputText.LineViewerStyle = LineViewerStyle.None;                  
                    InputText.BackgroundImage = null;
                    OutText.LineViewerStyle = LineViewerStyle.FullRow;                   
                    OutText.BackgroundImage = null;
                    break;
            }

            InputText.FoldMarkStyle = 0;
            InputText.ShowVRuler = false;
            InputText.Font = new Font("Courier New", 12);
            InputText.FoldMarkStyle = 0;
            OutText.ShowVRuler = false;
            OutText.Font = new Font("Courier New", 12);

            BackColor = backColor;
            groupBox2.BackColor = backColor;
            toolStrip1.BackColor = backColor;
            toolStrip2.BackColor = backColor;
            toolButtonCombox.BackColor = backColor;
            toolButtonCombox.ForeColor = foreColor;
            groupBox1.ForeColor = foreColor;
            groupBox2.ForeColor = foreColor;

            this.BackColor = backColor;
            this.splitter1.BackColor = backColor;

            this.toolStrip1.RenderMode = ToolStripRenderMode.Professional;
            this.toolStrip2.RenderMode = ToolStripRenderMode.Professional;
        }

        private void toolButtonFormat_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(InputText.Text))
            {
                string Reulst = String.Empty;
                SetOutText(Reulst);

                if (string.IsNullOrEmpty(toolButtonCombox.Text))
                {
                    Reulst = "请选择格式化类型";
                }
                else
                {
                    switch (toolButtonCombox.Text)
                    {
                        case "SQL脚本":
                            Reulst = DevelopAssistant.Format.TsqlFormatHelper.FormatToString(InputText.Text, true);
                            break;
                        case "Javascript":
                            Reulst = DevelopAssistant.Format.JavaScriptFormatHelper.FormatToString(InputText.Text);
                            break;
                        case "JSON":
                            Reulst = DevelopAssistant.Format.JsonFormatHelper.FormatToString(InputText.Text);
                            break;
                        case "CSS样式":
                            Reulst = DevelopAssistant.Format.CssFormatHelper.FormatToString(InputText.Text);
                            break;
                        case "XML文件":
                            Reulst = DevelopAssistant.Format.XMLFormatHelper.FormatToString(InputText.Text);
                            break;

                    }

                    SetOutText(Reulst);
                }
            }
            else
            {
                SetOutText("输入内容为空");
            }
        }

        private void SetOutText(string result)
        {
            toolStripLabelTotalChars.Text = string.Format("共计: {0} 个字符", result.ToCharArray().Length);
            OutText.Text = result;// "输入内容为空";
            OutText.Refresh();
        }

        private void toolButtonCombox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolButtonCombox.SelectedIndex > -1)
            {
                HighlightingItem item = (HighlightingItem)toolButtonCombox.Items[toolButtonCombox.SelectedIndex];
                this.InputText.SetHighlighting(_editorTheme, item.Value);
                this.OutText.SetHighlighting(_editorTheme, item.Value);
            }            
        }

        private void toolButtonSave_Click(object sender, EventArgs e)
        {
            string filter = "所有格式|*.*";
            SaveFileDialog saveDialog = new SaveFileDialog();
            switch (toolButtonCombox.SelectedIndex)
            {
                case 0:
                    filter = "sql|*.sql";
                    break;
                case 1:
                    filter = "js|*.js";
                    break;
                case 2:
                    filter = "json|*.json";
                    break;
                case 3:
                    filter = "css|*.css";
                    break;
            }
            saveDialog.Filter = filter;
            if (saveDialog.ShowDialog(this).Equals(DialogResult.OK))
            {
                string path = saveDialog.FileName;
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    //获得字节数组
                    byte[] data = System.Text.Encoding.Default.GetBytes(OutText.Text);
                    //开始写入
                    fs.Write(data, 0, data.Length);
                    fs.Flush();
                    data = null;
                }
            }
        }

        private void toolButtonCompress_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(InputText.Text))
            {
                string Reulst = String.Empty;
                SetOutText(Reulst);

                if (string.IsNullOrEmpty(toolButtonCombox.Text))
                {
                    Reulst = "请选择格式化类型";
                }
                else
                {
                    switch (toolButtonCombox.Text)
                    {
                        case "SQL脚本":
                            Reulst = DevelopAssistant.Format.TsqlFormatHelper.CompressToString(InputText.Text);
                            break;
                        case "Javascript":
                            Reulst = DevelopAssistant.Format.JavaScriptFormatHelper.CompressToString(InputText.Text);
                            break;
                        case "JSON":
                            Reulst = DevelopAssistant.Format.JsonFormatHelper.CompressToString(InputText.Text);
                            break;
                        case "CSS样式":
                            Reulst = DevelopAssistant.Format.CssFormatHelper.CompressToString(InputText.Text);
                            break;
                        case "XML文件":
                            Reulst = DevelopAssistant.Format.XMLFormatHelper.CompressToString(InputText.Text);
                            break;

                    }

                    SetOutText(Reulst);
                }
            }
            else
            {
                SetOutText("输入内容为空");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.groupBox1.Title = "输入内容：";
            this.groupBox2.Title = "输出内容：";
            this.groupBox1.Height = this.Height / 2;             
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.groupBox1.Height = this.Height / 2;            
        }
        
        public override void OnThemeChanged(EventArgs e)
        {             
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            _editorTheme = themeName;

            Color listBoxColor = SystemColors.Window;
            Color foreColor = SystemColors.ControlText;
            Color backColor = SystemColors.Control;
            Color textColor = Color.White;
            switch (themeName)
            {
                case "Black":
                    listBoxColor = Color.FromArgb(048, 048, 048);
                    //foreColor = Color.FromArgb(240, 240, 248);
                    textColor = Color.FromArgb(030, 030, 030);
                    foreColor = Color.FromArgb(240, 240, 248);// Color.White;
                    backColor = Color.FromArgb(255, 050, 050, 050);
                    break;
                case "Default":
                    listBoxColor = SystemColors.Window;
                    foreColor = SystemColors.ControlText;
                    backColor = SystemColors.Control;
                    break;
            }
            panel1.BackColor = backColor;
            panel2.BackColor = backColor;

            InputText.BackColor = textColor;
            OutText.BackColor = textColor;

            BackColor = backColor;
            groupBox2.BackColor = backColor;
            toolStrip1.BackColor = backColor;
            toolStrip2.BackColor = backColor;
            toolButtonCombox.BackColor = backColor;
            toolButtonCombox.ForeColor = foreColor;
            groupBox1.ForeColor = foreColor;
            groupBox2.ForeColor = foreColor;

            HighlightingItem item = (HighlightingItem)toolButtonCombox.Items[toolButtonCombox.SelectedIndex];
            this.InputText.SetHighlighting(_editorTheme, item.Value);
            this.OutText.SetHighlighting(_editorTheme, item.Value);

            this.BackColor = backColor;
            this.splitter1.BackColor = backColor;
            

        }

    }
}

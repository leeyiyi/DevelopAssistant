﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NDrawItemEventArgs
    {
        private Color _backColor;
        public Color BackColor
        {
            get { return _backColor; }
        }

        private Color _foreColor;
        public Color ForeColor
        {
            get { return _foreColor; }
        }

        private Rectangle _bounds;
        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        private Font _font;
        public Font Font
        {
            get { return _font; }
        }

        private Graphics _graphics;
        public Graphics Graphics
        {
            get { return _graphics; }
        }

        private int _index;
        public int Index
        {
            get { return _index; }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
        }

        private StringFormat _stringFormat;
        public StringFormat StringFormat
        {
            get { return _stringFormat; }
        }

        private DrawItemState _drawItemState;
        public DrawItemState DrawItemState
        {
            get { return _drawItemState; }
        }

        private TextFormatFlags _textFormat;
        public TextFormatFlags TextFormat
        {
            get { return _textFormat; }
            set { _textFormat = value; }
        }

        public NDrawItemEventArgs(Graphics graphics, Font font, Rectangle rect, int index, DrawItemState state, Color foreColor, Color backColor) :
            this(graphics, font, rect, index, state, foreColor, backColor, "", null, TextFormatFlags.Default)
        {

        }       

        internal NDrawItemEventArgs(Graphics graphics, Font font, Rectangle rect, int index, DrawItemState state, Color foreColor, Color backColor, string text, StringFormat stringFormat, TextFormatFlags textFormat)
        {
            this._text = text;
            this._font = font;
            this._bounds = rect;
            this._index = index;
            this._graphics = graphics;
            this._drawItemState = state;
            this._foreColor = foreColor;
            this._backColor = backColor;
            this._textFormat = textFormat;
            this._stringFormat = stringFormat;
        }

        public virtual void DrawBackground()
        {
            using (Brush brush = new SolidBrush(_backColor))
            {
                _graphics.FillRectangle(brush, _bounds);
            }
        }

        public virtual void DrawFocusRectangle()
        {
            if (_drawItemState == DrawItemState.Selected)
            {
                using (Pen pen = new Pen(Color.Gray, 1.0f))
                {
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    Rectangle rect = new Rectangle(_bounds.Left, _bounds.Top, _bounds.Width - 1, _bounds.Height - 1);
                    _graphics.DrawRectangle(pen, rect);
                }
            }
        }

        public virtual void DrawText()
        {
            if (!string.IsNullOrEmpty(_text))
            {
                //using (Brush brush = new SolidBrush(_foreColor))
                //{                    

                //}
                TextRenderer.DrawText(_graphics, _text, this.Font, _bounds, _foreColor, _textFormat);
            }
        }

    }

    public delegate void NDrawItemEventHandler(NListBox sender, NDrawItemEventArgs e);
}

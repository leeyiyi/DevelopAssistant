﻿// Type: ICSharpCode.WinFormsUI.Controls.ExpandingEventArgs
// Assembly: ExpandableGridView, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 01AF7F03-14C8-403C-9988-D5C2B7E87D00
// Assembly location: C:\Users\lenovo\Desktop\TreeGridViewTest\TreeGridViewTest\lib\ExpandableGridView.dll

using System.ComponentModel;

namespace ICSharpCode.WinFormsUI.Controls
{
  public class ExpandingEventArgs : CancelEventArgs
  {
    private TreeGridNode _node;

    public TreeGridNode Node
    {
      get
      {
        return this._node;
      }
    }

    private ExpandingEventArgs()
    {
    }

    public ExpandingEventArgs(TreeGridNode node)
    {
      this._node = node;
    }
  }
}

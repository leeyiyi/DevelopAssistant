﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.Control;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NScrollPanelControlCollection   
    {
        private NScrollPanel panel = null;

        private ControlCollection controlCollection = null;

        public NScrollPanelControlCollection(NScrollPanel nScrollPanel)
        {
            panel = nScrollPanel;
            controlCollection = nScrollPanel.innerPanel.Controls;
        }

        public void Add(Control value)
        {
            controlCollection.Add(value);
            panel.UpdataScrollBar();
        }

        public void Remove(Control value)
        {
            controlCollection.Remove(value);
            panel.UpdataScrollBar();
        }

        public void Clear()
        {
            controlCollection.Clear();
            panel.UpdataScrollBar();
        }

        public void RemoveAt(int index)
        {
            controlCollection.RemoveAt(index);
            panel.UpdataScrollBar();
        }

        public bool Contains(Control control)
        {
            return controlCollection.Contains(control);
        }

        public int Count
        {
            get
            {
                if (controlCollection == null) return 0;
                return controlCollection.Count;
            }
        }

        public Control this[int index]
        {
            get
            {
                return controlCollection[index];
            }
        }
       
    }
}

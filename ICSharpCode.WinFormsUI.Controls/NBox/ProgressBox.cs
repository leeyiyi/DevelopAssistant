﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    using ICSharpCode.WinFormsUI.Controls.Properties;

    /* 
     * [ToolboxBitmap(typeof(ProgressBox),"xxx.bmp")]
     * 其中ProgressBox是你的自定义控件，"xxx.bmp"是你要用的图标名称。
    */
    public class NProgressBox : System.Windows.Forms.Control //System .Windows .Forms.Control
    {        
        private int _leftborderwidth = 1;
        private int _topborderwidth = 1;
        private int _rightborderwidth = 1;
        private int _bottomborderwidth = 1;
        private Color _blackColor = SystemColors.AppWorkspace;
        private Color _borderColor = SystemColors.ControlDark;
        private Color _waitBackColor = SystemColors.Control;
        private Color _waitTextColor = SystemColors.MenuText;

        private string _themeName;

        private int _alpha = 10;
        public int Alpha
        {
            get { return _alpha; }
            set
            {
                _alpha = value;
                if (_alpha > 0)
                {
                    _alpha = _alpha / 100 * 255;
                }
            }
        }

        private ProgressBar _progressBar;
        public ProgressBar ProgressBar
        {
            get { return _progressBar; }
            set
            {
                _progressBar = value;

                if (_progressBar == null)
                    throw new Exception("ProgressBar"+"为空");

                _progressBar.Maximum = 100;
                _progressBar.Minimum = 0;

                this.Controls.Clear();
                this.Controls.Add(_progressBar);
                this._waitMessageBox = null;
              
                this.Invalidate();

            }
        }

        private bool _showWaitMessage = false;
        public bool ShowWaitMessage
        {
            get { return _showWaitMessage; }
            set
            {
                _showWaitMessage = value;
                this.Invalidate();
            }
        }

        private int _radius = 8;   
        [DefaultValue(typeof(int), "8")]
        public int Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        private Label _waitMessageBox;
        public Label WaitMessageBox
        {
            get { return _waitMessageBox; }
            set
            {
                _waitMessageBox = value;
                if (_waitMessageBox == null)
                {
                    throw new Exception("WaitMessageBox为空");
                }
                _waitMessageBox.AutoSize = false;              
                _waitMessageBox.Width = 170;
                _waitMessageBox.Height = 40;
                _waitMessageBox.BorderStyle = BorderStyle.FixedSingle;
                _waitMessageBox.BackColor = _waitBackColor;
                _waitMessageBox.ForeColor = _waitTextColor;
                _waitMessageBox.Padding = new System.Windows.Forms.Padding(4);
                if (string.IsNullOrEmpty(_waitMessageBox.Text))
                {
                    _waitMessageBox.Text = "系统正在处理中...";                   
                }
                if (_waitMessageBox.Image == null)
                {
                    _waitMessageBox.Image = Resources.Loading;
                }
                _waitMessageBox.TextAlign = ContentAlignment.MiddleRight;
                _waitMessageBox.ImageAlign = ContentAlignment.MiddleLeft;

                _progressBar = null;

                this.Controls.Clear();
                this.Controls.Add(_waitMessageBox);
                
                this.Invalidate();
            }
        }

        public string ThemeName
        {
            get { return _themeName; }
        }

        public NProgressBox()
        {
            this.Visible = false;
            this.BackColor = SystemColors.AppWorkspace;
            this.ClientSize = new Size(100,24);        
            this.WaitMessageBox = new Label();           
        }

        public void Show(Control owner)
        {           
            this.Visible = true;
            this.Invalidate();
            Application.DoEvents();
            this.BringToFront();
        }

        public void Show(Control owner, System.Drawing.Rectangle Bound)
        {
            if (this.WaitMessageBox != null)
                this.WaitMessageBox.Visible = false;           
            SetBoundsCore(Bound.X, Bound.Y, Bound.Width, Bound.Height, BoundsSpecified.All);
            LayoutControls();
            Show(owner);
            if (this.WaitMessageBox != null)
                this.WaitMessageBox.Visible = true;
        }

        public void Hiden()
        {
            this.Visible = false;
        }

        public void Close()
        {
            this.Visible = false;            
        }

        private void LayoutControls()
        {
            if (_progressBar != null)
            {
                int X = this.ClientRectangle.X + (this.ClientRectangle.Width - this._progressBar.Width) / 2;
                int Y = this.ClientRectangle.Y + (this.ClientRectangle.Height - this._progressBar.Height) / 2;
                _progressBar.SetBounds(X, Y, this._progressBar.Width, _progressBar.Height);
                
            }
            if (_waitMessageBox != null)
            {
                int X = this.ClientRectangle.X + (this.ClientRectangle.Width - this._waitMessageBox.Width) / 2;
                int Y = this.ClientRectangle.Y + (this.ClientRectangle.Height - this._waitMessageBox.Height) / 2;
                _waitMessageBox.SetBounds(X, Y, this._waitMessageBox.Width, _waitMessageBox.Height);
            }
        }

        private void DrawWaitMessageBox(Graphics g)
        {
            if (_showWaitMessage && _progressBar == null && _waitMessageBox == null) //&& this.TabCount <= 0
            {
                Rectangle box_rect = new Rectangle(0, 0, 200, 40);
                Rectangle clt_rect = this.ClientRectangle;
                clt_rect.Inflate(-4, -4);
                if (clt_rect.Width > box_rect.Width && clt_rect.Height > box_rect.Height)
                {
                    box_rect.X = clt_rect.X + (clt_rect.Width - box_rect.Width) / 2;
                    box_rect.Y = clt_rect.Y + (clt_rect.Height - box_rect.Height) / 2;

                    GraphicsPath path = CommonHelper.CreateRoundedRectanglePath(box_rect, _radius);                   

                    using (Brush bru = new SolidBrush(_waitBackColor))
                    {
                        g.FillPath(bru, path);
                    }

                    using (Brush bru = new SolidBrush(SystemColors.ControlDarkDark))
                    {
                        g.DrawPath(new Pen(bru, 1.0f), path);
                    }

                    using (Brush bru = new SolidBrush(_waitTextColor))
                    {
                        StringFormat sf = new StringFormat();
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Alignment = StringAlignment.Center;
                        g.DrawString("系统正在处理中...", this.Font, bru, box_rect, sf);
                    }

                    path.Dispose();

                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            if (this.ClientSize.Width > 0 && this.ClientSize.Height > 0)
            {
                using (Brush backColorBrush = new SolidBrush(Color.FromArgb(this._alpha, this._blackColor)))
                {
                    e.Graphics.FillRectangle(backColorBrush, this.ClientRectangle);
                }
                ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
           _borderColor, _leftborderwidth, ButtonBorderStyle.Solid, //左边
           _borderColor, _topborderwidth, ButtonBorderStyle.Solid, //上边
           _borderColor, _rightborderwidth, ButtonBorderStyle.Solid, //右边
           _borderColor, _bottomborderwidth, ButtonBorderStyle.Solid);//底边
            }          
                
        }

        public void ReportProgressBarValue(int Value)
        {
            if (this._progressBar != null)
            {
                this._progressBar.Value = Value;
                Application.DoEvents();
            }           
        }

        public void SetTheme(string themeName)
        {            
            switch (themeName)
            {
                case "Default":
                    _waitBackColor = SystemColors.Control;
                    _waitTextColor = SystemColors.MenuText;
                    _blackColor = SystemColors.AppWorkspace;
                    if (_waitMessageBox != null)
                        _waitMessageBox.BackColor = Color.White;
                    break;
                case "Black":
                    _waitTextColor = Color.FromArgb(240, 240, 240);
                    _waitBackColor = Color.FromArgb(050, 050, 050);
                    _blackColor = Color.FromArgb(045, 045, 048);
                    if (_waitMessageBox != null)
                        _waitMessageBox.BackColor = Color.FromArgb(050, 050, 050);
                    break;
            }
            BackColor = _blackColor;
            _themeName = themeName;
        }
    }
}

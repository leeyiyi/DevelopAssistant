using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.ComponentModel;
using ICSharpCode.WinFormsUI.Controls.Properties;

namespace ICSharpCode.WinFormsUI.Controls.NodeControls
{
	public class NodeCheckBox : BindableControl
	{
		public const int ImageSize = 13;
		public const int Width = 13;
		private Bitmap _check;
		private Bitmap _uncheck;
		private Bitmap _unknown;

		#region Properties

		private bool _threeState;
		[DefaultValue(false)]
		public bool ThreeState
		{
			get { return _threeState; }
			set { _threeState = value; }
		}

		private bool _editEnabled = true;
		[DefaultValue(true)]
		public bool EditEnabled
		{
			get { return _editEnabled; }
			set { _editEnabled = value; }
		}

		#endregion

		public NodeCheckBox()
			: this(string.Empty)
		{
		}

		public NodeCheckBox(string propertyName)
		{
            _check = Resources._checked; 
            _uncheck = Resources._unchecked;
			_unknown = Resources.unknown;
			DataPropertyName = propertyName;
		}

		public override Size MeasureSize(NTreeNode node)
		{
			return new Size(Width, Width);
		}

		public override void Draw(NTreeNode node, DrawContext context)
		{
			Rectangle r = context.Bounds;
			int dy = (int)Math.Round((float)(r.Height - ImageSize) / 2);
			CheckState state = GetCheckState(node);
			if (Application.RenderWithVisualStyles)
			{
				VisualStyleRenderer renderer;
				if (state == CheckState.Indeterminate)
					renderer = new VisualStyleRenderer(VisualStyleElement.Button.CheckBox.MixedNormal);
				else if (state == CheckState.Checked)
					renderer = new VisualStyleRenderer(VisualStyleElement.Button.CheckBox.CheckedNormal);
				else
					renderer = new VisualStyleRenderer(VisualStyleElement.Button.CheckBox.UncheckedNormal);
				renderer.DrawBackground(context.Graphics, new Rectangle(r.X, r.Y + dy, ImageSize, ImageSize));
			}
			else
			{
				Image img;
				if (state == CheckState.Indeterminate)
					img = _unknown;
				else if (state == CheckState.Checked)
					img = _check;
				else
					img = _uncheck;
				context.Graphics.DrawImage(img, new Point(r.X, r.Y + dy));
				//ControlPaint.DrawCheckBox(context.Graphics, r, state2);
			}
		}

		protected virtual CheckState GetCheckState(NTreeNode node)
		{
			object obj = GetValue(node);
			if (obj is CheckState)
				return (CheckState)obj;
			else if (obj is bool)
				return (bool)obj ? CheckState.Checked : CheckState.Unchecked;
			else
				return CheckState.Unchecked;
		}

		protected virtual void SetCheckState(NTreeNode node, CheckState value)
		{
			Type type = GetPropertyType(node);
			if (type == typeof(CheckState))
			{
				SetValue(node, value);
				OnCheckStateChanged(node);
			}
			else if (type == typeof(bool))
			{
				SetValue(node, value != CheckState.Unchecked);
				OnCheckStateChanged(node);
			}            
        }

        public override void MouseDown(NTreeNodeMouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left && EditEnabled)
            {
                CheckState state = GetCheckState(args.Node);
                state = GetNewState(state);
                SetCheckState(args.Node, state);
                if (args.Node != null &&
                    args.Node.TreeView.Model == null)
                {
                    SetSelfValue(args.Node, state != CheckState.Unchecked);
                    args.Node.TreeView.UpdateView();
                }
                args.Handled = true;
            }
        }

		public override void MouseDoubleClick(NTreeNodeMouseEventArgs args)
		{
			args.Handled = true;
		}

		private CheckState GetNewState(CheckState state)
		{
			if (state == CheckState.Indeterminate)
				return CheckState.Unchecked;
			else if(state == CheckState.Unchecked)
				return CheckState.Checked;
			else 
				return ThreeState ? CheckState.Indeterminate : CheckState.Unchecked;
		}

		public override void KeyDown(KeyEventArgs args)
		{
			if (args.KeyCode == Keys.Space && EditEnabled)
			{
				Parent.BeginUpdate();
				try
				{
					if (Parent.CurrentNode != null)
					{
						CheckState value = GetNewState(GetCheckState(Parent.CurrentNode));
						foreach (NTreeNode node in Parent.Selection)
							SetCheckState(node, value);
					}
				}
				finally
				{
					Parent.EndUpdate();
				}
				args.Handled = true;
			}
		}

		public event EventHandler<NTreeViewPathEventArgs> CheckStateChanged;
		protected void OnCheckStateChanged(NTreeViewPathEventArgs args)
		{
			if (CheckStateChanged != null)
				CheckStateChanged(this, args);
		}

		protected void OnCheckStateChanged(NTreeNode node)
		{
			NTreeViewPath path = this.Parent.GetPath(node);
			OnCheckStateChanged(new NTreeViewPathEventArgs(path));
		}

	}
}

﻿using System;
using System.Windows.Forms;
namespace ICSharpCode.WinFormsUI.Controls
{
	internal abstract class InputState
	{
		private NTreeView _tree;

		public NTreeView Tree
		{
			get { return _tree; }
		}

		public InputState(NTreeView tree)
		{
			_tree = tree;
		}

		public abstract void KeyDown(System.Windows.Forms.KeyEventArgs args);
		public abstract void MouseDown(NTreeNodeMouseEventArgs args);
		public abstract void MouseUp(NTreeNodeMouseEventArgs args);

		/// <summary>
		/// handle OnMouseMove event
		/// </summary>
		/// <param name="args"></param>
		/// <returns>true if event was handled and should be dispatched</returns>
		public virtual bool MouseMove(MouseEventArgs args)
		{
			return false;
		}
	}
}

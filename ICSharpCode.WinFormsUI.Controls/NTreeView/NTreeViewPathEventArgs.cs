using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public class NTreeViewPathEventArgs : EventArgs
	{
		private NTreeViewPath _path;
		public NTreeViewPath Path
		{
			get { return _path; }
		}

		public NTreeViewPathEventArgs()
		{
			_path = new NTreeViewPath();
		}

		public NTreeViewPathEventArgs(NTreeViewPath path)
		{
			if (path == null)
				throw new ArgumentNullException();

			_path = path;
		}
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public class NTreeViewEventArgs: EventArgs
	{
		private NTreeNode _node;

		public NTreeNode Node
		{
			get { return _node; }
		}

		public NTreeViewEventArgs(NTreeNode node)
		{
			_node = node;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NHScrollBar : NScrollBar
    {
        public NHScrollBar()
        {
            this.orientation = ScrollBarOrientation.Horizontal;
            this.scrollOrientation = System.Windows.Forms.ScrollOrientation.HorizontalScroll;
        }
    }
}

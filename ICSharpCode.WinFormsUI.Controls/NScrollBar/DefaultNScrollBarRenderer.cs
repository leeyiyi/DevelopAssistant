﻿namespace ICSharpCode.WinFormsUI.Controls
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;

    public class DefaultScrollBarRenderer : NScrollBarRenderer
    {
        public DefaultScrollBarRenderer()
        {
            drawThumbGrip = true;

            // hot state
            thumbColors[0, 0] = Color.FromArgb(96, 111, 148); // border color
            thumbColors[0, 1] = Color.FromArgb(232, 233, 233); // left/top start color
            thumbColors[0, 2] = Color.FromArgb(230, 233, 241); // left/top end color
            thumbColors[0, 3] = Color.FromArgb(233, 237, 242); // right/bottom line color
            thumbColors[0, 4] = Color.FromArgb(209, 218, 228); // right/bottom start color
            thumbColors[0, 5] = Color.FromArgb(218, 227, 235); // right/bottom end color
            thumbColors[0, 6] = Color.FromArgb(190, 202, 219); // right/bottom middle color
            thumbColors[0, 7] = Color.FromArgb(96, 11, 148); // left/top line color

            // over state
            thumbColors[1, 0] = Color.FromArgb(60, 110, 176);
            thumbColors[1, 1] = Color.FromArgb(187, 204, 228);
            thumbColors[1, 2] = Color.FromArgb(205, 227, 254);
            thumbColors[1, 3] = Color.FromArgb(252, 253, 255);
            thumbColors[1, 4] = Color.FromArgb(170, 207, 247);
            thumbColors[1, 5] = Color.FromArgb(219, 232, 251);
            thumbColors[1, 6] = Color.FromArgb(190, 202, 219);
            thumbColors[1, 7] = Color.FromArgb(233, 233, 235);

            // pressed state
            thumbColors[2, 0] = Color.FromArgb(60, 110, 176);
            thumbColors[2, 1] = Color.FromArgb(187, 204, 228);
            thumbColors[2, 2] = Color.FromArgb(205, 227, 254);
            thumbColors[2, 3] = Color.FromArgb(252, 253, 255);
            thumbColors[2, 4] = Color.FromArgb(170, 207, 247);
            thumbColors[2, 5] = Color.FromArgb(219, 232, 251);
            thumbColors[2, 6] = Color.FromArgb(190, 202, 219);
            thumbColors[2, 7] = Color.FromArgb(233, 233, 235);

            /* picture of colors and indices
             *(0,0)
             * -----------------------------------------------
             * |                                             |
             * | |-----------------------------------------| |
             * | |                  (2)                    | |
             * | | |-------------------------------------| | |
             * | | |                (0)                  | | |
             * | | |                                     | | |
             * | | |                                     | | |
             * | |3|                (1)                  |3| |
             * | |6|                (4)                  |6| |
             * | | |                                     | | |
             * | | |                (5)                  | | |
             * | | |-------------------------------------| | |
             * | |                  (12)                   | |
             * | |-----------------------------------------| |
             * |                                             |
             * ----------------------------------------------- (15,17)
             */

            // hot state
            arrowColors[0, 0] = Color.FromArgb(223, 236, 252);
            arrowColors[0, 1] = Color.FromArgb(207, 225, 248);
            arrowColors[0, 2] = Color.FromArgb(245, 249, 255);
            arrowColors[0, 3] = Color.FromArgb(237, 244, 252);
            arrowColors[0, 4] = Color.FromArgb(244, 249, 255);
            arrowColors[0, 5] = Color.FromArgb(244, 249, 255);
            arrowColors[0, 6] = Color.FromArgb(251, 253, 255);
            arrowColors[0, 7] = Color.FromArgb(251, 253, 255);

            // over state
            arrowColors[1, 0] = Color.FromArgb(205, 222, 243);
            arrowColors[1, 1] = Color.FromArgb(186, 208, 235);
            arrowColors[1, 2] = Color.FromArgb(238, 244, 252);
            arrowColors[1, 3] = Color.FromArgb(229, 237, 247);
            arrowColors[1, 4] = Color.FromArgb(223, 234, 247);
            arrowColors[1, 5] = Color.FromArgb(241, 246, 254);
            arrowColors[1, 6] = Color.FromArgb(243, 247, 252);
            arrowColors[1, 7] = Color.FromArgb(250, 252, 255);

            // pressed state
            arrowColors[2, 0] = Color.FromArgb(215, 220, 225);
            arrowColors[2, 1] = Color.FromArgb(195, 202, 210);
            arrowColors[2, 2] = Color.FromArgb(242, 244, 245);
            arrowColors[2, 3] = Color.FromArgb(232, 235, 238);
            arrowColors[2, 4] = Color.FromArgb(226, 228, 230);
            arrowColors[2, 5] = Color.FromArgb(230, 233, 236);
            arrowColors[2, 6] = Color.FromArgb(244, 245, 245);
            arrowColors[2, 7] = Color.FromArgb(245, 247, 248);

            // background colors
            backgroundColors[0] = Color.FromArgb(235, 237, 239);
            backgroundColors[1] = Color.FromArgb(252, 252, 252);
            backgroundColors[2] = Color.FromArgb(247, 247, 247);
            backgroundColors[3] = Color.FromArgb(238, 238, 238);
            backgroundColors[4] = Color.FromArgb(240, 240, 240);

            // track colors
            trackColors[0] = Color.FromArgb(204, 204, 204);
            trackColors[1] = Color.FromArgb(220, 220, 220);

            // arrow border colors
            arrowBorderColors[0] = Color.FromArgb(135, 146, 160);
            arrowBorderColors[1] = Color.FromArgb(140, 151, 165);
            arrowBorderColors[2] = Color.FromArgb(128, 139, 153);
            arrowBorderColors[3] = Color.FromArgb(99, 110, 125);
        }

        public override void DrawBorder(Graphics g, bool enable, Color color, Rectangle rect)
        {
            base.DrawBorder(g, enable, color, rect);
        }

        public override void DrawThumb(Graphics g, Rectangle rect, ScrollBarState state, ScrollBarOrientation orientation)
        {
            base.DrawThumb(g, rect, state, ScrollBarOrientation.None);

            if (orientation == ScrollBarOrientation.Vertical)
            {
                DrawThumbVertical(g, rect, state);
            }
            else
            {
                DrawThumbHorizontal(g, rect, state);
            }
        }
    }
}

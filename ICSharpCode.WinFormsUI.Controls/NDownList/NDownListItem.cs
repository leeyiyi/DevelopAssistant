﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NDownListItem
    {

        private int _index;

        private ICSharpCode.WinFormsUI.Controls.DownList.ProgressBar _progressBar;

        private ICSharpCode.WinFormsUI.Controls.DownList.StateLabel _stateLabel;

        private Image _image;

        private NDownList _owner;

        private string _text;

        private string _name;

        private string _label;

        public int Index
        {
            get
            {
                return _index;
            }
            set
            {
                _index = value;
            }
        }

        public ICSharpCode.WinFormsUI.Controls.DownList.ProgressBar ProgressBar
        {
            get
            {
                return _progressBar;
            }
            set
            {
                _progressBar = value;
            }
        }

        public ICSharpCode.WinFormsUI.Controls.DownList.StateLabel StateLabel
        {
            get
            {
                return _stateLabel;
            }
            set
            {
                _stateLabel = value;
            }
        }

        public Image Icon
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public NDownList Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
            }
        }

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
            }
        }

        public NDownListItem(string Text)
        {
            this.Name = Text;
            this.Text = Text;
        }

        public NDownListItem(string Name, string Text)
        {
            this.Name = Name;
            this.Text = Text;
        }

        public void ReportProgressValue(int value)
        {
            if (value < 0 || value > 100)
                throw new Exception("value error : value>=0  and value<=100 ");

            if (this.ProgressBar != null)
                this.ProgressBar.Value = value;
            if (this.StateLabel != null)
                this.StateLabel.Text = value + " %";

        }

        public double GetProgressValue()
        {
            double result = 0.0;
            if (this.ProgressBar != null)
                result = this.ProgressBar.Value;
            return result;
        }

        public void Initialize()
        {
            this.ProgressBar = new ICSharpCode.WinFormsUI.Controls.DownList.ProgressBar(this.Owner);
            this.StateLabel = new ICSharpCode.WinFormsUI.Controls.DownList.StateLabel();
        }

        public override string ToString()
        {
            return this.Text;
        }

    }
}

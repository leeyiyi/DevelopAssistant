﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NDownListItemCollection : IEnumerable
    {
        private NDownList Owner;
        private IList<NDownListItem> list;

        public NDownListItem this[int index]
        {
            get
            {
                return this.list[index];
            }
        }

        public int Count
        {
            get
            {
                return this.list.Count;
            }
        }

        public NDownListItemCollection(NDownList list)
        {
            this.Owner = (NDownList)null;
            this.list = (IList<NDownListItem>)new List<NDownListItem>();
            this.Owner = list;
        }

        public NDownListItem Add(NDownListItem item)
        {
            return Add(item, false);
        }

        public NDownListItem Add(NDownListItem item, bool showProgressBar)
        {
            item.Owner = this.Owner;
            item.Index = this.list.Count;
            if (showProgressBar)
            {
                item.ProgressBar = new DownList.ProgressBar(this.Owner);
                item.StateLabel = new DownList.StateLabel();
            }
            item.Initialize();
            this.list.Add(item);            
            ++this.Owner.SelectedIndex;
            if (this.Owner.Visible)
            {
                this.Owner.Invalidate();
                this.Owner.MeasureScrollBar();
            }            
            return item;
        }

        public void Remove(NDownListItem item)
        {             
            this.list.Remove(item);
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)new NDownListItemArray(Enumerable.ToArray<NDownListItem>((IEnumerable<NDownListItem>)this.list));
        }

        public void Clear()
        {
            this.list.Clear();
            this.Owner.Invalidate();
        }

    }
}

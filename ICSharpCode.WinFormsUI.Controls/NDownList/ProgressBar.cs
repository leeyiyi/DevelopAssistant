﻿using System;
using System.Collections.Generic; 
using System.Text; 

namespace ICSharpCode.WinFormsUI.Controls.DownList
{
    public class ProgressBar
    {
        private NDownList Owner;
        private int _value;
        private int _maximum;

        public int Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
                this.OnValueChanged((object)this, new EventArgs());
            }
        }

        public int Maximum
        {
            get
            {
                return _maximum;
            }
            set
            {
                _maximum = value;
            }
        }

        public ProgressBar(NDownList list)
        {
            this.Owner = (NDownList)null;
            this.Value = 0;
            this.Maximum = 100;
            this.Owner = list;
        }

        protected virtual void OnValueChanged(object sender, EventArgs e)
        {
            if (this.Owner == null)
                return;
            this.Owner.Invalidate();
        }
    }
}

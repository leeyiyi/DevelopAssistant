﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NColumnHeaderCollection : List<NColumnHeader>
    {
        public NColumnHeaderCollection(NListView owner)
        {

        }
        
        public new void Add(NColumnHeader header)
        {
            header.Index = this.Count + 1;
            base.Add(header);            
        }

        public new void AddRange(IEnumerable<NColumnHeader> collection)
        {
            int count = this.Count;
            foreach (NColumnHeader header in collection)
            {
                count = count + 1;
                header.Index = count;
            }
            base.AddRange(collection);
        }
    }

    public class NColumnHeader : Component
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private int _width;
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private HorizontalAlignment _textAlign;
        public HorizontalAlignment TextAlign
        {
            get { return _textAlign; }
            set { _textAlign = value; }
        }

        private int _index;
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        private System.Drawing.Rectangle _bound;
        internal System.Drawing.Rectangle Bound
        {
            get { return _bound; }
            set { _bound = value; }
        }

        private bool _mouseInside = false;
        internal bool MouseInside
        {
            get { return _mouseInside; }
            set { _mouseInside = value; }
        }

        private bool _dragSize = false;
        internal bool DragSize
        {
            get { return _dragSize; }
            set { _dragSize = value; }
        }
        
        public NColumnHeader()
        {
            this.Width = 100;
            this.Name = "Column";
            this.Text = "Column";
            this.Bound = System.Drawing.Rectangle.Empty;
            this.TextAlign = HorizontalAlignment.Left;
        }
    }
}

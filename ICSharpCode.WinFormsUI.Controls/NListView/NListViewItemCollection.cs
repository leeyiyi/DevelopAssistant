﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NListViewItemCollection : List<NListViewItem>
    {
        private NListView listView = null;

        internal NListViewItemCollection()
        {

        }

        public NListViewItemCollection(NListView owner)
        {
            this.listView = owner;
        }

        public void Add(string text)
        {
            base.Add(new NListViewItem() { Text = text });
            if (listView != null)
                listView.UpdateScrollBarInformation();
        }

        public new void Add(NListViewItem item)
        {
            base.Add(item);
            if (listView != null)
                listView.UpdateScrollBarInformation();
        }

        public new void AddRange(IEnumerable<NListViewItem> collection)
        {
            base.AddRange(collection);
            if (listView != null)
                listView.UpdateScrollBarInformation();
        }
    }

    public class NListViewItem
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _imageIndex;
        public int ImageIndex
        {
            get { return _imageIndex; }
            set { _imageIndex = value; }
        }

        private int _stateImageIndex;
        public int StateImageIndex
        {
            get
            {
                return _stateImageIndex;
            }
            set
            {
                _stateImageIndex = value;
            }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private bool _mouseInside = false;
        internal bool MouseInside
        {
            get { return _mouseInside; }
            set { _mouseInside = value; }
        }

        private NListViewItemCollection _subItems;
        public NListViewItemCollection SubItems
        {
            get { return _subItems; }
            set { _subItems = value; }
        }

        private System.Drawing.Rectangle _bound;
        public System.Drawing.Rectangle Bound
        {
            get { return _bound; }
            set { _bound = value; }
        }

        public NListViewItem()
        {
            _subItems = new NListViewItemCollection();
        }

        public NListViewItem(string[] items)
        {
            this.Text = items[0];
            this.SubItems = new NListViewItemCollection();
            if (items.Length > 1)
            {               
                for (int i = 1; i < items.Length; i++)
                {
                    this.SubItems.Add(items[i]);
                }
            }           
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NStatusBar : System.Windows.Forms.StatusStrip
    {
        private int _topborderwidth = 1;
        private int _bottomborderwidth = 1;
        private int _leftborderwidth = 1;
        private int _rightborderwidth = 1;

        private Color _borderColor = SystemColors.ControlDark;
        [CategoryAttribute("设计")]
        [DescriptionAttribute("边框颜色")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                this.Invalidate();
            }
        }

        private NBorderStyle _borderStyle= NBorderStyle.None;
        public NBorderStyle BorderStyle
        {
            get { return _borderStyle; }
            set
            {
                _borderStyle = value;

                switch (_borderStyle)
                {
                    case NBorderStyle.Top:

                        _leftborderwidth = 0;
                        _rightborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;
                    case NBorderStyle.Bottom:

                        _topborderwidth = 0;
                        _leftborderwidth = 0;
                        _rightborderwidth = 0;

                        break;
                    case NBorderStyle.Left:

                        _topborderwidth = 0;
                        _rightborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.Right:

                        _topborderwidth = 0;
                        _leftborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.LeftRight:

                        _topborderwidth = 0;
                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.TopBottom:

                        _leftborderwidth = 0;
                        _rightborderwidth = 0;

                        break;

                    case NBorderStyle.NoTop:

                        _topborderwidth = 0;

                        break;

                    case NBorderStyle.NoBottom:

                        _bottomborderwidth = 0;

                        break;

                    case NBorderStyle.All:
                    default: break;

                }

                this.Invalidate();
            }
        }

        public NStatusBar()
        {
            this.Font = NToolStripItemTheme.TextFont;
            this.ImageScalingSize = new Size(20, 20);
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            if (_borderStyle != NBorderStyle.None)
            {
                System.Windows.Forms.ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
             _borderColor, _leftborderwidth, ButtonBorderStyle.Solid, //左边
             _borderColor, _topborderwidth, ButtonBorderStyle.Solid, //上边
             _borderColor, _rightborderwidth, ButtonBorderStyle.Solid, //右边
             _borderColor, _bottomborderwidth, ButtonBorderStyle.Solid);//底边
            }           
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Invalidate();
        }
    }
}

﻿namespace ICSharpCode.WinFormsUI.Controls.NTimeLine
{
    partial class NTimelineControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NTimelineControl));
            this.TimeLineIcons = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // TimeLineIcons
            // 
            this.TimeLineIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TimeLineIcons.ImageStream")));
            this.TimeLineIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.TimeLineIcons.Images.SetKeyName(0, "edit.png");
            this.TimeLineIcons.Images.SetKeyName(1, "delete.png");
            this.TimeLineIcons.Images.SetKeyName(2, "add.png");
            this.TimeLineIcons.Images.SetKeyName(3, "alarm_clock_32px.png");
            this.TimeLineIcons.Images.SetKeyName(4, "Index_Add_16px.png");
            this.TimeLineIcons.Images.SetKeyName(5, "Package_Add_16px.png");
            // 
            // NTimelineControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Name = "NTimelineControl";
            this.Size = new System.Drawing.Size(267, 159);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList TimeLineIcons;
    }
}

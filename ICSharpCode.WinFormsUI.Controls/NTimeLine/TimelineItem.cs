﻿using System;
using System.Collections.Generic;
using System.Drawing; 

namespace ICSharpCode.WinFormsUI.Controls.NTimeLine
{
    public class TimeLineEventArgs
    {
        public string Command { get; set; }

        public TimelineItem Data { get; set; }

        public TimeLineEventArgs(TimelineItem Data)
        {
            this.Data = Data;
        }
    }

    [Serializable]
    public class DateTimeItem : TimelineItem
    {       
        public Image Icon { get; set; }
        public string Title { get; set; } 
        public string Summary { get; set; }
        public string Description { get; set; }
        public string ToolTip { get; set; }
        public string PersonName { get; set; }
        public DateTime DateTime { get; set; }
        public ImportantLevel Level { get; set; }
        public Timeliness Timeliness { get; set; }
        public string ResponsiblePerson { get; set; }

        internal Rectangle EditRect { get; set; }
        internal Rectangle DeleteRect { get; set; }
        /// <summary>
        /// 0 :默认 1：修改  2：删除
        /// </summary>
        internal int ButtonState { get; set; }
        public Rectangle ClickRect { get; set; }
        public bool Selected { get; set; }

        private string _tag;
        public override string Tag
        {
            get
            {
                if (string.IsNullOrEmpty(_tag))
                {
                    _tag = DateTime.ToString("yyyyMMddHHmmss");
                }
                return _tag;
            }
        }

    }

    [Serializable]
    public class DateItem : TimelineItem
    {
        public DateTime Date { get; set; }      
        public List<DateTimeItem> List { get; set; }

        internal Size Size { get; set; }
        internal Rectangle Bound { get; set; }
        internal Rectangle AddRect { get; set; }
        internal Rectangle ClickRect { get; set; }
        public bool Selected { get; set; }

        private string _tag;
        public override string Tag
        {
            get
            {
                if (string.IsNullOrEmpty(_tag))
                {
                    _tag = Date.ToString("yyyyMMdd");
                }
                return _tag;
            }
        }

    }

    [Serializable]
    public class MonthItem : TimelineItem
    {
        public DateTime Date { get; set; }
        public string DateLabel { get; set; }
        public List<DateItem> List { get; set; }

        internal Size Size { get; set; }
        internal Rectangle Bound { get; set; }
    }

    [Serializable]
    public class TimelineItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual string Tag { get; set; }
    }

    public enum ImportantLevel
    {
        Normal = 0,
        Important = 1,
        MoreImportant = 2,
        MostImportant = 3
    }

    /// <summary>
    /// 时效
    /// </summary>
    public enum Timeliness
    {
        /// <summary>
        /// 不计算时效
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 正常进行中
        /// </summary>
        Green = 1,
        /// <summary>
        /// 将要到期
        /// </summary>
        Yellow = 2,
        /// <summary>
        /// 已到期
        /// </summary>
        Orange = 3,
        /// <summary>
        /// 已过期
        /// </summary>
        Red = 4,
        /// <summary>
        /// 已作废
        /// </summary>
        Dark = 5,
        /// <summary>
        /// 已完成
        /// </summary>
        Black = 6
    }

}

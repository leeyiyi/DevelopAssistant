﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class CommonHelper
    {
        public static GraphicsPath CreateRoundedRectanglePath(Rectangle rect, int radius)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            if (radius > 0)
            {
                roundedRect.AddArc(rect.X, rect.Y, radius * 2, radius * 2, 180, 90);
                roundedRect.AddLine(rect.X + radius, rect.Y, rect.Right - radius * 2, rect.Y);
                roundedRect.AddArc(rect.X + rect.Width - radius * 2, rect.Y, radius * 2, radius * 2, 270, 90);
                roundedRect.AddLine(rect.Right, rect.Y + radius * 2, rect.Right, rect.Y + rect.Height - radius * 2);
                roundedRect.AddArc(rect.X + rect.Width - radius * 2, rect.Y + rect.Height - radius * 2, radius * 2, radius * 2, 0, 90);
                roundedRect.AddLine(rect.Right - radius * 2, rect.Bottom, rect.X + radius * 2, rect.Bottom);
                roundedRect.AddArc(rect.X, rect.Bottom - radius * 2, radius * 2, radius * 2, 90, 90);
                roundedRect.AddLine(rect.X, rect.Bottom - radius * 2, rect.X, rect.Y + radius * 2);
                roundedRect.CloseFigure();
            }
            else
            {
                roundedRect.AddLine(rect.X + radius, rect.Y, rect.Right - radius * 2, rect.Y);
                roundedRect.AddLine(rect.Right, rect.Y + radius * 2, rect.Right, rect.Y + rect.Height - radius * 2);
                roundedRect.AddLine(rect.Right - radius * 2, rect.Bottom, rect.X + radius * 2, rect.Bottom);
                roundedRect.AddLine(rect.X, rect.Bottom - radius * 2, rect.X, rect.Y + radius * 2);
            }
            roundedRect.CloseFigure();
            return roundedRect;
        }

        public static System.Drawing.Image CreateDropColorImage(Image srcImage, Color backColor)
        {
            Bitmap image = new Bitmap(srcImage.Width, srcImage.Height);
            using (Graphics g = Graphics.FromImage(image))
            {
                System.Windows.Forms.ControlPaint.DrawImageDisabled(g, srcImage, 0, 0, backColor);
            }
            return image;
        }

        public static System.Drawing.Image CreateCoverColorImage(Image srcImage, Color foreColor)
        {
            Bitmap image = new Bitmap(srcImage.Width, srcImage.Height);
            using (Graphics g = Graphics.FromImage(image))
            {
                using (ImageAttributes imageAttributes = new ImageAttributes())
                {
                    ColorMap[] colorMap = new ColorMap[2];
                    colorMap[0] = new ColorMap();
                    colorMap[0].OldColor = Color.FromArgb(0, 0, 0);
                    colorMap[0].NewColor = foreColor;
                    colorMap[1] = new ColorMap();
                    colorMap[1].OldColor = image.GetPixel(0, 0);
                    colorMap[1].NewColor = Color.Transparent;

                    imageAttributes.SetRemapTable(colorMap);

                    g.DrawImage(srcImage, new Rectangle(0, 0, image.Width, image.Height),
                       0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);
                }
                //g.DrawImage(srcImage, new Rectangle(0, 0, image.Width, image.Height),
                //   0, 0, image.Width, image.Height, GraphicsUnit.Pixel);

            }
            return image;
        }

    }
}

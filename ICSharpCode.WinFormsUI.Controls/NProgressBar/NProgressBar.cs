﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NProgressBar : Control
    {
        private int _minimum;
        private int _maximum;
        private int _value;
        private Color _faceColor;
        private Color _baseColor;
        private bool _showValue;
        private bool _linearGradient;
        private IContainer components;

        public int Minimum
        {
            get
            {
                return this._minimum;
            }
            set
            {
                this._minimum = value;
            }
        }

        public int Maximum
        {
            get
            {
                return this._maximum;
            }
            set
            {
                this._maximum = value;
            }
        }

        public int Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
                this.Invalidate();
            }
        }

        public Color FaceColor
        {
            get
            {
                return this._faceColor;
            }
            set
            {
                this._faceColor = value;
            }
        }

        public Color BaseColor
        {
            get { return _baseColor; }
            set { _baseColor = value; }
        }

        public bool ShowValue
        {
            get
            {
                return this._showValue;
            }
            set
            {
                this._showValue = value;
            }
        }

        public bool LinearBrush
        {
            get
            {
                return this._linearGradient;
            }
            set
            {
                this._linearGradient = value;
            }
        }

        public NProgressBar()
        {             
            this.components = new System.ComponentModel.Container();
            this.InitializeComponent();
            this.Initialize();
        }

        public void Initialize()
        {
            this.Minimum = 0;
            this.Maximum = 100;
            this.ShowValue = false;
            this.LinearBrush = false;
            this.FaceColor = Color.FromArgb(86, 176, 227);
            this.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
            this.UpdateStyles();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            Rectangle clientRectangle = this.ClientRectangle;
            Rectangle rect1 = new Rectangle(clientRectangle.X, clientRectangle.Y, clientRectangle.Width, clientRectangle.Height);
            Rectangle rect2 = new Rectangle(clientRectangle.X, clientRectangle.Y, (int)((double)this.Value * 1.0 / (double)this.Maximum * (double)clientRectangle.Width), clientRectangle.Height);
            Color color = Color.FromArgb(230, 247, (int)byte.MaxValue);
            graphics.FillRectangle((Brush)new SolidBrush(_baseColor), rect1);
            if (!this.LinearBrush && this.Value > 0)
                graphics.FillRectangle((Brush)new SolidBrush(this.FaceColor), rect2);
            if (this.LinearBrush && this.Value > 0)
            {
                graphics.FillRectangle((Brush)new LinearGradientBrush(new Rectangle(clientRectangle.X, clientRectangle.Y + rect2.Height / 2, rect2.Width, rect2.Height), color, this.FaceColor, LinearGradientMode.Vertical), new Rectangle(clientRectangle.X, clientRectangle.Y, rect2.Width, rect2.Height / 2));
                graphics.FillRectangle((Brush)new LinearGradientBrush(new Rectangle(clientRectangle.X, clientRectangle.Y + rect2.Height / 2, rect2.Width, rect2.Height), this.FaceColor, color, LinearGradientMode.Vertical), new Rectangle(clientRectangle.X, clientRectangle.Y + rect2.Height / 2, rect2.Width, rect2.Height / 2));
            }
            if (this.ShowValue && this.Value > 0)
                TextRenderer.DrawText((IDeviceContext)graphics, "已完成 " + (object)this.Value + " %", this.Font, clientRectangle, Color.Green, Color.Empty, TextFormatFlags.Right | TextFormatFlags.VerticalCenter | TextFormatFlags.WordEllipsis);
            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, SystemColors.ControlDark, 1, ButtonBorderStyle.Solid);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.Name = "NProgressBar";
            this.Size = new Size(200, 30);
            this.ResumeLayout(false);
        }

    }
}

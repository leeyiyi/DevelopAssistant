﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public partial class NLabel : System.Windows.Forms.Control
    {
        ToolTip tooltip = new ToolTip();

        private string _Text;
        public new string Text
        {
            set
            {
                _Text = value;
                this.Invalidate();
            }
            get { return _Text; }
        }

        private Image _Image;
        public Image Image
        {
            set
            {
                _Image = value;
                this.Invalidate();
            }
            get { return _Image; }
        }

        private ContentAlignment _ImageAlign;
        public ContentAlignment ImageAlign
        {
            set { _ImageAlign = value; }
            get { return _ImageAlign; }
        }

        private ContentAlignment _TextAlign;
        public ContentAlignment TextAlign
        {
            set { _TextAlign = value; }
            get { return _TextAlign; }
        }

        private string _ToolTip;
        public string ToolTip
        {
            set
            {
                _ToolTip = value;
                if (!string.IsNullOrEmpty(_ToolTip))
                {                    
                    tooltip.SetToolTip(this, _ToolTip);
                }
                else
                {
                    tooltip.RemoveAll();             
                }

            }
            get { return _ToolTip; }
        }

        public NLabel()
        {
            InitializeComponent();           
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Graphics graphic = e.Graphics;
            LinearGradientBrush linerbru = new LinearGradientBrush(new Rectangle(0, 0, this.Width, (int)(this.Height * 1.5)), SystemColors.Control, SystemColors.Window, LinearGradientMode.Vertical);
            graphic.FillRectangle(linerbru, this.ClientRectangle); 
            if (this._Image != null)
            {
                graphic.DrawImage(this._Image, new Point(6, 4));
            }
            if (!string.IsNullOrEmpty(this._Text))
            {
                graphic.DrawString(this._Text, this.Font, Brushes.Black, new Point(26, 3));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStripSeparator : ToolStripSeparator
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            Pen pen = new Pen(NToolStripItemTheme.StripSeparatorColor, 1.0f);
            Graphics g = e.Graphics;
            Rectangle bound = e.ClipRectangle;

            g.DrawLine(pen, bound.Left + bound.Width / 2 - 0.5f, bound.Top + 4,
                bound.Left + bound.Width / 2 - 0.5f, bound.Top + 4 + bound.Height - 8);

            pen.Dispose();
        }
    }
}

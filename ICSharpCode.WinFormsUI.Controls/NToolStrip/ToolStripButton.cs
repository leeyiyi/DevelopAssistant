﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStripButton : System.Windows.Forms.ToolStripButton
    {
        private int iconSize = 16;

        internal NToolStripButton(string text, System.Drawing.Bitmap icon) : base(text, icon)
        {
            this.Text = text;
            this.Image = icon;
            //this.Font = NToolStripItemTheme.TextFont;
        }

        public NToolStripButton()
        {
            //this.Font = NToolStripItemTheme.TextFont;
        }       

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);

            Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);
            Rectangle bound = new Rectangle(0, 0, Width, Height);

            if (this.Pressed)
            {
                textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
            }
            else
            {
                if (this.Selected)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                }
            }

            if (!this.Enabled)
            {
                textBru = new SolidBrush(NToolStripItemTheme.DisableColor);
            }

            int pos = 0;
            if (this.Image != null && (this.DisplayStyle == ToolStripItemDisplayStyle.Image || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                Image image = this.Image;
                if (!this.Enabled)
                {
                    e.Graphics.DrawImage(CommonHelper.CreateDropColorImage(image, this.Owner.BackColor), new Rectangle(pos + 2, (this.Height - iconSize) / 2 + 1, iconSize, iconSize),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                }
                else
                {
                    e.Graphics.DrawImage(CommonHelper.CreateCoverColorImage(image, this.Owner.ForeColor), new Rectangle(pos + 2, (this.Height - iconSize) / 2 + 1, iconSize, iconSize),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                }                
                pos = pos + iconSize + 4;
            }

            if (!string.IsNullOrEmpty(this.Text) && (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                e.Graphics.DrawString(this.Text, this.Font, textBru, new Rectangle(pos, 0, this.Width - iconSize - 4, this.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
            }
        }

    }

    public class NToolStripDropDownButton : System.Windows.Forms.ToolStripDropDownButton
    {
        private int iconSize = 16;

        public NToolStripDropDownButton()
        {
            //this.Font = NToolStripItemTheme.TextFont;
        }

        protected override ToolStripDropDown CreateDefaultDropDown()
        {
            ToolStripDropDown toolStripDropDown = new NToolStripDropDownMenu();
            return toolStripDropDown;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);

            Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);
            Rectangle bound = new Rectangle(0, 0, Width, Height);

            if (this.Pressed)
            {
                textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), bound);
            }
            else
            {
                if (this.Selected)
                {
                    textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                    e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
                }
            }

            int pos = 0;
            if (this.Image != null && (this.DisplayStyle == ToolStripItemDisplayStyle.Image || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                e.Graphics.DrawImage(this.Image, new Rectangle(pos + 2, (this.Height - iconSize) / 2 + 1, iconSize, iconSize),
                    new Rectangle(0, 0, this.Image.Width, this.Image.Height), GraphicsUnit.Pixel);
                pos = pos + iconSize + 4;
            }

            if (!string.IsNullOrEmpty(this.Text) && (this.DisplayStyle == ToolStripItemDisplayStyle.Text || this.DisplayStyle == ToolStripItemDisplayStyle.ImageAndText))
            {
                e.Graphics.DrawString(this.Text, this.Font, textBru, new Rectangle(pos, 0, this.Width - iconSize - 4, this.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
            }

            Point[] pntArr = new Point[] {
                new Point(this.Width-4-8,8),
                new Point(this.Width-4,8),
                new Point(this.Width-4-8/2,12)
            };

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.FillPolygon(textBru, pntArr, System.Drawing.Drawing2D.FillMode.Winding);

            if (this.Pressed)
            {
                e.Graphics.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(0, 0, Width - 1, Height));
            }

        }
    }

    public class NAddInToolStripItem : NToolStripButton
    {
        public NAddInToolStripItem(string text, System.Drawing.Bitmap icon) 
        {
            this.Text = text;
            this.Image = icon;
        }
    }
}

﻿using System;
using System.Drawing;

namespace ICSharpCode.WinFormsUI.Controls
{
    public struct NToolStripItemTheme
    {
        public static string Name { get; set; }

        public static Color TextColor { get; set; }

        public static Color BackColor { get; set; }

        public static Color BaseColor { get; set; }

        public static Color HighlightBackColor { get; set; }

        public static Color HighlightForeColor { get; set; }

        public static Color DisableColor { get; set; }

        public static Color PressedColor { get; set; }

        public static Color StripSeparatorColor { get; set; }

        public static Font TextFont = new Font("微软雅黑", 10.0f);

        public static Color GripDotsColor = Color.FromArgb(190, 190, 190);

    }
}

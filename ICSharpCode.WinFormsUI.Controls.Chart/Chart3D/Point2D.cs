﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls.Chart3D
{
    public class Point2D
    {
        private float _x;
        public float X
        {
            get { return _x; }
            set { _x = value; }
        }
        private float _y;
        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        private float _module;
        internal float Module
        {
            get { return _module; }
            set { _module = value; }
        }

        public Point2D(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public System.Drawing.PointF ToPointF()
        {
            return new System.Drawing.PointF(this.X, this.Y);
        }      

    }

    public static class extensions
    {
        public static Point2D Offset(this Point2D pt2d, float X, float Y)
        {
            Point2D Point2D = new Point2D(pt2d.X, pt2d.Y);
            Point2D.X += X;
            Point2D.Y += Y;
            return Point2D;
        }

        public static System.Drawing.PointF Offset(this System.Drawing.PointF ptf, float X, float Y)
        {
            System.Drawing.PointF PointF = new System.Drawing.PointF(ptf.X, ptf.Y);
            PointF.X += X;
            PointF.Y += Y;
            return PointF;
        }

    }
}

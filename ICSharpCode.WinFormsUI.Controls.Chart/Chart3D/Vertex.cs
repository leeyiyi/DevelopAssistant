﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls.Chart3D
{
    public class Vertex
    {
        private float _x;
        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        private float _y;
        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        private float _z;
        public float Z
        {
            get { return _z; }
            set { _z = value; }
        }

        /// <summary>
        /// Z,X,Y
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Z"></param>
        public Vertex(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Vertex Copy()
        {
            return new Vertex(X, Y, Z);
        }

        public Vertex Offset(float x, float y, float z)
        {
            return new Vertex(X + x, Y + y, Z + z);
        }

    }
}

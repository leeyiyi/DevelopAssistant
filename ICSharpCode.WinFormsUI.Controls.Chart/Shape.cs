﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls.Chart
{
    public class Shape
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected float lineWidth = 1.0f;
        public float LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        protected Color lineColor = Color.Black;
        public Color LineColor
        {
            set { lineColor = value; }
            get { return lineColor; }
        }

    }
}

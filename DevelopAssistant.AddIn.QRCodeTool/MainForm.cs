﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;

namespace DevelopAssistant.AddIn.QRCodeTool
{
    public partial class MainForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        private string themeName = string.Empty;

        public MainForm(string xtheme)
        {
            themeName = xtheme;
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.tabControl1.Radius = 1;
            this.tabControl1.TabCaptionLm = -3;
            this.tabControl1.ItemSize = new System.Drawing.Size(86, 28);
            this.tabControl1.Alignment = TabAlignment.Bottom;
            this.tabControl1.BaseColor = SystemColors.Control;
            this.tabControl1.BackColor = SystemColors.Control;
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase theme =
                new ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase();
            switch (themeName)
            {
                case "Mac":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeMac();
                    break;
                case "VS2012":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeVS2012();
                    break;
                case "Shadow":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeShadow();
                    break;
            }            
            this.XTheme = theme;
            panel3.BorderColor = XTheme.FormBorderOutterColor;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {                      
            OnThemeChanged(new EventArgs());
            this.Resizable = false;
            this.txtPostFilePath.Enabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QRCodeContent"></param>
        /// <param name="Icon"></param>
        /// <returns></returns>
        public Image CreateQRCode(string QRCodeContent, Bitmap Icon)
        {
            QRCodeEncoder endocder = new QRCodeEncoder();
            //二维码背景颜色
            endocder.QRCodeBackgroundColor = System.Drawing.Color.White;
            //二维码编码方式
            endocder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            //每个小方格的宽度
            endocder.QRCodeScale = 10;
            //二维码版本号
            endocder.QRCodeVersion = 5;
            //纠错等级
            endocder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;

            Bitmap bitmap = endocder.Encode(QRCodeContent, System.Text.Encoding.UTF8);

            return bitmap;

        }

        public string GetContentFromQRCode(Bitmap image)
        {
            QRCodeDecoder decoder = new QRCodeDecoder();
            string result = decoder.decode(new ThoughtWorks.QRCode.Codec.Data.QRCodeBitmapImage(image), System.Text.Encoding.UTF8);
            return result;
        }

        public byte[] ConvertToBytes(Bitmap QRCodeImage)
        {
            MemoryStream ms = new MemoryStream();
            QRCodeImage.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }

        private void BtnApply_Click(object sender, EventArgs e)
        {
            try
            {
                string Content = this.txtContent.Text;
                Image QRCodeImage = this.InputQRCodeImage.BackgroundImage;

                int tabControlSelectedIndex = tabControl1.SelectedIndex;

                System.Threading.Thread th = new System.Threading.Thread(delegate () {

                    try
                    {
                        this.BtnApply.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                        {
                            this.BtnApply.Enabled = false;
                        }));

                        if (tabControlSelectedIndex == 0)
                        {
                            this.BtnApply.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                            {
                                this.OutQRCodeImage.BackgroundImage = null;
                            }));

                            Image result = CreateQRCode(this.txtContent.Text, null);

                            this.txtContent2.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                            {
                                if (!string.IsNullOrEmpty(Content))
                                    OutQRCodeImage.BackgroundImage = result;
                            }));
                        }
                        else
                        {
                            this.BtnApply.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                            {
                                this.txtContent2.Text = "";
                            }));

                            string result = GetContentFromQRCode((Bitmap)this.InputQRCodeImage.BackgroundImage);

                            this.txtContent2.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                            {
                                if (QRCodeImage != null)
                                    this.txtContent2.Text = result;
                            }));
                        }

                        this.BtnApply.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                        {
                            this.BtnApply.Enabled = true;
                        }));

                    }
                    catch (Exception ex)
                    {
                        NLogger.WriteToLine(ex.Message, "error", DateTime.Now, ex.Source, ex.StackTrace);
                        this.BtnApply.Invoke(new System.Windows.Forms.MethodInvoker(delegate ()
                        {
                            this.BtnApply.Enabled = true;
                        }));
                        MessageBox.Show(null, "系统提示", ex.Message);
                    }

                });
                th.IsBackground = true;
                th.Start();
            }
            catch (Exception ex)
            {
                NLogger.WriteToLine(ex.Message, "error", DateTime.Now, ex.Source, ex.StackTrace);
                //ConsoleHelper.WriteLine("error", ex.Message, ex.Source, ex.StackTrace);
            }
        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openfileDialog = new System.Windows.Forms.OpenFileDialog();
            openfileDialog.Filter = "*.jpg|*.jpg|*.png|*.png|*.gif|*.gif|*.bmp|*.bmp|所有文件|*.*";
            try
            {
                if (openfileDialog.ShowDialog(this).Equals(System.Windows.Forms.DialogResult.OK))
                {
                    string path = openfileDialog.FileName;
                    this.txtPostFilePath.Text = path;
                    if (System.IO.File.Exists(path))
                    {
                        Image image = Image.FromFile(path);
                        InputQRCodeImage.BackgroundImage = image;
                    }
                }
            }
            catch (Exception ex)
            {
                NLogger.WriteToLine(ex.Message, "error", DateTime.Now, ex.Source, ex.StackTrace);
                //ConsoleHelper.WriteLine("error", ex.Message, ex.Source, ex.StackTrace);
            }
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color toolBackColor = SystemColors.Control;
            Color textBackColor = SystemColors.Window;
            Color toolBorderColor = SystemColors.ControlLight;
            Color toolStripBackColor = SystemColors.ControlLight;
            Color disableColor = SystemColors.Control;
            Color buttonBackColor = SystemColors.Control;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    textBackColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    toolBorderColor = SystemColors.ControlLight;
                    disableColor = SystemColors.Control;
                    buttonBackColor = Color.FromArgb(246, 248, 250);
                    tabControl1.ForeColor = Color.Black;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = SystemColors.Control;
                    tabControl1.BackColor = SystemColors.Control;
                    tabControl1.SelectedColor = SystemColors.Control;
                    tabControl1.BaseTabColor = SystemColors.Control;
                    toolStripBackColor = Color.FromArgb(246, 248, 250);
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    buttonBackColor = Color.FromArgb(038, 038, 038);
                    toolBorderColor = SystemColors.ControlDark;
                    tabControl1.ForeColor = Color.FromArgb(240, 240, 240);
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    toolStripBackColor = Color.FromArgb(062, 062, 062);
                    break;
            }

            panel1.ForeColor = foreColor;
            panel1.BackColor = textBackColor;
            panel2.ForeColor = foreColor;
            panel2.BackColor = textBackColor;
            panel3.ForeColor = foreColor;
            panel3.BackColor = toolBackColor;

            this.XBackgroundColor = backColor;

            this.tabPage1.ForeColor = foreColor;
            this.tabPage1.BackColor = backColor;
            this.tabPage2.ForeColor = foreColor;
            this.tabPage2.BackColor = backColor;             
             
            this.txtContent.XForeColor = foreColor;
            this.txtContent.XBackColor = textBackColor;
            this.txtContent2.XForeColor = foreColor;
            this.txtContent2.XBackColor = textBackColor;
            this.txtPostFilePath.XForeColor = foreColor;
            this.txtPostFilePath.XBackColor = toolStripBackColor;
            this.txtPostFilePath.XDisableColor = disableColor;

            this.BtnApply.ForeColor = foreColor; 
            this.BtnApply.BackColor = buttonBackColor;
            this.btnUploadImage.ForeColor = foreColor;
            this.btnUploadImage.BackColor = buttonBackColor;
        }
    }
}

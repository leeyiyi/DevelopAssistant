﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;

namespace DevelopAssistant.AddIn.QRCodeTool
{
    using DevelopAssistant.AddIn.QRCodeTool.Properties;     

    public class QRCodeToolAddIn : WindowAddIn 
    {
        public QRCodeToolAddIn()
        {
            this.IdentityID = "81e45912-2f14-472a-8387-8b408f29f653";
            this.Name = "二维码";
            this.Text = "二维码";
            this.Tooltip = "二维码工具";            
            this.Icon = Resources.scan_24px;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm(Parameter[2].ToString())
            { 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn
{
    public class WindowAddIn : AddInBase
    {
        public WindowAddIn()
        {
            HostAssemblyName = "DevelopAssistant.Core.dll";
            Position = "/DevelopAssistant/MainForm/MainMenu/Tools";
            TypeName = "DevelopAssistant.AddIn.WindowAddIn";
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            base.Initialize(AssemblyName, Winname);
        }

    }

    public class DockContentAddIn : AddInBase
    {
        public DockContentAddIn()
        {
            HostAssemblyName = "DevelopAssistant.Core.dll";
            Position = "/DevelopAssistant/MainForm/MainMenu/Tools";
            TypeName = "DevelopAssistant.AddIn.DockContentAddIn";
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            base.Initialize(AssemblyName, Winname);
        }
    }    

    public class ExecuteFileAddIn : AddInBase
    {
        public ExecuteFileAddIn()
        {
            HostAssemblyName = "DevelopAssistant.Core.dll";
            Position = "/DevelopAssistant/MainForm/MainMenu/Tools";
            TypeName = "DevelopAssistant.AddIn.ExecuteFileAddIn";
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //base.Initialize(AssemblyName, Winname);
        }

        public ExecuteFileAddIn(string ExecuteFile)
            : this()
        {
            this.ExecuteFile = ExecuteFile;
        }

    }
}

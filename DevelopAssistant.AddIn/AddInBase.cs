﻿using System;
using System.Collections.Generic; 
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn
{
    public class AddInBase  
    {
        #region 属性

        public string IdentityID { get; set; }

        public System.Drawing.Bitmap Icon { get; set; }

        public string Name { get; set; }
        public string Text { get; set; }
        public string Tooltip { get; set; }
        public string TypeName { get; protected set; }
        /// <summary>
        /// 位置描述 XML
        /// </summary>
        public string Position { get; set; }
        public string ExecuteFile { get; set; }
        public bool Uninstall { get; set; }

        public CopyRight Copyright { get; set; }

        #endregion

        protected String HostAssemblyName = string.Empty;

        protected System.Reflection.Assembly HostAssembly { get; set; }

        protected System.Windows.Forms.Form HostWindow { get; set; }

        /// <summary>
        /// 插件初始化
        /// </summary>
        /// <param name="AssemblyName">宿信程序集名称</param>
        /// <param name="Winname">宿主程序主窗体名称</param>
        public virtual void Initialize(string AssemblyName, string Winname)
        {
            HostAssemblyName = AssemblyName + ".dll"; 
            if (HostAssembly == null)
            {
                if (!string.IsNullOrEmpty(HostAssemblyName))
                {
                    HostAssembly = GetAssembly(AppDomain.CurrentDomain.BaseDirectory);
                }
            }
            if (HostWindow == null && HostAssembly != null)
            {
                if (!string.IsNullOrEmpty(HostAssemblyName))
                {
                    HostWindow = (System.Windows.Forms.Form)HostAssembly.CreateInstance(AssemblyName + "." + Winname + "", false);
                }
            }
        }

        /// <summary>
        /// 插件执行
        /// </summary>
        /// <param name="Parameter">OwnerForm/DockPanel/LeftTree/TextEditor</param>
        /// <returns></returns>
        public virtual object Execute(params object[] Parameter)
        {
            return null;
        }

        public virtual void ExecuteExe(string ExecutePath)
        {
            ExecutePath = string.Format("{0}\\{1}", Application.StartupPath, ExecutePath);
            if (System.IO.File.Exists(ExecutePath))
                System.Diagnostics.Process.Start(ExecutePath);
        }

        public virtual object GetData(params object[] Parameter)
        {
            return null;
        }
 
        protected System.Reflection.Assembly GetAssembly(string path)
        {
            if (string.IsNullOrEmpty(HostAssemblyName))
                throw new Exception("宿主引用集名称为空");
            return System.Reflection.Assembly.LoadFile(path + "\\" + HostAssemblyName);
        }

    }

    public class CopyRight
    {
        public string Author { get; set; }
        public string Copyright { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.DataPie
{   
    public class DataBaseFileInfo
    {
        public string Logical_Name { get; set; }
        public string Physical_Name { get; set; }
        public double Maxsize { get; set; }
        public double Size { get; set; }
    }
}

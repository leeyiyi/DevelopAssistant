﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;

namespace DevelopAssistant.AddIn.DataPie
{
    [ToolboxItem(false)]
    public partial class Script : UserControl
    {
        public Script()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            attachmentColumn.DefaultCellStyle.NullValue = null;
            
            // load image strip
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.ImageSize = new Size(16, 16);
            this.imageList1.Images.AddStrip(Properties.Resources.newGroupPostIconStrip);
            this.attachmentColumn.HeaderCell = new AttachmentColumnHeader(imageList1.Images[2]);

        }

        public void BindInfo(DataBaseServer dataBaseServer, DataSet dataSetSource)
        {
            using (DataBaseHelper db = new DataBaseHelper(dataBaseServer.ConnectionString, dataBaseServer.ProviderName))
            {
                DataTable dataTypeSource = dataSetSource.Tables["dataType"];

                TreeGridNode tableNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[0]["Name"] + "", dataTypeSource.Rows[0]["Objects"] + "", "");
                TreeGridNode viewNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[1]["Name"] + "", dataTypeSource.Rows[1]["Objects"] + "", "");
                TreeGridNode procedureNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[2]["Name"] + "", dataTypeSource.Rows[2]["Objects"] + "", "");
                TreeGridNode functionNode = this.nTreeDataGridView1.Nodes.AddNode(1, null, dataTypeSource.Rows[3]["Name"] + "", dataTypeSource.Rows[3]["Objects"] + "", "");
            
                DataTable dataTableSource = dataSetSource.Tables["DataList"];

                foreach (DataRow row in dataTableSource.Rows)
                {
                    string ObjectType = row["ParentId"] + "";
                    TreeGridNode node = null;
                    switch (ObjectType)
                    {
                        case "1":
                            node = tableNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                            break;
                        case "2":
                            node = viewNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                            break;
                        case "3":
                            node = procedureNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                            break;
                        case "4":
                            node = functionNode.Nodes.AddNode(Int32.Parse(row["Id"] + ""), null, row["Name"] + "", row["Objects"] + "", "");
                            break;
                    }


                    if (node != null)
                    {
                        DataRow[] rows = dataTableSource.Select("ParentId='" + node.Id + "'");

                        foreach (DataRow rrow in rows)
                        {
                            var node_node = node.Nodes.AddNode(Int32.Parse(rrow["Id"] + ""), false, null, rrow["Name"] + "", rrow["Objects"] + "", rrow["Describ"] + "");
                            node_node.Image = imageList1.Images[0];
                        }
                    }
                    

                }

                db.Dispose();
            }            
        }       

        private void tvTaskList_NodeChecked(object sender, CheckedEventArgs e)
        {
            checkedNode(e.Node);
        }

        private void checkedNode(TreeGridNode node)
        {
            checkedParentNode(node);
            checkedChildsNode(node);
            nTreeDataGridView1.Refresh();
        }

        private void checkedParentNode(TreeGridNode node)
        {
            if (node.Level < 1)
                return;

            TreeGridCell cell = (TreeGridCell)node.Cells[1];

            if (node.Parent == null)
                return;

            if (cell.Checked)
            {
                ((TreeGridCell)(node.Parent.Cells[1])).Checked = cell.Checked;
                checkedParentNode(node.Parent);
            }
            else
            {
                bool allunchecked = true;
                foreach (TreeGridNode sub_node in node.Parent.Nodes)
                {
                    if (((TreeGridCell)(sub_node.Cells[1])).Checked)
                    {
                        allunchecked = false;
                        break;
                    }
                }
                if (allunchecked)
                {
                    ((TreeGridCell)(node.Parent.Cells[1])).Checked = false;
                    checkedParentNode(node.Parent);
                }
            }

        }

        private void checkedChildsNode(TreeGridNode node)
        {
            TreeGridCell cell = (TreeGridCell)node.Cells[1];

            if (node.Nodes.Count > 0)
            {
                foreach (TreeGridNode sub_node in node.Nodes)
                {
                    ((TreeGridCell)(sub_node.Cells[1])).Checked = cell.Checked;
                    checkedChildsNode(sub_node);
                }
            }
        }

        private void nTreeDataGridView1_NodeChecked(object sender, CheckedEventArgs e)
        {
            checkedNode(e.Node);
        }

    }
}

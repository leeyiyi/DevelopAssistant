﻿namespace DevelopAssistant.AddIn.DataPie
{
    partial class Script
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Script));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new ICSharpCode.WinFormsUI.Controls.NButton();           
            this.nTreeDataGridView1 = new ICSharpCode.WinFormsUI.Controls.NTreeDataGridView();
            this.attachmentColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.ObjectName = new ICSharpCode.WinFormsUI.Controls.TreeGridColumn();
            this.ObjectsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Describ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTreeDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 381);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 61);
            this.panel1.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(457, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 32);
            this.button2.TabIndex = 0;
            this.button2.Text = "生成SQL脚本";
            this.button2.UseVisualStyleBackColor = true;            
            // 
            // nTreeDataGridView1
            // 
            this.nTreeDataGridView1.AllowUserToAddRows = false;
            this.nTreeDataGridView1.AllowUserToDeleteRows = false;
            this.nTreeDataGridView1.AllowUserToResizeRows = false;
            this.nTreeDataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nTreeDataGridView1.CheckBoxes = true;
            this.nTreeDataGridView1.ColumnHeadersHeight = 26;
            this.nTreeDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.nTreeDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.attachmentColumn,
            this.ObjectName,
            this.ObjectsCount,
            this.Describ});
            this.nTreeDataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.nTreeDataGridView1.GridColor = System.Drawing.SystemColors.ControlLight;
            this.nTreeDataGridView1.ImageList = this.imageList1;
            this.nTreeDataGridView1.Location = new System.Drawing.Point(6, 7);
            this.nTreeDataGridView1.Name = "nTreeDataGridView1";
            this.nTreeDataGridView1.RowHeadersVisible = false;
            this.nTreeDataGridView1.Size = new System.Drawing.Size(588, 366);
            this.nTreeDataGridView1.TabIndex = 13;
            this.nTreeDataGridView1.NodeChecked += new ICSharpCode.WinFormsUI.Controls.CheckedEventHandler(this.nTreeDataGridView1_NodeChecked);
            // 
            // attachmentColumn
            // 
            this.attachmentColumn.HeaderText = "";
            this.attachmentColumn.MinimumWidth = 25;
            this.attachmentColumn.Name = "attachmentColumn";
            this.attachmentColumn.Width = 25;
            // 
            // ObjectName
            // 
            this.ObjectName.DefaultNodeImage = null;
            this.ObjectName.HeaderText = "名称";
            this.ObjectName.Name = "ObjectName";
            this.ObjectName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ObjectName.Width = 200;
            // 
            // ObjectsCount
            // 
            this.ObjectsCount.HeaderText = "对象个数";
            this.ObjectsCount.Name = "ObjectsCount";
            this.ObjectsCount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ObjectsCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ObjectsCount.Width = 80;
            // 
            // Describ
            // 
            this.Describ.HeaderText = "描述";
            this.Describ.Name = "Describ";
            this.Describ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Describ.Width = 260;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "file_16px.png");
            // 
            // Script
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nTreeDataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "Script";
            this.Size = new System.Drawing.Size(600, 442);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nTreeDataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton button2;      
        private ICSharpCode.WinFormsUI.Controls.NTreeDataGridView nTreeDataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn attachmentColumn;
        private ICSharpCode.WinFormsUI.Controls.TreeGridColumn ObjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Describ;
        private System.Windows.Forms.ImageList imageList1;
    }
}

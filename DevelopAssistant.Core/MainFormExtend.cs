﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic; 

using ICSharpCode.WinFormsUI.Theme;
using ICSharpCode.WinFormsUI.Docking;

using DevelopAssistant.Core.Contents;
using DevelopAssistant.Core.DBMS;
using DevelopAssistant.Core.ToolBar;
using DevelopAssistant.Core.ToolBox;
using DevelopAssistant.Common;
using DevelopAssistant.Service;
using DevelopAssistant.AddIn;
using ICSharpCode.WinFormsUI.Controls.NTimeLine;

namespace DevelopAssistant.Core
{
    /// <summary>
    /// 公共方法
    /// </summary>
    public partial class MainForm
    {
        #region 打开执行文件

        public void ExecuteFile(string ExecutePath)
        {
            ExecutePath = string.Format("{0}\\{1}", Application.StartupPath, ExecutePath);
            if (System.IO.File.Exists(ExecutePath))
                System.Diagnostics.Process.Start(ExecutePath);
        }

        #endregion

        #region 执行插件

        public void ExecuteAddIn(AddInBase plugin)
        {            
            if (plugin != null)
            {
                string TypeName = plugin.TypeName;
                switch (TypeName)
                {
                    case "DevelopAssistant.AddIn.WindowAddIn":
                        var window = plugin.Execute(this, this.dockPanel,AppSettings.WindowTheme.Name,AppSettings.EditorSettings.TSQLEditorTheme);
                        AddModalWindow((Form)window, plugin.Text);
                        break;
                    case "DevelopAssistant.AddIn.DockContentAddIn":
                        var dock = plugin.Execute(this, this.dockPanel, AppSettings.WindowTheme.Name, AppSettings.EditorSettings.TSQLEditorTheme);
                        AddNewContent((DockContent)dock, plugin.Text);
                        break;
                    case "DevelopAssistant.AddIn.ExecuteFileAddIn":
                        plugin.ExecuteExe(plugin.ExecuteFile);
                        break;
                }
            }
        }

        public void ExecuteAddIn(AddInBase plugin,params object[] argments)
        {
            if (plugin != null)
            {
                string Tooltip = plugin.Tooltip;
                if (Tooltip.Contains("[=数所库名]"))
                {
                    Tooltip = Tooltip.Replace("[=数所库名]", ((DataBaseServer)argments[2]).DataBaseName);
                }
                string TypeName = plugin.TypeName;
                switch (TypeName)
                {
                    case "DevelopAssistant.AddIn.WindowAddIn":
                        var window = plugin.Execute(argments);
                        AddModalWindow((Form)window, Tooltip);
                        break;
                    case "DevelopAssistant.AddIn.DockContentAddIn":
                        var dock = plugin.Execute(argments);
                        AddNewContent((DockContent)dock, Tooltip);
                        break;
                    case "DevelopAssistant.AddIn.ExecuteFileAddIn":
                        plugin.ExecuteExe(plugin.ExecuteFile);
                        break;
                }
            }
        }

        #endregion

        #region Tab 操作

        public void CloseAllContents(bool closed = false)
        {
            if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form form in MdiChildren)
                {
                    if (form.Name != "MenuBar" && form.Name != "HomeContent" &&
                        form.Name != "SnippetBar" && form.Name != "TodoBar")
                        form.Close();
                    else if (closed)
                        form.Close();
                }
            }
            else
            {
                for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
                {
                    if (dockPanel.Contents[index] is IDockContent)
                    {
                        IDockContent content = (IDockContent)dockPanel.Contents[index];
                        if (((DockContent)content).Name != "MenuBar" && ((DockContent)content).Name != "HomeContent" &&
                            ((DockContent)content).Name != "SnippetBar" && ((DockContent)content).Name != "TodoBar")
                            content.DockHandler.Close();
                        else if (closed)
                            content.DockHandler.Close();
                    }
                }
            }
        }

        public void CloseContent(DockContent dcnt)
        {
            this.Invoke(new MethodInvoker(delegate()
            {
                if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                {
                    dcnt.Close();
                }
                else
                {
                    dcnt.Close();
                }

            }));
        }

        public void CloseOtherContent()
        {
            if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form form in MdiChildren)
                    if (form.Name != "MenuBar" && form.Name != "HomeContent" &&
                        form.Name != "SnippetBar")
                        form.Close();
            }
            else
            {
                for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
                {
                    if (dockPanel.Contents[index] is IDockContent)
                    {
                        IDockContent content = (IDockContent)dockPanel.Contents[index];
                        if (!content.Equals((DockContent)dockPanel.ActiveDocument))
                            if (((DockContent)content).Name != "MenuBar" && ((DockContent)content).Name != "HomeContent" &&
                                ((DockContent)content).Name != "SnippetBar")
                                content.DockHandler.Close();
                    }
                }
            }
        }

        private IDockContent GetContentFromPersistString(string persistString)
        {
            DockContent dummyDoc = new DockContent();
            switch (persistString)
            {
                case "DevelopAssistant.Core.ToolBar.TodoBar":
                    this.TodoBar = new DevelopAssistant.Core.ToolBar.TodoBar(this)
                    {
                        AutoHidePortion = 196,
                        AllowEndUserDocking = false
                        //CloseButtonVisible = false
                    };
                    dummyDoc = this.TodoBar;
                    break;
                case "DevelopAssistant.Core.ToolBar.SnippetBar":
                    this.snippetBar = new DevelopAssistant.Core.ToolBar.SnippetBar()
                    {
                        CloseButtonVisible = false,
                        AllowEndUserDocking = false,
                        AutoHidePortion = 196,
                        IsHidden = false,
                    };
                    dummyDoc = this.snippetBar;
                    break;
                case "DevelopAssistant.Core.ToolBar.MenuBar":
                    this.MenuBar = new DevelopAssistant.Core.ToolBar.MenuBar(this,contextMenuStrip3)
                    {
                        CloseButtonVisible = false,
                        AllowEndUserDocking = false,
                        AutoHidePortion = 196,
                        IsHidden = false,
                    };
                    dummyDoc = this.MenuBar;
                    break;
                case "DevelopAssistant.Core.ToolBar.LoginBar":
                    dummyDoc = new DevelopAssistant.Core.ToolBar.LoginBar()
                    {
                        CloseButtonVisible = false,
                        AllowEndUserDocking = false,
                        AutoHidePortion = 196,
                        IsHidden = false,
                    };
                    break;
                case "DevelopAssistant.Core.Contents.HomeContent":
                    dummyDoc = new DevelopAssistant.Core.Contents.HomeContent(this) { CloseButtonVisible = false };
                    dummyDoc.TabPageContextMenuStrip = contextMenuStrip2;
                    homeContent = (HomeContent)dummyDoc;
                    break;
                case "DevelopAssistant.Core.DBMS.QueryDocument":
                    dummyDoc = new DevelopAssistant.Core.DBMS.QueryDocument(this);
                    dummyDoc.TabPageContextMenuStrip = contextMenuStrip1;
                    break;
                default:
                    dummyDoc = null;
                    break;
            }
            return dummyDoc;
        }

        public DockContent AddNewContent(DockContent dcnt, string text)
        {
            dcnt.Text = text;
            if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                dcnt.MdiParent = this;
                dcnt.Show();
            }
            else
                dcnt.Show(dockPanel);
            dcnt.TabPageContextMenuStrip = contextMenuStrip1;
            return dcnt;
        }

        public DockContent AddSingleContent(DockContent dcnt, string text)
        {                
            foreach (DockContent content in dockPanel.Contents)
            {
                string cnt_name = content.Name;
                if (cnt_name.Equals(dcnt.Name))
                {
                    dcnt = content;
                    break;
                }
            }
            dcnt.Text = text; 
            if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                dcnt.MdiParent = this;
                dcnt.Show();
            }
            else
                dcnt.Show(dockPanel);
            dcnt.TabPageContextMenuStrip = contextMenuStrip1;
            return dcnt;
        }

        public DockContent AddSingleContent(DockContent dcnt, DockStyle dockstyle)
        {
            int selectedIndex = 0;
            foreach (DockContent content in dockPanel.Contents)
            {
                string cnt_name = content.Name;
                if (cnt_name.Equals(dcnt.Name))
                {
                    dcnt = content;
                    break;
                }
                selectedIndex++;
            }
            dcnt.Show(this.dockPanel);
            dcnt.DockTo(this.dockPanel, dockstyle);//设置dcnt在Form1的DockPanel容器中的停靠方式：共有上下左右，以及None五种
            dcnt.TabPageContextMenuStrip = contextMenuStrip1;
            return dcnt;
        }

        public DockContent AddFloatWindow(DockContent dcnt, string text)
        {
            dcnt.DockHandler.DockPanel = dockPanel;
            int h = dcnt.Height, w = dcnt.Width,
                f = dockPanel.GetDockWindowSize(DockState.DockLeft);//196/2           
            if (this.Width - f - w - 10 < 0)
                f = 0;
            int l = this.Location.X + f / 2 + (this.Width - w) / 2;
            int t = this.Location.Y + (this.Height - h) / 2;             
            AddNewContent(dcnt, text);            
            dcnt.FloatAt(new Rectangle(l, t, w, h));
            dcnt.IsFloat = true;
            return dcnt;            
        }

        public Form AddModalWindow(Form window, string text)
        {
            window.ShowDialog(this);
            return window;
        }

        public void SetWindowAtDockPanCenter(Form window)
        {
            int w = dockPanel.GetDockWindowSize(DockState.DockLeft);//196/2
            if (this.Width - w - window.Width - 10 < 0)
                w = 0;
            int l = this.Location.X + w / 2 + (this.Width - window.Width) / 2;
            int t = this.Location.Y + (this.Height - window.Height) / 2;
            window.SetBounds(l, t, window.Width, window.Height);
        }

        public void SetSeletedContent(DockContent dnt)
        {
            int selectedIndex = 0;
            foreach (DockContent content in dockPanel.Contents)
            {
                string cnt_name = content.Name;
                if (content.Name.Equals(dnt.Name))
                {
                    this.Invoke(new MethodInvoker(delegate() { content.Show(dockPanel); }));
                    break;
                }
                selectedIndex++;
            }
        }

        public IDockContent GetContent(string name)
        {
            IDockContent dnt = null;

            for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
            {
                if (dockPanel.Contents[index] is IDockContent)
                {
                    IDockContent content = (IDockContent)dockPanel.Contents[index];
                    if (((DockContent)content).Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                    {
                        dnt = content;
                        return dnt;
                    }
                       
                }
            }

            return dnt;
        }

        public IDockContent GetContent(string name,bool last)
        {
            IDockContent dnt = null;

            for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
            {
                if (dockPanel.Contents[index] is IDockContent)
                {
                    IDockContent content = (IDockContent)dockPanel.Contents[index];
                    if (((DockContent)content).Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                    {
                        dnt = content;
                        if (!last)
                            return dnt;
                    }

                }
            }

            return dnt;
        }

        public List<IDockContent> GetContents(string name)
        {
            List<IDockContent> contents = new List<IDockContent>();

            for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
            {
                if (dockPanel.Contents[index] is IDockContent)
                {
                    IDockContent content = (IDockContent)dockPanel.Contents[index];
                    if (((DockContent)content).Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                    {
                        contents.Add(content);                         
                    }
                }
            }

            return contents;
        }

        public List<IDockContent> GetContents()
        {
            List<IDockContent> contents = new List<IDockContent>();
            for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
            {
                contents.Add(dockPanel.Contents[index]);
            }
            return contents;
        }

        public DockContent GetSelectedContent()
        {
            DockContent dnt = (DockContent)dockPanel.ActiveContent;            
            return dnt;
        }

        public DockContent GetCurrentQueryForm()
        {
            string name = "QueryDocument";
            DockContent dnt = null;

            var activeDocument = this.dockPanel.ActiveDocument;
            if (activeDocument != null
                && ((DockContent)activeDocument).Name == name)
            {
                dnt = (DockContent)activeDocument;
                this.dockPanel.SetSelectedContent(dnt);
                return dnt;
            }

            for (int index = dockPanel.Contents.Count - 1; index >= 0; index--)
            {
                if (dockPanel.Contents[index] is IDockContent)
                {
                    IDockContent content = (IDockContent)dockPanel.Contents[index];
                    if (((DockContent)content).Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                    {
                        dnt = (DockContent)content;
                        //return dnt;
                        break;
                    }
                }
            }

            if (dnt != null)
                this.dockPanel.SetSelectedContent(dnt);            

            return dnt;
        }

        #endregion

        #region ToolStripButton 操作

        public void SetQueryFormToolBarItemVisible(bool visible, bool tab = false)
        {
            bool _visible = visible;
            if (GetContents("QueryDocument").Count > 1)
                _visible = true;
            SetQueryFormToolBarItemTabVisible(_visible, tab);
        }

        public void SetQueryFormToolBarItemTabVisible(bool visible, bool tab = false)
        {
            bool _visible = visible;

            this.toolStripSeparator2.Visible = _visible;
            this.toolBarFormat.Visible = _visible;
            this.toolBarFinder.Visible = _visible;
            this.toolBarUndo.Visible = _visible;
            this.toolBarRedo.Visible = _visible;
            this.toolBarStop.Visible = _visible;
            this.toolBarAddComment.Visible = _visible;
            this.toolBarRemoveComment.Visible = _visible;
            this.toolBarIndent.Visible = _visible;
            this.toolBarOutIndent.Visible = _visible;
            this.toolBarMinum.Visible = _visible;
            this.toolBarButtonOpen.Visible = _visible;
            this.toolBarButtonSave.Visible = _visible;

            if (this.ConnectedDataBaseServer != null)
            {
                switch (this.ConnectedDataBaseServer.ProviderName)
                {
                    case "System.Data.Sqlite":
                        this.toolBarCommit.Visible = false;
                        this.toolBarRollback.Visible = false;
                        break;
                    default:
                        if (AppSettings.EditorSettings.EnableDataBaseTran)
                        {
                            if (tab)
                            {
                                DockContent act = GetSelectedContent();
                                if (act.GetType().Equals(typeof(QueryDocument)) &&
                                    ((QueryDocument)act).TranBeginning)
                                {
                                    this.toolBarCommit.Visible = _visible;
                                    this.toolBarRollback.Visible = _visible;
                                }
                                else
                                {
                                    this.toolBarCommit.Visible = false;
                                    this.toolBarRollback.Visible = false;
                                }
                            }
                            else
                            {
                                this.toolBarCommit.Visible = _visible;
                                this.toolBarRollback.Visible = _visible;
                            }
                        }
                        else
                        {
                            this.toolBarCommit.Visible = false;
                            this.toolBarRollback.Visible = false;
                        }
                        break;
                }
            }

            this.toolStripSeparator5.Visible = _visible;
            this.toolStripSeparator6.Visible = _visible;
            this.toolStripSeparator7.Visible = _visible;

            this.menuItemEdit.Visible = _visible;
            this.toolBarExcecute.Visible = _visible;
        }

        public void SetQueryFormToolBarItemEnabled(bool enabled)
        {
            bool _enabled = enabled;
            this.toolBarFormat.Enabled = _enabled;
            this.toolBarFinder.Enabled = _enabled;
            this.toolBarExcecute.Enabled = _enabled;
            this.toolBarCommit.Enabled = _enabled;
            this.toolBarRollback.Enabled = _enabled;
            this.toolBarStop.Enabled = !_enabled;
            this.toolBarUndo.Enabled = _enabled;
            this.toolBarRedo.Enabled = _enabled;
            this.toolBarAddComment.Enabled = _enabled;
            this.toolBarRemoveComment.Enabled = _enabled;
            this.toolBarIndent.Enabled = _enabled;
            this.toolBarOutIndent.Enabled = _enabled;
            this.toolBarMinum.Enabled = _enabled;
        }

        public void SetMainFormToolBarEnable(bool enable)
        {
            this.MainToolBar.Enabled = enable;
        }

        public void SetUndoMenuItem(bool enable)
        {
            this.toolBarUndo.Enabled = enable;
        }

        public void SetRedoMenuItem(bool enable)
        {
            this.toolBarRedo.Enabled = enable;
        }

        public void SetSysStatusInfo(string messageText, string tooltipText = null)
        {
            messageText = new StringPlus().CutShortStringCn(messageText, 86, true);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate()
                {
                    this.SysStatusInfo.Text = messageText;
                    if (!string.IsNullOrEmpty(tooltipText))
                    {
                        this.SysStatusInfo.ToolTipText = tooltipText;
                    }
                })); 
            }
            else
            {
                this.SysStatusInfo.Text = messageText;
                if (!string.IsNullOrEmpty(tooltipText))
                {
                    this.SysStatusInfo.ToolTipText = tooltipText;
                }
            }
            Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
                       
        }

        public void AddInContextMenuItem()
        {
          
        }

        #endregion

        #region 数据库操作

        public object DisplayMessage(string Message, string ToolTipText = "")
        {
            SetSysStatusInfo(Message, ToolTipText);           
            return null;
        }

        public void AddDataBaseServer(string Name, string ConnectionString, string ProviderName, bool LoadAllObject = false)
        {
            if (MenuBar != null)
            {
                DataBaseServer dbs = new DataBaseServer(ConnectionString, ProviderName)
                {
                    Name = Name,                    
                    OnConnecting = true,
                    AllObject = LoadAllObject
                };

                if (string.IsNullOrEmpty(Name))
                {
                    string connectionName = dbs.DataBaseName +
                        "(" + dbs.Server + "";
                    if (!string.IsNullOrEmpty(dbs.Port))
                    {
                        connectionName += ":" + dbs.Port + "";
                    }
                    if (!string.IsNullOrEmpty(dbs.Instance_Name))
                    {
                        connectionName += "\\" + dbs.Instance_Name;
                    }
                    connectionName += ")";

                    dbs.Name = connectionName;
                }

                DelegateDisplayMessage callback = new DelegateDisplayMessage(DisplayMessage);
                MenuBar.AddDataBaseConnectionAsync(dbs, callback);
            }
        }      

        #endregion

        #region QueryDocument SnippetCode 操作

        public void SetQueryDocumentText(string Text, bool NewDocument)
        {
            string captionText = "SQLQuery-" + serverName;
            DockContent dcnt = null;
            DockContent CurrentQueryForm = null;
            if (NewDocument)
            {
                dcnt = new QueryDocument(this);
                AddNewContent(dcnt, "新建查询");
            }
            else if ((CurrentQueryForm = GetCurrentQueryForm()) != null)
            {
                dcnt = CurrentQueryForm;
            }
            else
            {
                IDockContent Content = GetContent("QueryDocument");
                if (Content == null)
                {
                    dcnt = new QueryDocument(this);
                    AddNewContent(dcnt, "新建查询");
                }
                else
                {
                    dcnt = GetSelectedContent();
                }
            }            
            if (dcnt.GetType().Equals(typeof(QueryDocument)))
            {
                ((QueryDocument)dcnt).Text = captionText;
                ((QueryDocument)dcnt).SetSqlCommandText(Text);
            }
            else
            {
                dcnt = (DockContent)GetContent("QueryDocument",true);  
                SetSeletedContent(dcnt);
                ((QueryDocument)dcnt).Text = captionText;
                ((QueryDocument)dcnt).SetSqlCommandText(Text);
            }
            if (!NewDocument)
            {
                ((QueryDocument)dcnt).SetServer(this.ConnectedDataBaseServer);
            }
        }

        public void SetQueryDocumentText(string Text, bool NewDocument, string ProviderName,string ConnectionString)
        {
            string captionText = "SQLQuery-" + serverName;
            DockContent dcnt = null;
            DockContent CurrentQueryForm = null;
            if (NewDocument)
            {
                dcnt = new QueryDocument(this);
                AddNewContent(dcnt, "新建查询");
            }
            else if ((CurrentQueryForm = GetCurrentQueryForm()) != null)
            {
                dcnt = CurrentQueryForm;
            }
            else
            {
                IDockContent Content = GetContent("QueryDocument");
                if (Content == null)
                {
                    dcnt = new QueryDocument(this);
                    AddNewContent(dcnt, "新建查询");
                }
                else
                {
                    dcnt = GetSelectedContent();
                }
            }
            if (!dcnt.GetType().Equals(typeof(QueryDocument)))
            {
                dcnt = (DockContent)GetContent("QueryDocument");
                SetSeletedContent(dcnt);                             
            }
            if (!((QueryDocument)dcnt).ProviderName.Equals(ProviderName) ||
                !((QueryDocument)dcnt).ConnectionString.Equals(ConnectionString))
            {
                dcnt = new QueryDocument(this);
                AddNewContent(dcnt, "新建查询");
            }
            ((QueryDocument)dcnt).Text = captionText;
            ((QueryDocument)dcnt).SetSqlCommandText(Text);           
            if (!NewDocument)
            {
                ((QueryDocument)dcnt).SetServer(this.ConnectedDataBaseServer);
            }

        }

        #endregion

        #region 菜单上插件

        public ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem GetAddinToolStripMenuItem(ContextMenuStrip contextMenu, string identityID)
        {
            ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem item = null;
            foreach (ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menu in contextMenu.Items)
            {
                var addinBase = menu.AddIn;
                if (addinBase == null) continue;
                Type typeBase = addinBase.GetType().BaseType.BaseType;
                if (typeBase.Equals(typeof(AddInBase)))
                {
                    if (identityID.Equals(((AddInBase)addinBase).IdentityID))
                    {
                        item = menu;
                    }
                }
            }
            return item;
        }

        #endregion

        #region 保存当前设置

        public void DockPanelSaveAsXml()
        {
            dockPanel.SaveAsXml(configFile);
        }

        #endregion

        #region 待办事项加载

        internal int todoTotalRecords = 0;
        internal int todoLimitRecords = 0;
        internal List<MonthItem> MonthList = new List<MonthItem>();

        internal bool SysTodoInfoAlterState = true;
        internal System.Windows.Forms.Timer todoPrompTimer = null;

        TodoManager.ITodoService service = new TodoManager.TodoServiceImpl();

        public void LoadTodoList(IList<MonthItem> list,int limit, int total)
        {
            this.MonthList = (List<MonthItem>)list;
            this.todoLimitRecords = limit;
            this.todoTotalRecords = total;
        }

        public void RefreshScheduleTasks()
        {                       
            homeContent.DoScheduleTasks(service.GetScheduleTasks(""));
        }

        public void ShowTodoPromp(string tipText)
        {
            this.SysTodoInfo.Text = tipText;
            this.SysTodoInfo.Visible = true;
            //todoPrompTimer = new System.Windows.Forms.Timer((System.ComponentModel.IContainer)this.components);
            if (todoPrompTimer == null)
            {
                todoPrompTimer = new System.Windows.Forms.Timer();
                todoPrompTimer.Interval = 1000 * 2;
                todoPrompTimer.Tick += (t, e) =>
                {
                    if (!this.SysTodoInfo.Visible)
                        return;

                    if (SysTodoInfoAlterState)
                        SysTodoInfo.Image = DevelopAssistant.Core.Properties.Resources.deep_lamp_24px;
                    else
                        SysTodoInfo.Image = DevelopAssistant.Core.Properties.Resources.lamp_24px;
                    SysTodoInfoAlterState = !SysTodoInfoAlterState;
                };
            }            
            todoPrompTimer.Enabled = true;

        }

        public void CloseTodoPromp()
        {
            this.SysTodoInfo.Visible = false;
            if (todoPrompTimer != null)
            {
                todoPrompTimer.Enabled = false;
                todoPrompTimer.Dispose();
                todoPrompTimer = null;
            }
            //this.RefreshScheduleTasks();             
        }

        #endregion

        #region 主题

        public void SetFormEditorTheme()
        {
            this.dockPanel.SetThemeStyle(AppSettings.EditorSettings.TSQLEditorTheme,
                //AppSettings.WindowTheme.Name
                DockPanelThemeStyle.ControlStyle.Name);
            if (this.MenuBar != null)
                this.MenuBar.SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
            if (this.SnippetBar != null)
                this.SnippetBar.SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
            if (this.homeContent != null)
                this.homeContent.SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
            if (this.TodoBar != null)
                this.TodoBar.SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
            ApplyFormTheme(AppSettings.EditorSettings.TSQLEditorTheme);
        }

        private void ApplyFormTheme(string theme)
        {
            Color menuColor = SystemColors.Control;
            Color textColor = SystemColors.MenuText;
            Color backColor = SystemColors.Control;           

            switch (theme)
            {
                case "Default":
                    menuColor = Color.FromArgb(246, 248, 250);
                    textColor = SystemColors.MenuText;
                    backColor = SystemColors.Control;
                    break;
                case "Black":
                    menuColor = Color.FromArgb(045, 045, 048);
                    textColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(050, 050, 050); 
                    break;
            }

            this.dockPanel.BackColor = backColor;

            this.MainToolBar.ForeColor = textColor;
            this.MainToolBar.BackColor = menuColor;

            this.MainMenu.ForeColor = textColor;
            this.MainMenu.BackColor = backColor;  
            
            //this.contextMenuStrip1.ForeColor = textColor;
            //this.contextMenuStrip1.BackColor = backColor;

            //this.contextMenuStrip2.ForeColor = textColor;
            //this.contextMenuStrip2.BackColor = backColor;

            //this.contextMenuStrip3.ForeColor = textColor;
            //this.contextMenuStrip3.BackColor = backColor;

            this.MainToolBar.SetTheme(theme);
            this.MainMenu.SetTheme(theme);

        }

        #endregion       

    }
}

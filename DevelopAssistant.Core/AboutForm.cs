﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core
{
    public partial class AboutForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        private MainForm mainForm = null;

        public AboutForm()
        {
            InitializeComponent();
        }

        public AboutForm(MainForm OwnerForm)
            : this()
        {
            this.mainForm = OwnerForm;
            this.lblproductName.Text = OwnerForm.ProductName;
            this.lblproductVersion.Text = OwnerForm.ProductVersion;          
            this.XTheme = (ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase)AppSettings.WindowTheme.Clone();
            if (this.XTheme.Name == "Mac")
                this.XTheme.RoundedStyle = ICSharpCode.WinFormsUI.Controls.RoundStyle.All;
        }

        private string Trim(string input)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(input))
            {
                result = input.Trim().TrimEnd('\\');
            }
            return result;
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            OnThemeChange(new EventArgs());
            RichDocument.LanguageOption = RichTextBoxLanguageOptions.ImeCancelComplete;
            RichDocument.Text = "     开发助手是一款面向开发人员的辅助助手，它集数据库管理（目前支持sqlserver,sqlite,mysql,postgresql），代码生成（支持从数据库生成实体映射类，数据库操作DAL中间层），数据库文档生成，代码收藏夹（支持C#,SQL,Javascrip,Html,XML,Python语法高亮)，富文本编辑，插件管理等功能模块。自2015年到如今，它已经历过４个重大版本的升级，径过不断修复和完善，目前已成为一款堪称得心应手的开发辅助工具。";
        }

        private void lblproductVersion_Click(object sender, EventArgs e)
        {
            string application_start_path = Trim(AppDomain.CurrentDomain.BaseDirectory);

            Version_on_Manager.Instance.UpdateAutoupgradeApplication(application_start_path);

            if (System.IO.File.Exists(string.Format("{0}\\Autoupgrade.EXE", application_start_path)))
            {
                System.Diagnostics.Process.Start(string.Format("{0}\\Autoupgrade.EXE", application_start_path));
                try
                {
                    this.mainForm.DockPanelSaveAsXml();
                    System.Threading.Thread.Sleep(100);
                    Application.Exit();
                }
                catch (Exception ex)
                {
                    DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
                    Environment.Exit(0);
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", linkLabel1.Text);
        }

        protected void OnThemeChange(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    break;
            }
            this.panel1.ForeColor = foreColor;
            this.panel1.BackColor = backColor;
            this.label2.ForeColor = foreColor;
            this.label2.BackColor = backColor;
            this.RichDocument.ForeColor = foreColor;
            this.RichDocument.BackColor = backColor;
            //this.linkLabel1.ForeColor = linkColor;
            //this.linkLabel1.BackColor = BackColor;
            this.linkLabel1.LinkColor = linkColor;
            this.ForeColor = foreColor;
            this.BackColor = backColor;            
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevelopAssistant.Core.DBMS;
using DevelopAssistant.Core.ToolBox;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Docking;
using DevelopAssistant.Service.SnippetCode;
using ICSharpCode.WinFormsUI.Controls.NodeControls;

namespace DevelopAssistant.Core.ToolBar
{
    [System.Runtime.InteropServices.ComVisible(true)]
    public partial class SnippetBar : DockContent
    {
        MainForm _MainForm;

        public SnippetBar()
        {
            InitializeComponent();
            InitializeSnippetTree();           
        }

        private void InitializeControls()
        {
            TagLabel.Font = NToolStripItemTheme.TextFont;
        }

        private void InitializeSnippetTree()
        {
            NodeStateIcon nodeIconBox1 = new NodeStateIcon();
            nodeIconBox1.DataPropertyName = "Icon";
            SnippetTree.NodeControls.Add(nodeIconBox1);

            NodeTextBox nodeTextBox1 = new NodeTextBox();
            nodeTextBox1.DataPropertyName = "Text";
            SnippetTree.NodeControls.Add(nodeTextBox1);            
        }

        private void SnippetBar_Load(object sender, EventArgs e)
        {
            LoadSnippetTreeData(null, string.Empty);
        }

        private void LoadSnippetTreeData(NTreeNode owner, string pid)
        {
            using (var db = NORM.DataBase.DataBaseFactory.Default)
            {
                string sqlText = "SELECT [ID]" +
                    ",[NAME]" +
                    ",[Pid]" +
                    //",[Snippet]" +
                    ",[IsShow]" +
                    ",[OrderIndex]" +
                    ",[Childs]" +
                    ",[Note]" +
                    ",[Define]" +
                    " FROM [T_SnippetTree]" +
                    " WHERE [IsShow]='1'";

                if (!string.IsNullOrEmpty(pid))
                {
                    sqlText += " AND [Pid]='" + pid + "'";
                }
                else
                {
                    sqlText += " AND [Pid]='0'";
                }

                sqlText += " ORDER BY [OrderIndex] ASC";


                DataTable snippetDataTable = db.QueryDataSet(CommandType.Text, sqlText,null).Tables[0];

                foreach (DataRow dataRow in snippetDataTable.Rows)
                {
                    string cid = dataRow["ID"] + "";
                    string childs = dataRow["Childs"] + "";
                    string cpid = dataRow["Pid"] + "";

                    var node = new NSnippetTreeNode();
                    node.Id = Int32.Parse(dataRow["ID"] + "");
                    node.Text = dataRow["NAME"] + "";
                    //node.Snippet = dataRow["Snippet"] + "";
                    node.Pid = Int32.Parse(dataRow["Pid"] + "");
                    node.Note = dataRow["Note"] + "";
                    node.Define = dataRow["Define"] + "";
                    node.Childs = 0;

                    if (!string.IsNullOrEmpty(childs))
                    {
                        node.Childs = Int32.Parse(childs);
                    }

                    if (node.Childs > 0)
                    {
                        node.ImageIndex = 0;
                        node.SelectedImageIndex = 0;
                    }
                    else
                    {
                        node.ImageIndex = 2;
                        node.SelectedImageIndex = 2;
                    }

                    if (owner == null && string.IsNullOrEmpty(pid))
                        SnippetTree.Nodes.Add(node);

                    if (owner != null)
                    {                        
                        owner.Nodes.Add(node);
                        owner.ImageIndex = 1;
                        owner.SelectedImageIndex = 1;
                        if (owner.Level < 2)
                            owner.Expand();
                    }

                    LoadSnippetTreeData(node, cid);
                }

            }
        }

        private void SnippetTree_Collapsed(object sender, NTreeViewEventArgs e)
        {
            if (e.Node.Nodes != null && e.Node.Nodes.Count > 0)
            {
                e.Node.ImageIndex = 0;
                e.Node.SelectedImageIndex = e.Node.ImageIndex;
            }
        }

        private void SnippetTree_Expanded(object sender, NTreeViewEventArgs e)
        {
            if (e.Node.Nodes != null && e.Node.Nodes.Count > 0)
            {
                e.Node.ImageIndex = 1;
                e.Node.SelectedImageIndex = e.Node.ImageIndex;
            }
        }

        public void AddSqlSnippetCode(string ConnectionString, string ProviderName,string SnippetCode)
        {
            NTreeNode CurrentRootNode = null;
            switch (ProviderName)
            {
                case "System.Data.Sql":
                case "System.Data.SQL":
                    CurrentRootNode = (NTreeNode)this.SnippetTree.Nodes[2].Nodes[0].Nodes[0];
                    break;
                case "System.Data.Sqlite":
                case "System.Data.SQLite":
                    CurrentRootNode = (NTreeNode)this.SnippetTree.Nodes[2].Nodes[0].Nodes[1];
                    break;               
                case "System.Data.MySql":
                    CurrentRootNode = (NTreeNode)this.SnippetTree.Nodes[2].Nodes[0].Nodes[2];
                    break;
                case "System.Data.PostgreSql":
                    CurrentRootNode = (NTreeNode)this.SnippetTree.Nodes[2].Nodes[0].Nodes[3];
                    break;
            }
            if (CurrentRootNode == null)
                return;

            MessageDialog messageBox = new MessageDialog("输入名称：");
            if (messageBox.ShowDialog(this).Equals(DialogResult.OK))
            {
                //添加到数据库
                using (var db = NORM.DataBase.DataBaseFactory.Default)
                {
                    string Name = messageBox.ResultText;
                    int Pid = CurrentRootNode.Id;
                    string Snippet = SnippetConvert.EncodeSnippetContent(SnippetCode);
                    int IsShow = 1;
                    int OrderIndex = 0;
                    string Note = "SQL片段";
                    string Define = ConnectionString;
                    int Childs = 0;

                    StringBuilder strSql = new StringBuilder();
                    strSql.Append(" insert into T_SnippetTree ");
                    strSql.Append(" ( [NAME],[Pid],[Snippet],[IsShow],[OrderIndex],[Note],[Formats],[Define],[Childs] ) ");
                    strSql.Append(" values(  '" + Name + "','" + Pid + "','" + Snippet + "'," + IsShow + ",'" + OrderIndex + "','" + Note + "','.sql','" + Define + "','" + Childs + "' ) ; ");
                    strSql.Append(System.Environment.NewLine);
                    strSql.Append(" update [T_SnippetTree] set Childs=Childs+1 where [ID]='" + Pid + "' ");
                    db.Execute(CommandType.Text, strSql.ToString(), null);
                }

                var node = new NSnippetTreeNode();
                node.Pid = CurrentRootNode.Pid;
                node.Text = messageBox.ResultText;
                node.Snippet = SnippetCode;
                node.Note = "SQL片段";
                node.Define = ConnectionString;
                node.ImageIndex = 2;
                node.SelectedImageIndex = 2;
                CurrentRootNode.Nodes.Add(node);
                CurrentRootNode.Childs = CurrentRootNode.Childs + 1;

                this.SnippetTree.Refresh();

            }
            

        }

        public NTreeNode GetFilterById(int nodeId, NTreeNodeCollection nodes)
        {
            SnippetTree.Focus();
            if (nodes == null)
                nodes = SnippetTree.Nodes;
            foreach (NTreeNode node in nodes)
            {
                if (node.Id == nodeId)
                {
                    if (node.Parent != null)
                        node.Parent.Expand();
                    SnippetTree.SelectedNode = node;
                    break;
                }
                GetFilterById(nodeId, node.Nodes);
            }
            return (NTreeNode)SnippetTree.SelectedNode;
        }

        public void RefreshTree()
        {
            NTreeNode node = (NTreeNode)this.SnippetTree.SelectedNode;
            if (node == null)
            {
                SnippetTree.Nodes.Clear();
                LoadSnippetTreeData(null, string.Empty);
            }
            else
            {
                node.Nodes.Clear();
                LoadSnippetTreeData(node, node.Id.ToString());
            }
        }

        public void SetTheme(string themeName)
        {            
            switch (themeName)
            {
                case "Default":
                    TagLabel.ForeColor = SystemColors.ControlText;
                    toolStrip1.ForeColor = SystemColors.ControlText;
                    toolStrip1.BackColor = SystemColors.Control;
                    BackColor= SystemColors.Control;
                    panel1.BackColor = SystemColors.Control;
                    panel2.BackColor = SystemColors.Control;
                    txtKeyword.XForeColor = Color.Black;
                    txtKeyword.XBackColor = Color.White;
                    break;
                case "Black":
                    TagLabel.ForeColor = Color.FromArgb(240, 240, 240);
                    toolStrip1.ForeColor = Color.FromArgb(240, 240, 240);
                    toolStrip1.BackColor = Color.FromArgb(045, 045, 048);
                    BackColor = Color.FromArgb(045, 045, 048);
                    panel1.BackColor = Color.FromArgb(045, 045, 048);
                    panel2.BackColor = Color.FromArgb(045, 045, 048);
                    txtKeyword.XForeColor= Color.FromArgb(240, 240, 240);
                    txtKeyword.XBackColor = Color.FromArgb(045, 045, 048);
                    break;
            }
            this.SnippetTree.SetTheme(themeName);
        }

        private void SnippetTree_NodeMouseDoubleClick(object sender, NTreeNodeMouseEventArgs e)
        {
            var node = e.Node as NSnippetTreeNode;
            if (string.IsNullOrEmpty(node.Text))
                return;

            if (_MainForm == null)
                _MainForm = (MainForm)Application.OpenForms["MainForm"];

            if (_MainForm != null && node.Childs == 0)
            {
                var snippet = GetSnippet(node.Id);
                SnippetPad _pad = new SnippetPad(_MainForm);
                _MainForm.AddNewContent(_pad, node.Text);
                if (string.IsNullOrEmpty(snippet.Snippet))
                {
                    snippet.Define = ".sql";
                    snippet.Snippet = SnippetConvert.EncodeSnippetContent(node.Snippet);
                }
                _pad.LoadSnippet(node.Id, SnippetConvert.DecodeSnippetContent(snippet.Snippet), snippet.Define);
            }
        }

        private NSnippetTreeNode GetSnippet(int id)
        {
            var node = new NSnippetTreeNode();
            node.Id = id;
            using (var db = NORM.DataBase.DataBaseFactory.Default)
            {
                string strSql = "SELECT [ID],[NAME],[Pid],[Snippet],[IsShow],[OrderIndex],[Formats],[Note],[Define] ";
                strSql += ",[Childs] FROM [T_SnippetTree] ";
                strSql += "WHERE [ID]='" + id + "'";

                DataTable dt = db.QueryDataSet(CommandType.Text, strSql, null).Tables[0];

                foreach (DataRow dr in dt.Rows)
                {                   
                    node.Text = dr["NAME"] + "";
                    node.Snippet = dr["Snippet"] + "";
                    node.Define = dr["Formats"] + "";
                }

            }
            return node;
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NTreeNode node=(NTreeNode)this.SnippetTree.SelectedNode;
            if (node == null)
            {
                SnippetTree.Nodes.Clear();
                LoadSnippetTreeData(null, string.Empty);
            }
            else
            {
                node.Nodes.Clear();
                LoadSnippetTreeData(node, node.Id.ToString());
            }
        }

        private void SnippetTree_NodeMouseClick(object sender, NTreeNodeMouseEventArgs e)
        {
            NTreeNode node = (NTreeNode)e.Node;
            if (e.Button == MouseButtons.Right)
            {
                this.SnippetTree.SelectedNode = node;
            }
            if (node.Nodes.Count > 0)
            {
                删除ToolStripMenuItem.Visible = false;
                toolStripButtonDeleteNode.Enabled = false;
            }
            else
            {
                删除ToolStripMenuItem.Visible = true;
                toolStripButtonDeleteNode.Enabled = true;
            }
        }

        private void SnippetTree_MouseClick(object sender, MouseEventArgs e)
        {
            NTreeNode node = (NTreeNode)SnippetTree.GetNodeAt(e.Location);
            if (node != null)
            {

            }            
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;
            NTreeNode parent_node = node.Parent as NTreeNode;

            if (MessageBox.Show(this,"确认要删除 " + node.Text + " 吗？ ","信息提示",
                MessageBoxButtons.OKCancel).Equals(DialogResult.OK))
            {
                using (var db = NORM.DataBase.DataBaseFactory.Default)
                {
                    string strSql = "DELETE FROM [T_SnippetTree] ";
                    strSql += "WHERE [ID]='" + node.Id + "' ; ";
                    strSql += System.Environment.NewLine;
                    strSql += "UPDATE [T_SnippetTree] SET Childs=Childs-1 WHERE [ID]='" + node.Pid + "'";
                    db.Execute(CommandType.Text, strSql, null);
                }

                if (parent_node != null)
                {
                    parent_node.Nodes.Clear();
                    LoadSnippetTreeData(parent_node, node.Pid.ToString());
                    parent_node.Expand();
                }
                else
                {
                    SnippetTree.Nodes.Clear();
                    LoadSnippetTreeData(null, "");
                    SnippetTree.ExpandAll();
                }

                if (parent_node.Parent != null)
                {
                    SnippetTree.SelectedNode = parent_node.Parent;
                    RefreshTree();
                    parent_node.ExpandAll();
                }

            }          

        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;
            SnippetEdit snippetEdit = new SnippetEdit(node.Id,"Edit");
            if (snippetEdit.ShowDialog(this).Equals(DialogResult.OK))
            {
                NTreeNode root_node = (NTreeNode)this.SnippetTree.Nodes[0];
                NTreeNode parent_node = (NTreeNode)node.Parent;
                parent_node.Nodes.Clear();
                LoadSnippetTreeData(parent_node, parent_node.Id.ToString());
                parent_node.Expand();
            }
        }

        private void 添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int nodeId = 0;
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;
            if (node != null)
                nodeId = node.Id;

            SnippetEdit snippetEdit = new SnippetEdit(nodeId, "Add");
            if (snippetEdit.ShowDialog(this).Equals(DialogResult.OK))
            {
                if (node != null)
                {
                    node.Nodes.Clear();
                    LoadSnippetTreeData(node, node.Id.ToString());
                    node.Expand();
                }
                else
                {
                    SnippetTree.Nodes.Clear();
                    LoadSnippetTreeData(null, "");
                    SnippetTree.ExpandAll();
                }
            }
        }

        private void SearchKeywords_Click(object sender, EventArgs e)
        {
            SearchKeyword(txtKeyword.Text, SnippetTree.Nodes);
        }

        private void SearchKeyword(string keyword, NTreeNodeCollection nodes)
        {
            SnippetTree.Focus();
            foreach (NTreeNode node in nodes)
            {
                if (node.Text.IndexOf(keyword, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    if (node.Parent != null)
                        node.Parent.Expand();
                    SnippetTree.SelectedNode = node;
                    break;
                }
                SearchKeyword(keyword, node.Nodes);
            }
        }

        private void toolStripButtonAddRoot_Click(object sender, EventArgs e)
        {
            int nodeId = 0;
            SnippetEdit snippetEdit = new SnippetEdit(nodeId, "Add");
            if (snippetEdit.ShowDialog(this).Equals(DialogResult.OK))
            {
                SnippetTree.Nodes.Clear();
                LoadSnippetTreeData(null, "");
                SnippetTree.ExpandAll();
            }
        }

        private void toolStripButtonAddNode_Click(object sender, EventArgs e)
        {
            int nodeId = 0;
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;
            if (node != null)
                nodeId = node.Id;

            SnippetEdit snippetEdit = new SnippetEdit(nodeId, "Add");
            if (snippetEdit.ShowDialog(this).Equals(DialogResult.OK))
            {
                if (node != null)
                {
                    node.Nodes.Clear();
                    LoadSnippetTreeData(node, node.Id.ToString());
                    node.Expand();
                }
                else
                {
                    SnippetTree.Nodes.Clear();
                    LoadSnippetTreeData(null, "");
                    SnippetTree.ExpandAll();
                }
            }
        }

        private void toolStripButtonEditNode_Click(object sender, EventArgs e)
        {
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;
            if (node == null) return;

            SnippetEdit snippetEdit = new SnippetEdit(node.Id, "Edit");
            if (snippetEdit.ShowDialog(this).Equals(DialogResult.OK))
            {
                NTreeNode root_node = (NTreeNode)this.SnippetTree.Nodes[0];
                NTreeNode parent_node = (NTreeNode)node.Parent;
                parent_node.Nodes.Clear();
                LoadSnippetTreeData(parent_node, parent_node.Id.ToString());
                parent_node.Expand();
            }

        }

        private void toolStripButtonDeleteNode_Click(object sender, EventArgs e)
        {
            NTreeNode node = this.SnippetTree.SelectedNode as NTreeNode;

            if (node == null) return;

            NTreeNode parent_node = node.Parent as NTreeNode;

            if (MessageBox.Show(this, "确认要删除 " + node.Text + " 吗？ ", "信息提示",
                MessageBoxButtons.OKCancel).Equals(DialogResult.OK))
            {
                using (var db = NORM.DataBase.DataBaseFactory.Default)
                {
                    string strSql = "DELETE FROM [T_SnippetTree] ";
                    strSql += "WHERE [ID]='" + node.Id + "' ; ";
                    strSql += System.Environment.NewLine;
                    strSql += "UPDATE [T_SnippetTree] SET Childs=Childs-1 WHERE [ID]='" + node.Pid + "'";
                    db.Execute(CommandType.Text, strSql, null);
                }

                if (parent_node != null)
                {
                    parent_node.Nodes.Clear();
                    LoadSnippetTreeData(parent_node, node.Pid.ToString());
                    parent_node.Expand();
                }
                else
                {
                    SnippetTree.Nodes.Clear();
                    LoadSnippetTreeData(null, "");
                    SnippetTree.ExpandAll();
                }
            }
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            NTreeNode node = (NTreeNode)this.SnippetTree.SelectedNode;
            if (node == null)
            {
                SnippetTree.Nodes.Clear();
                LoadSnippetTreeData(null, string.Empty);
            }
            else
            {
                node.Nodes.Clear();
                LoadSnippetTreeData(node, node.Id.ToString());
            }
        }

        private void toolStripButtonDetail_Click(object sender, EventArgs e)
        {
            var node = SnippetTree.SelectedNode as NSnippetTreeNode;
            if (node == null || string.IsNullOrEmpty(node.Text))
                return;

            if (_MainForm == null)
                _MainForm = (MainForm)Application.OpenForms["MainForm"];

            if (_MainForm != null && node.Childs == 0)
            {
                var snippet = GetSnippet(node.Id);
                SnippetPad _pad = new SnippetPad(_MainForm);
                _MainForm.AddNewContent(_pad, node.Text);
                if (string.IsNullOrEmpty(snippet.Snippet))
                {
                    snippet.Define = ".sql";
                    snippet.Snippet = SnippetConvert.EncodeSnippetContent(node.Snippet);
                }
                _pad.LoadSnippet(node.Id,SnippetConvert.DecodeSnippetContent(snippet.Snippet), snippet.Define);
            }
        }

    }

    public class NSnippetTreeNode : NTreeNode
    {
        public string Snippet { get; set; }
        public string Define { get; set; }
    }
}

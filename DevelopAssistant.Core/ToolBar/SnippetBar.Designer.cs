﻿namespace DevelopAssistant.Core.ToolBar
{
    partial class SnippetBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SnippetBar));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.SnippetTree = new ICSharpCode.WinFormsUI.Controls.NTreeView();
            this.contextMenuStrip1 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.添加ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.编辑ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.删除ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.刷新ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.panel3 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripButtonAddRoot = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAddNode = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonEditNode = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonDetail = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonDeleteNode = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonRefresh = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAbout = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.SearchKeywords = new ICSharpCode.WinFormsUI.Controls.NImageButton();
            this.txtKeyword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.TagLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "zTree02.png");
            this.imageList1.Images.SetKeyName(1, "zTree04.png");
            this.imageList1.Images.SetKeyName(2, "zTree05.png");
            this.imageList1.Images.SetKeyName(3, "zTree06.png");
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.SnippetTree);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 555);
            this.panel1.TabIndex = 1;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // SnippetTree
            // 
            this.SnippetTree.BackColor = System.Drawing.SystemColors.Window;
            this.SnippetTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SnippetTree.ContextMenuStrip = this.contextMenuStrip1;
            this.SnippetTree.Cursor = System.Windows.Forms.Cursors.Default;
            this.SnippetTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SnippetTree.DragDropMarkColor = System.Drawing.Color.Black;
            this.SnippetTree.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.SnippetTree.ImageList = this.imageList1;
            this.SnippetTree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.SnippetTree.Location = new System.Drawing.Point(30, 30);
            this.SnippetTree.Model = null;
            this.SnippetTree.Name = "SnippetTree";
            this.SnippetTree.NBorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.SnippetTree.SelectedNode = null;
            this.SnippetTree.Size = new System.Drawing.Size(254, 525);
            this.SnippetTree.TabIndex = 1;
            this.SnippetTree.NodeMouseClick += new ICSharpCode.WinFormsUI.Controls.NTreeNodeMouseClickEventHandler(this.SnippetTree_NodeMouseClick);
            this.SnippetTree.NodeMouseDoubleClick += new ICSharpCode.WinFormsUI.Controls.NTreeNodeMouseClickEventHandler(this.SnippetTree_NodeMouseDoubleClick);
            this.SnippetTree.Collapsed += new ICSharpCode.WinFormsUI.Controls.NTreeViewEventHandler(this.SnippetTree_Collapsed);
            this.SnippetTree.Expanded += new ICSharpCode.WinFormsUI.Controls.NTreeViewEventHandler(this.SnippetTree_Expanded);
            this.SnippetTree.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SnippetTree_MouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加ToolStripMenuItem,
            this.编辑ToolStripMenuItem,
            this.删除ToolStripMenuItem,
            this.刷新ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(117, 116);
            // 
            // 添加ToolStripMenuItem
            // 
            this.添加ToolStripMenuItem.Name = "添加ToolStripMenuItem";
            this.添加ToolStripMenuItem.Size = new System.Drawing.Size(116, 28);
            this.添加ToolStripMenuItem.Text = "添加";
            this.添加ToolStripMenuItem.Click += new System.EventHandler(this.添加ToolStripMenuItem_Click);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(116, 28);
            this.编辑ToolStripMenuItem.Text = "编辑";
            this.编辑ToolStripMenuItem.Click += new System.EventHandler(this.编辑ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(116, 28);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(116, 28);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // panel3
            // 
            this.panel3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Right;
            this.panel3.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel3.Controls.Add(this.toolStrip1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 30);
            this.panel3.MarginWidth = 0;
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(1);
            this.panel3.Size = new System.Drawing.Size(30, 525);
            this.panel3.TabIndex = 2;
            this.panel3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip1.Font = new System.Drawing.Font("微软雅黑", 10F);            
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAddRoot,
            this.toolStripButtonAddNode,
            this.toolStripButtonEditNode,
            this.toolStripButtonDetail,
            this.toolStripButtonDeleteNode,
            this.toolStripButtonRefresh,
            this.toolStripButtonAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(1, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(28, 523);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAddRoot
            // 
            this.toolStripButtonAddRoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddRoot.Image = global::DevelopAssistant.Core.Properties.Resources.add_circle_24px;
            this.toolStripButtonAddRoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddRoot.Name = "toolStripButtonAddRoot";
            this.toolStripButtonAddRoot.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAddRoot.Text = "添加根节点";
            this.toolStripButtonAddRoot.ToolTipText = "添加根节点";
            this.toolStripButtonAddRoot.Click += new System.EventHandler(this.toolStripButtonAddRoot_Click);
            // 
            // toolStripButtonAddNode
            // 
            this.toolStripButtonAddNode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddNode.Image = global::DevelopAssistant.Core.Properties.Resources.add;
            this.toolStripButtonAddNode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddNode.Name = "toolStripButtonAddNode";
            this.toolStripButtonAddNode.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAddNode.Text = "添加节点";
            this.toolStripButtonAddNode.ToolTipText = "添加节点";
            this.toolStripButtonAddNode.Click += new System.EventHandler(this.toolStripButtonAddNode_Click);
            // 
            // toolStripButtonEditNode
            // 
            this.toolStripButtonEditNode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEditNode.Image = global::DevelopAssistant.Core.Properties.Resources.modify_24px;
            this.toolStripButtonEditNode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditNode.Name = "toolStripButtonEditNode";
            this.toolStripButtonEditNode.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonEditNode.Text = "编辑节点";
            this.toolStripButtonEditNode.ToolTipText = "编辑节点";
            this.toolStripButtonEditNode.Click += new System.EventHandler(this.toolStripButtonEditNode_Click);
            // 
            // toolStripButtonDetail
            // 
            this.toolStripButtonDetail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDetail.Image = global::DevelopAssistant.Core.Properties.Resources.publish;
            this.toolStripButtonDetail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDetail.Name = "toolStripButtonDetail";
            this.toolStripButtonDetail.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonDetail.Text = "详细";
            this.toolStripButtonDetail.Click += new System.EventHandler(this.toolStripButtonDetail_Click);
            // 
            // toolStripButtonDeleteNode
            // 
            this.toolStripButtonDeleteNode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDeleteNode.Enabled = false;
            this.toolStripButtonDeleteNode.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolStripButtonDeleteNode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteNode.Name = "toolStripButtonDeleteNode";
            this.toolStripButtonDeleteNode.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonDeleteNode.Text = "删除节点";
            this.toolStripButtonDeleteNode.ToolTipText = "删除节点";
            this.toolStripButtonDeleteNode.Click += new System.EventHandler(this.toolStripButtonDeleteNode_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRefresh.Image = global::DevelopAssistant.Core.Properties.Resources.recovery;
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRefresh.Text = "刷新";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAbout.Image = global::DevelopAssistant.Core.Properties.Resources.checked_32px;
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAbout.Text = "关于";
            this.toolStripButtonAbout.ToolTipText = "关于";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.SearchKeywords);
            this.panel2.Controls.Add(this.txtKeyword);
            this.panel2.Controls.Add(this.TagLabel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(284, 30);
            this.panel2.TabIndex = 0;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // SearchKeywords
            // 
            this.SearchKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchKeywords.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SearchKeywords.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SearchKeywords.Image = global::DevelopAssistant.Core.Properties.Resources.query;
            this.SearchKeywords.Location = new System.Drawing.Point(259, 4);
            this.SearchKeywords.Name = "SearchKeywords";
            this.SearchKeywords.Size = new System.Drawing.Size(22, 22);
            this.SearchKeywords.TabIndex = 2;
            this.SearchKeywords.ToolTipText = "快速查找";
            this.SearchKeywords.Click += new System.EventHandler(this.SearchKeywords_Click);
            // 
            // txtKeyword
            // 
            this.txtKeyword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeyword.BackColor = System.Drawing.Color.White;
            this.txtKeyword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtKeyword.ForeColor = System.Drawing.Color.Gray;
            this.txtKeyword.FousedColor = System.Drawing.Color.Orange;
            this.txtKeyword.Icon = null;
            this.txtKeyword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtKeyword.IsButtonTextBox = false;
            this.txtKeyword.IsClearTextBox = false;
            this.txtKeyword.IsPasswordTextBox = false;
            this.txtKeyword.Location = new System.Drawing.Point(57, 2);
            this.txtKeyword.MaxLength = 32767;
            this.txtKeyword.Multiline = false;
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.PasswordChar = '\0';
            this.txtKeyword.Placeholder = null;
            this.txtKeyword.ReadOnly = false;
            this.txtKeyword.Size = new System.Drawing.Size(198, 24);
            this.txtKeyword.TabIndex = 2;
            this.txtKeyword.UseSystemPasswordChar = false;
            this.txtKeyword.XBackColor = System.Drawing.Color.White;
            this.txtKeyword.XDisableColor = System.Drawing.Color.Empty;
            this.txtKeyword.XForeColor = System.Drawing.Color.Gray;
            // 
            // TagLabel
            // 
            this.TagLabel.AutoSize = true;
            this.TagLabel.Location = new System.Drawing.Point(4, 8);
            this.TagLabel.Name = "TagLabel";
            this.TagLabel.Size = new System.Drawing.Size(80, 18);
            this.TagLabel.TabIndex = 1;
            this.TagLabel.Text = "关键词：";
            // 
            // SnippetBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 555);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SnippetBar";
            this.Text = "代码管理";
            this.Load += new System.EventHandler(this.SnippetBar_Load);
            this.panel1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NTreeView SnippetTree;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
        private System.Windows.Forms.Label TagLabel;
        private ICSharpCode.WinFormsUI.Controls.NImageButton SearchKeywords;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtKeyword;
        private System.Windows.Forms.ImageList imageList1;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 添加ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 编辑ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 删除ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 刷新ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel3;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAddRoot;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAddNode;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonDeleteNode;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAbout;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonEditNode;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonRefresh;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonDetail;
    }
}
﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.Tabs
{
    public partial class LoggerTab : ICSharpCode.WinFormsUI.Docking.DockContent
    {
        public LoggerTab()
        {
            InitializeComponent();
        }

        public LoggerTab(MainForm mainForm)
            : this()
        {             
            
        }

        private void LoggerForm_Load(object sender, EventArgs e)
        {
            panel2.BorderColor = SystemColors.ControlDark;
            panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;            

            OnThemeChanged(new EventArgs());

            DateTime now = DateTime.Now;

            this.LoggerContent.Text = "读取中...";

            this.combDateTimes.Enabled = false;

            string path = DevelopAssistant.Common.NLogger.BaseDirectory + "\\Logger";

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            System.Threading.Tasks.Task.Run(() => {
                DirectoryInfo d = new System.IO.DirectoryInfo(path);
                DirectoryInfo[] dlist = d.GetDirectories();
                foreach (DirectoryInfo di in dlist)
                {
                    DirectoryInfo[] ddlist = di.GetDirectories();
                    foreach (DirectoryInfo ddi in ddlist)
                    {
                        FileInfo[] fis = ddi.GetFiles();
                        foreach (FileInfo fi in fis)
                        {
                            this.combDateTimes.Invoke(new MethodInvoker(() => { this.combDateTimes.Items.Add(fi.Name); }));
                        }
                    }
                }
                System.Threading.Thread.Sleep(200);
                this.combDateTimes.Invoke(new MethodInvoker(() =>
                {
                    if (this.combDateTimes.Items.Count > 0)
                        this.combDateTimes.SelectedIndex = this.combDateTimes.Items.Count - 1;
                    this.combDateTimes.Enabled = true;
                    if (this.combDateTimes.Items.Count < 1)
                        this.LoggerContent.Text = "";
                }));
            });
           
        }

        private void LoadLogContent(string name)
        {
            DateTime dateNow = DateTime.Parse(name);
            string path = DevelopAssistant.Common.NLogger.BaseDirectory + "\\Logger\\" + dateNow.Year + "\\" + dateNow.ToString("yyyyMM") + "\\" + dateNow.ToString("yyyyMMdd") + ".txt";
            this.LoggerContent.Text = "";
            if (System.IO.File.Exists(path))
            {
                //using (System.IO.FileStream stream = System.IO.File.OpenRead(path))
                //{
                //    byte[] content = new byte[stream.Length];
                //    stream.Read(content, 0, content.Length);
                //    this.LoggerContent.Text = System.Text.Encoding.UTF8.GetString(content);                  
                //}
                this.LoggerContent.Text = "正在加载...";
                string strContent = "";
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    strContent = sr.ReadToEnd();
                }
                this.LoggerContent.Text = strContent;
            }
            this.lblNowDate.Text = "当前:" + name;
        }

        private void combDateTimes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = combDateTimes.SelectedItem.ToString()
                .Replace(".txt", "")
                .Insert(4, "-").Insert(7, "-");  
            LoadLogContent(name);
        }

        public override void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color textColor = SystemColors.Window;
            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    textColor = SystemColors.Window;
                    lblNowDate.ForeColor = Color.Black;
                    combDateTimes.ForeColor = Color.Black;
                    BackColor = SystemColors.Control;                   
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    textColor = Color.FromArgb(045, 045, 048);
                    lblNowDate.ForeColor = Color.FromArgb(240, 240, 240);                  
                    BackColor = Color.FromArgb(045, 045, 048);                  
                    break;
            }

            combDateTimes.ForeColor = foreColor;
            combDateTimes.BackColor = textColor;

            LoggerContent.SetHighlighting(AppSettings.EditorSettings.TSQLEditorTheme, "Logger");
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (combDateTimes != null)
            {
                var Location = combDateTimes.Location;
                combDateTimes.Location = new Point(this.Width - combDateTimes.Width - 8, Location.Y);
            }
        }
    }
}

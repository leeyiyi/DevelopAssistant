﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing; 
using System.Text;
using System.Windows.Forms;
using ICSharpCode.TextEditor;
using System.Diagnostics;
using System.IO;
using ICSharpCode.TextEditor.Document;
using DevelopAssistant.Service;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class FindAndReplace : ToolBoxBase //ToolBoxBase
    {
        private FindAndReplaceRequest Request = null;
        private IDocumentContent DocumentContent = null;
        private Rectangle workArea = new Rectangle();

        public FindAndReplace()
        {
            InitializeComponent();
        }

        public FindAndReplace(MainForm OwnerForm, IDocumentContent DocumentContent)
            : this()
        {
            this.Owner = OwnerForm;
            this.Request = new FindAndReplaceRequest();
            this.DocumentContent = DocumentContent;
            this.XTheme = AppSettings.WindowTheme;
            this.NToolBar.BorderColor = AppSettings.WindowTheme.FormBorderOutterColor;            
        }      

        private void btnLook_Click(object sender, EventArgs e)
        {
            Request.CommandText = "Look";
            if (!string.IsNullOrEmpty(txtLookFor.Text))
            {
                Request.LookFor = txtLookFor.Text.Trim();
                Request.MatchCase = chkMatchCase.Checked;
                DocumentContent.FindAndReplace(Request);
            }            
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            Request.CommandText = "Replace";
            Request.LookFor = txtLookFor.Text.Trim();
            Request.ReplaceWith = txtReplaceWith.Text.Trim();
            Request.MatchCase = chkMatchCase.Checked;
            DocumentContent.FindAndReplace(Request);
        }

        private void LoadXTheme()
        {
            this.NToolBar.BorderColor = this.XTheme.FormBorderOutterColor;
        }

        private void FindAndReplace_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
            ((MainForm)Owner).SetWindowAtDockPanCenter(this);
        }

        protected void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color toolBackColor = SystemColors.Control;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    textColor = Color.FromArgb(038, 038, 038);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }
            this.panel1.ForeColor = foreColor;
            this.panel1.BackColor = backColor;

            txtLookFor.XForeColor = foreColor;
            txtLookFor.XBackColor = textColor;
            txtReplaceWith.XForeColor = foreColor;
            txtReplaceWith.XBackColor = textColor;
            btnLook.ForeColor = foreColor;
            btnLook.BackColor = backColor;
            btnReplace.ForeColor = foreColor;
            btnReplace.BackColor = backColor;
            NToolBar.ForeColor = foreColor;
            NToolBar.BackColor = toolBackColor;
        }

    }

}

﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls.NTimeLine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class EditTotoForm : ToolBoxBase
    {
        MainForm mainForm = null;

        DateTimeItem dateTimeItem = null;

        TodoManager.ITodoService service = new TodoManager.TodoServiceImpl();

        public EditTotoForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        public void InitializeControls()
        {
            this.tabControl1.Radius = 1;
            this.tabControl1.TabCaptionLm = -3;
            this.tabControl1.ItemSize = new System.Drawing.Size(86, 28);
            this.tabControl1.Alignment = TabAlignment.Top;
            this.tabControl1.BaseColor = SystemColors.Control;
            this.tabControl1.BackColor = SystemColors.Control;
            this.tabControl1.SelectedColor = SystemColors.Control;
            this.tabControl1.BorderColor = SystemColors.ControlDark;
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;            

            this.tabPage1.BackColor = SystemColors.Control;
            this.tabPage2.BackColor = SystemColors.Control;

            if (AppSettings.WindowTheme != null)
            {
                Type type = AppSettings.WindowTheme.GetType();
                if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeVS2012))
                {
                    tabControl1.Radius = 1;
                    tabControl1.ItemSize = new Size(96, 28);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeMac))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new Size(104, 24);
                }
                else if (type == typeof(ICSharpCode.WinFormsUI.Theme.ThemeShadow))
                {
                    tabControl1.Radius = 8;
                    tabControl1.ItemSize = new System.Drawing.Size(102, 26);
                }

                this.panel1.BorderColor = AppSettings.WindowTheme.FormBorderOutterColor;
            }

            this.dateTimePicker1.AutoSize = false;
            this.dateTimePicker1.Size = new Size(156, 24);

            System.Collections.Generic.List<ListItem> list = new System.Collections.Generic.List<ListItem>();
            list.Add(new ListItem() { Text = "进行中", Value = "1" });
            list.Add(new ListItem() { Text = "已作废", Value = "5" });
            list.Add(new ListItem() { Text = "已完成", Value = "6" });
            combImportState.Items.AddRange(list.ToArray());
            combImportState.SelectedIndex = 0;

        }

        public void BindItemData(TimelineItem todoItem)
        {
            dateTimeItem = (DateTimeItem)todoItem;
            this.ntextBoxName.Text = dateTimeItem.Name + "";
            this.ntextBoxSummary.Text = dateTimeItem.Summary + "";
            this.ntextBoxTitle.Text = dateTimeItem.Title + "";
            this.ntextBoxUser.Text = dateTimeItem.ResponsiblePerson + "";
            this.dateTimePicker1.Value = dateTimeItem.DateTime;
            if (!string.IsNullOrEmpty(dateTimeItem.Description))
                this.NEditorText.Text = NORM.Common.EncryptDES.DecryptDES(dateTimeItem.Description);

            int selectedIndex = 0;
            foreach(ListItem item in this.combImportState.Items)
            {
                if(Convert.ToInt32(item.Value)== (int)((DateTimeItem)todoItem).Timeliness)
                {
                    this.combImportState.SelectedIndex = selectedIndex;
                    break;
                }
                selectedIndex++;
            }
        }

        public EditTotoForm(MainForm form, TimelineItem todoItem) : this()
        {
            mainForm = form;
            XTheme = AppSettings.WindowTheme;            
            BindItemData(todoItem);
        }

        private void EditTotoForm_Load(object sender, EventArgs e)
        {
            this.NEditorText.ShowLineNumber = false;
            OnThemeChanged(new EventArgs());
        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            try
            {
                TodoManager.TodoDTO todo = new TodoManager.TodoDTO();
                todo.ID = Convert.ToInt32(dateTimeItem.Id);
                todo.Name = ntextBoxName.Text.Trim();
                todo.Title= ntextBoxTitle.Text.Trim();
                todo.Summary = ntextBoxSummary.Text.Trim();
                todo.Description = NEditorText.Text.Trim();
                todo.ResponsiblePerson = ntextBoxUser.Text.Trim();
                todo.DateTime = dateTimePicker1.Value;
                todo.YearMonth = todo.DateTime.Value.ToString("yyyy-MM");
                todo.Timeliness = Convert.ToInt32(((ListItem)combImportState.SelectedItem).Value);
                if (!string.IsNullOrEmpty(NEditorText.Text) && NEditorText.Text != "undefine")
                    todo.Description = NORM.Common.EncryptDES.EncryptDESM(NEditorText.Text);

                service.EditTodoItem(todo);

                mainForm.RefreshScheduleTasks();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
                new DevelopAssistant.Core.ToolBox.MessageDialog(DevelopAssistant.Core.Properties.Resources.error_32px, "修改待办事项出错", MessageBoxButtons.OK).ShowDialog(this);
            }
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolBackColor = SystemColors.Control;
            Color textBackColor = SystemColors.Window;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    textBackColor = SystemColors.Window;
                    tabControl1.ForeColor = Color.Black;
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = SystemColors.Control;
                    tabControl1.BackColor = SystemColors.Control;
                    tabControl1.SelectedColor = SystemColors.Control;
                    tabControl1.BaseTabColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    tabControl1.ForeColor = Color.FromArgb(240, 240, 240);
                    tabControl1.BorderColor = SystemColors.ControlDark;
                    tabControl1.ArrowColor = Color.White;
                    tabControl1.BaseColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BackColor = Color.FromArgb(045, 045, 048);
                    tabControl1.SelectedColor = Color.FromArgb(045, 045, 048);
                    tabControl1.BaseTabColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            this.XBackgroundColor = backColor;
            this.panel1.ForeColor = foreColor;
            this.panel1.BackColor = toolBackColor;
            this.panel2.ForeColor = foreColor;
            this.panel2.BackColor = backColor;
            this.tabPage1.ForeColor = foreColor;
            this.tabPage1.BackColor = backColor;
            this.tabPage2.ForeColor = foreColor;
            this.tabPage2.BackColor = backColor;
            this.btnApplyOk.ForeColor = foreColor;
            this.btnApplyOk.BackColor = backColor;
            this.btnCancle.ForeColor = foreColor;
            this.btnCancle.BackColor = backColor;
            this.ntextBoxName.XForeColor = foreColor;
            this.ntextBoxName.XBackColor = textBackColor;
            this.ntextBoxSummary.XForeColor = foreColor;
            this.ntextBoxSummary.XBackColor = textBackColor;
            this.ntextBoxTitle.XForeColor = foreColor;
            this.ntextBoxTitle.XBackColor = textBackColor;
            this.ntextBoxUser.XForeColor = foreColor;
            this.ntextBoxUser.XBackColor = textBackColor;
            this.combImportState.ForeColor = foreColor;
            this.combImportState.BackColor = textBackColor;
            this.dateTimePicker1.ForeColor = foreColor;
            this.dateTimePicker1.BackColor = textBackColor;
            //this.dateTimePicker1.CalendarForeColor = foreColor;
            //this.dateTimePicker1.CalendarMonthBackground = backColor;

            this.NEditorText.SetTheme(themeName);

        }
    }
}

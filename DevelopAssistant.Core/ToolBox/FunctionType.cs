﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class FunctionType : ToolBoxBase
    {
        private string _functionTypeName;
        public string FuntionTypeName
        {
            get { return _functionTypeName; }
        }

        public FunctionType()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.XTheme = AppSettings.WindowTheme;
            ListItem item = new ListItem();
            item.Text = "内联表值函数";
            item.Value = "innertablefunction";
            this.comboBox1.Items.Add(item);
            item = new ListItem();
            item.Text = "多语句表值函数";
            item.Value = "multiltablefunction";
            this.comboBox1.Items.Add(item);
            item = new ListItem();
            item.Text = "标量值函数";
            item.Value = "standvaluefunction";
            this.comboBox1.Items.Add(item);

            this.comboBox1.SelectedIndex = 0;
        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            this._functionTypeName = ((ListItem)this.comboBox1.SelectedItem).Value;
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

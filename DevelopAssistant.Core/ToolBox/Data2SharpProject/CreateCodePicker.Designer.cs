﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class CreateCodePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateCodePicker));
            this.checkNormEntity = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.txtTableName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.lblStateMsg = new System.Windows.Forms.Label();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.listView1 = new ICSharpCode.WinFormsUI.Controls.NListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.checkDTO = new System.Windows.Forms.CheckBox();
            this.checkBLL = new System.Windows.Forms.CheckBox();
            this.checkDAL = new System.Windows.Forms.CheckBox();
            this.checkIDAL = new System.Windows.Forms.CheckBox();
            this.checkModel = new System.Windows.Forms.CheckBox();
            this.cbox_identity = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new ICSharpCode.WinFormsUI.Controls.NProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPKey = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.combTypes = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.txtModelpath = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNameSpace = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtOutMsg = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkNormEntity
            // 
            this.checkNormEntity.AutoSize = true;
            this.checkNormEntity.Location = new System.Drawing.Point(312, 22);
            this.checkNormEntity.Name = "checkNormEntity";
            this.checkNormEntity.Size = new System.Drawing.Size(90, 16);
            this.checkNormEntity.TabIndex = 1;
            this.checkNormEntity.Text = "Norm Entity";
            this.checkNormEntity.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.txtTableName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkNormEntity);
            this.groupBox1.Location = new System.Drawing.Point(11, 3);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(656, 54);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtTableName
            // 
            this.txtTableName.BackColor = System.Drawing.Color.White;
            this.txtTableName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTableName.Enabled = true;
            this.txtTableName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtTableName.FousedColor = System.Drawing.Color.Orange;
            this.txtTableName.Icon = null;
            this.txtTableName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtTableName.IsButtonTextBox = false;
            this.txtTableName.IsClearTextBox = false;
            this.txtTableName.IsPasswordTextBox = false;
            this.txtTableName.Location = new System.Drawing.Point(72, 17);
            this.txtTableName.MaxLength = 32767;
            this.txtTableName.Multiline = false;
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.PasswordChar = '\0';
            this.txtTableName.Placeholder = null;
            this.txtTableName.ReadOnly = false;
            this.txtTableName.Size = new System.Drawing.Size(224, 24);
            this.txtTableName.TabIndex = 6;
            this.txtTableName.UseSystemPasswordChar = false;
            this.txtTableName.XBackColor = System.Drawing.Color.White;
            this.txtTableName.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "表名：";
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.lblStateMsg);
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 582);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(678, 60);
            this.panel1.TabIndex = 3;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblStateMsg
            // 
            this.lblStateMsg.AutoSize = true;
            this.lblStateMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblStateMsg.Location = new System.Drawing.Point(21, 26);
            this.lblStateMsg.Name = "lblStateMsg";
            this.lblStateMsg.Size = new System.Drawing.Size(53, 12);
            this.lblStateMsg.TabIndex = 2;
            this.lblStateMsg.Text = "生成成功";
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(494, 16);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 32);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "生成";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(585, 16);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 32);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.BackColor = System.Drawing.SystemColors.Window;
            this.listView1.ColumnHeaderHeight = 30;
            this.listView1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView1.GridLines = true;
            this.listView1.ItemHeight = 28;
            this.listView1.LabelEdit = true;
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.Location = new System.Drawing.Point(11, 64);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(656, 262);
            this.listView1.StateImageList = this.imageList1;
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "PKey.png");
            this.imageList1.Images.SetKeyName(1, "text_24px.png");
            this.imageList1.Images.SetKeyName(2, "Feild.png");
            this.imageList1.Images.SetKeyName(3, "Document.png");
            this.imageList1.Images.SetKeyName(4, "Coloumn.png");
            this.imageList1.Images.SetKeyName(5, "modify_24px.png");
            this.imageList1.Images.SetKeyName(6, "PrimaryKey.png");
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "生成的类型：";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox3.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox3.Controls.Add(this.checkDTO);
            this.groupBox3.Controls.Add(this.checkBLL);
            this.groupBox3.Controls.Add(this.checkDAL);
            this.groupBox3.Controls.Add(this.checkIDAL);
            this.groupBox3.Controls.Add(this.checkModel);
            this.groupBox3.Location = new System.Drawing.Point(96, 79);
            this.groupBox3.MarginWidth = 0;
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 39);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Title = "";
            this.groupBox3.TopBlackColor = System.Drawing.Color.Empty;
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // checkDTO
            // 
            this.checkDTO.AutoSize = true;
            this.checkDTO.Location = new System.Drawing.Point(60, 13);
            this.checkDTO.Name = "checkDTO";
            this.checkDTO.Size = new System.Drawing.Size(42, 16);
            this.checkDTO.TabIndex = 8;
            this.checkDTO.Text = "DTO";
            this.checkDTO.UseVisualStyleBackColor = true;
            // 
            // checkBLL
            // 
            this.checkBLL.AutoSize = true;
            this.checkBLL.Location = new System.Drawing.Point(207, 13);
            this.checkBLL.Name = "checkBLL";
            this.checkBLL.Size = new System.Drawing.Size(42, 16);
            this.checkBLL.TabIndex = 7;
            this.checkBLL.Text = "BLL";
            this.checkBLL.UseVisualStyleBackColor = true;
            // 
            // checkDAL
            // 
            this.checkDAL.AutoSize = true;
            this.checkDAL.Location = new System.Drawing.Point(159, 13);
            this.checkDAL.Name = "checkDAL";
            this.checkDAL.Size = new System.Drawing.Size(42, 16);
            this.checkDAL.TabIndex = 6;
            this.checkDAL.Text = "DAL";
            this.checkDAL.UseVisualStyleBackColor = true;
            // 
            // checkIDAL
            // 
            this.checkIDAL.AutoSize = true;
            this.checkIDAL.Location = new System.Drawing.Point(106, 13);
            this.checkIDAL.Name = "checkIDAL";
            this.checkIDAL.Size = new System.Drawing.Size(48, 16);
            this.checkIDAL.TabIndex = 5;
            this.checkIDAL.Text = "IDAL";
            this.checkIDAL.UseVisualStyleBackColor = true;
            // 
            // checkModel
            // 
            this.checkModel.AutoSize = true;
            this.checkModel.Location = new System.Drawing.Point(6, 13);
            this.checkModel.Name = "checkModel";
            this.checkModel.Size = new System.Drawing.Size(54, 16);
            this.checkModel.TabIndex = 4;
            this.checkModel.Text = "Model";
            this.checkModel.UseVisualStyleBackColor = true;
            // 
            // cbox_identity
            // 
            this.cbox_identity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbox_identity.AutoSize = true;
            this.cbox_identity.Location = new System.Drawing.Point(558, 10);
            this.cbox_identity.Name = "cbox_identity";
            this.cbox_identity.Size = new System.Drawing.Size(84, 16);
            this.cbox_identity.TabIndex = 31;
            this.cbox_identity.Text = "标识为自增";
            this.cbox_identity.UseVisualStyleBackColor = true;
            this.cbox_identity.CheckedChanged += new System.EventHandler(this.cbox_identity_CheckedChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.BaseColor = System.Drawing.Color.Empty;
            this.progressBar1.FaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(176)))), ((int)(((byte)(227)))));
            this.progressBar1.LinearBrush = false;
            this.progressBar1.Location = new System.Drawing.Point(420, 89);
            this.progressBar1.Maximum = 100;
            this.progressBar1.Minimum = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.ShowValue = false;
            this.progressBar1.Size = new System.Drawing.Size(222, 20);
            this.progressBar1.TabIndex = 30;
            this.progressBar1.Value = 0;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(361, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "进度：";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtPKey
            // 
            this.txtPKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtPKey.BackColor = System.Drawing.Color.White;
            this.txtPKey.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPKey.Enabled = true;
            this.txtPKey.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPKey.FousedColor = System.Drawing.Color.Orange;
            this.txtPKey.Icon = null;
            this.txtPKey.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPKey.IsButtonTextBox = false;
            this.txtPKey.IsClearTextBox = false;
            this.txtPKey.IsPasswordTextBox = false;
            this.txtPKey.Location = new System.Drawing.Point(420, 6);
            this.txtPKey.MaxLength = 32767;
            this.txtPKey.Multiline = false;
            this.txtPKey.Name = "txtPKey";
            this.txtPKey.PasswordChar = '\0';
            this.txtPKey.Placeholder = null;
            this.txtPKey.ReadOnly = false;
            this.txtPKey.Size = new System.Drawing.Size(121, 24);
            this.txtPKey.TabIndex = 28;
            this.txtPKey.UseSystemPasswordChar = false;
            this.txtPKey.XBackColor = System.Drawing.Color.White;
            this.txtPKey.XForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPKey.TextChanged += new System.EventHandler(this.txtPKey_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(361, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "类型：";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // combTypes
            // 
            this.combTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.combTypes.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTypes.Font = new System.Drawing.Font("宋体", 9F);
            this.combTypes.FormattingEnabled = true;
            this.combTypes.ItemIcon = null;
            this.combTypes.Location = new System.Drawing.Point(420, 46);
            this.combTypes.Name = "combTypes";
            this.combTypes.Size = new System.Drawing.Size(196, 22);
            this.combTypes.TabIndex = 26;
            this.combTypes.SelectedIndexChanged += new System.EventHandler(this.combTypes_SelectedIndexChanged);
            // 
            // txtModelpath
            // 
            this.txtModelpath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtModelpath.BackColor = System.Drawing.Color.White;
            this.txtModelpath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtModelpath.Enabled = true;
            this.txtModelpath.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtModelpath.FousedColor = System.Drawing.Color.Orange;
            this.txtModelpath.Icon = null;
            this.txtModelpath.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtModelpath.IsButtonTextBox = false;
            this.txtModelpath.IsClearTextBox = false;
            this.txtModelpath.IsPasswordTextBox = false;
            this.txtModelpath.Location = new System.Drawing.Point(98, 45);
            this.txtModelpath.MaxLength = 32767;
            this.txtModelpath.Multiline = false;
            this.txtModelpath.Name = "txtModelpath";
            this.txtModelpath.PasswordChar = '\0';
            this.txtModelpath.Placeholder = null;
            this.txtModelpath.ReadOnly = false;
            this.txtModelpath.Size = new System.Drawing.Size(217, 24);
            this.txtModelpath.TabIndex = 25;
            this.txtModelpath.UseSystemPasswordChar = false;
            this.txtModelpath.XBackColor = System.Drawing.Color.White;
            this.txtModelpath.XForeColor = System.Drawing.SystemColors.WindowText;
            this.txtModelpath.TextChanged += new System.EventHandler(this.txtModelpath_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(3, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 24;
            this.label4.Text = "二级命名空间：";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNameSpace.BackColor = System.Drawing.Color.White;
            this.txtNameSpace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNameSpace.Enabled = true;
            this.txtNameSpace.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtNameSpace.FousedColor = System.Drawing.Color.Orange;
            this.txtNameSpace.Icon = null;
            this.txtNameSpace.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtNameSpace.IsButtonTextBox = false;
            this.txtNameSpace.IsClearTextBox = false;
            this.txtNameSpace.IsPasswordTextBox = false;
            this.txtNameSpace.Location = new System.Drawing.Point(98, 6);
            this.txtNameSpace.MaxLength = 32767;
            this.txtNameSpace.Multiline = false;
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.PasswordChar = '\0';
            this.txtNameSpace.Placeholder = null;
            this.txtNameSpace.ReadOnly = false;
            this.txtNameSpace.Size = new System.Drawing.Size(217, 24);
            this.txtNameSpace.TabIndex = 23;
            this.txtNameSpace.UseSystemPasswordChar = false;
            this.txtNameSpace.XBackColor = System.Drawing.Color.White;
            this.txtNameSpace.XForeColor = System.Drawing.SystemColors.WindowText;
            this.txtNameSpace.TextChanged += new System.EventHandler(this.txtNameSpace_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(361, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "主键：";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(3, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "顶级命名空间：";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtOutMsg
            // 
            this.txtOutMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutMsg.BackColor = System.Drawing.Color.White;
            this.txtOutMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOutMsg.Enabled = true;
            this.txtOutMsg.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOutMsg.FousedColor = System.Drawing.Color.Orange;
            this.txtOutMsg.Icon = null;
            this.txtOutMsg.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtOutMsg.IsButtonTextBox = false;
            this.txtOutMsg.IsClearTextBox = false;
            this.txtOutMsg.IsPasswordTextBox = false;
            this.txtOutMsg.Location = new System.Drawing.Point(10, 463);
            this.txtOutMsg.MaxLength = 32767;
            this.txtOutMsg.Multiline = true;
            this.txtOutMsg.Name = "txtOutMsg";
            this.txtOutMsg.PasswordChar = '\0';
            this.txtOutMsg.Placeholder = null;
            this.txtOutMsg.ReadOnly = false;
            this.txtOutMsg.Size = new System.Drawing.Size(656, 72);
            this.txtOutMsg.TabIndex = 34;
            this.txtOutMsg.UseSystemPasswordChar = false;
            this.txtOutMsg.XBackColor = System.Drawing.Color.White;
            this.txtOutMsg.XForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOutMsg.TextChanged += new System.EventHandler(this.txtOutMsg_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtNameSpace);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cbox_identity);
            this.panel2.Controls.Add(this.txtModelpath);
            this.panel2.Controls.Add(this.progressBar1);
            this.panel2.Controls.Add(this.combTypes);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtPKey);
            this.panel2.Location = new System.Drawing.Point(10, 332);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(656, 125);
            this.panel2.TabIndex = 35;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.listView1);
            this.panel3.Controls.Add(this.txtOutMsg);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(6, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(678, 548);
            this.panel3.TabIndex = 36;
            // 
            // CreateCodePicker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(690, 648);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "CreateCodePicker";
            this.Text = "生成代码";
            this.Load += new System.EventHandler(this.CreateCodePicker_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkNormEntity;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtTableName;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private ICSharpCode.WinFormsUI.Controls.NListView listView1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBLL;
        private System.Windows.Forms.CheckBox checkDAL;
        private System.Windows.Forms.CheckBox checkIDAL;
        private System.Windows.Forms.CheckBox checkModel;
        private System.Windows.Forms.CheckBox cbox_identity;
        private ICSharpCode.WinFormsUI.Controls.NProgressBar progressBar1;
        private System.Windows.Forms.Label label6;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPKey;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combTypes;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtModelpath;
        private System.Windows.Forms.Label label4;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtNameSpace;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtOutMsg;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblStateMsg;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox checkDTO;
        private System.Windows.Forms.Panel panel3;
    }
}
﻿using DevelopAssistant.Common;
using DevelopAssistant.Core.DBMS;
using DevelopAssistant.Service;
using DevelopAssistant.Service.TemplatingEngine;
using ICSharpCode.WinFormsUI.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class CreateCodePicker : ToolBoxBase
    {
        MainForm mainform;
        DataBaseServer server;

        TextTemplatingEngine host = new TextTemplatingEngine(); 

        public string TableName { get; set; }

        public CreateCodePicker()
        {
            InitializeComponent();
            InitializeControls();
        }

        public CreateCodePicker(string text, string code)
            : this()
        {
            TableName = text;
            this.txtTableName.Text = TableName;
        }

        private void InitializeControls()
        {
            ListItem item = null;
            item = new ListItem();
            item.Text = "DbTypes.Sql";
            item.Value = "Sql";
            combTypes.Items.Add(item);
            item = new ListItem();
            item.Text = "DbTypes.PostgreSql";
            item.Value = "PostgreSql";
            combTypes.Items.Add(item);
            item = new ListItem();
            item.Text = "DbTypes.Oracle";
            item.Value = "Oracle";
            combTypes.Items.Add(item);
            item = new ListItem();
            item.Text = "DbTypes.MySql";
            item.Value = "MySql";
            combTypes.Items.Add(item);
            item = new ListItem();
            item.Text = "DbTypes.OleDb";
            item.Value = "OleDb";
            combTypes.Items.Add(item);
            item = new ListItem();
            item.Text = "DbTypes.SqlLite";
            item.Value = "SqlLite";
            combTypes.Items.Add(item);

            combTypes.SelectedIndex = 0;

            lblStateMsg.Text = "";
            txtNameSpace.Text = "ICsharpCode";
            txtModelpath.Text = "Demo";

        }
        
        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            this.txtOutMsg.Text = "";

            host.NameSpace = txtNameSpace.Text;
            host.SpaceName = txtModelpath.Text;
            host.ClassName = txtTableName.Text;
            host.DType = DbTypes.Sql;
            host.FileExtension = ".cs";

            host.ProviderName = server.ProviderName;
            host.ConnectionString = server.ConnectionString;

            switch (server.ProviderName.ToLower())
            {
                case "system.data.sql":
                    host.DType = DbTypes.Sql;
                    break;
                case "system.data.sqlite":
                    host.DType = DbTypes.SqlLite;
                    break;
                case "system.data.mysql":
                    host.DType = DbTypes.MySql;
                    break;
                case "system.data.postgresql":
                    host.DType = DbTypes.PostgreSql;
                    break;
                case "system.data.oracle":
                    host.DType = DbTypes.Oracle;
                    break;
                case "system.data.oledb":
                    host.DType = DbTypes.OleDb;
                    break;
            }

            int selectCount=0;
            foreach (var control in groupBox3.Controls)
            {
                if (control is CheckBox)
                {
                    if (((CheckBox)control).Checked)
                        selectCount++;
                }
            }

            if (selectCount < 1)
            {
                MessageBox.Show("请选择要生成的内容");
                return;
            }


            CodeTextDocument textPad = new CodeTextDocument(mainform);

            if (checkModel.Checked)
            {
                textPad.AddCodeTabPage("", "Model代码");
            }
            if (checkDTO.Checked) 
            {
                textPad.AddCodeTabPage("", "DTO代码");
            }
            if (checkIDAL.Checked)
            {
                textPad.AddCodeTabPage("", "IDAL代码");
            }
            if (checkDAL.Checked) 
            {
                textPad.AddCodeTabPage("", "DAL代码");
            }
            if (checkBLL.Checked) 
            {
                textPad.AddCodeTabPage("", "BLL代码");
            }

            mainform.AddNewContent(textPad, TableName + " 代码");

            textPad.SelectedDatabaseType = ((ListItem)combTypes.Items[combTypes.SelectedIndex]).Value;

            backgroundWorker1.RunWorkerAsync(textPad);
            
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateCodePicker_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());

            this.btnApplyOk.Focus();
            mainform = (MainForm)Application.OpenForms["MainForm"];
            server = mainform.ConnectedDataBaseServer;

            this.panel1.BorderColor = mainform.XTheme.FormBorderOutterColor;

            listView1.PerformLayout();
            listView1.BeginUpdate();

            this.listView1.Columns.AddRange(new NColumnHeader[]{
               new NColumnHeader(){ Name ="名称", Text ="名称", Width =130 },
               new NColumnHeader(){ Name ="数据类型", Text ="数据类型", Width =150 },
               new NColumnHeader(){ Name ="标识", Text ="标识", Width =120 },
               new NColumnHeader(){ Name ="描述", Text ="描述", Width =100 }
            });

            using (var db = Utility.GetAdohelper(server))
            {
                DataTable table = db.GetTableObject(TableName);
                foreach (DataRow row in table.Rows)
                {
                    string columnName = row["ColumnName"] + "";
                    string typeName = row["TypeName"] + "";
                    string cisnull = row["CisNull"] + "";
                    string length = row["Length"] + "";
                    string describ = row["Describ"] + "";

                    cisnull = cisnull.Trim(',');

                    NListViewItem item = new NListViewItem(
                        new string[] { columnName, SnippetBase.getDataBaseDataType(typeName,length,server.ProviderName), cisnull, describ });
                    item.Text = columnName;
                    item.Name = columnName;

                    if (cisnull.Contains("pk"))
                    {
                        item.StateImageIndex = 0;
                        this.txtPKey.Text = columnName;
                    }
                    else
                    {
                        item.StateImageIndex = 1;
                    }

                    this.listView1.Items.Add(item);
                }
            }            

            listView1.EndUpdate();
            listView1.ResumeLayout();

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtOutMsg_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void cbox_identity_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtPKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void combTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtModelpath_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtNameSpace_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private string GetTitleLetter(string word)
        {
            try
            {
                if (string.IsNullOrEmpty(word)) return word;
                return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(word);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            return word.Substring(0, 1).ToUpper() + word.Substring(1);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (e.Argument != null)
                {
                    CodeTextDocument textPad = e.Argument as CodeTextDocument;

                    using (var db = Utility.GetAdohelper(host.ConnectionString, host.ProviderName))
                    {
                        //host.Server = server;               
                        host.Table = db.GetTableObject(txtTableName.Text, NORM.DataBase.DataBaseSchema.Table);
                    }

                    string CodeText = string.Empty;

                    host.Pk_Column = new ColumnInfo();
                    host.Pk_Column.ColumnName = txtPKey.Text;

                    switch (textPad.SelectedDatabaseType)
                    {
                        case "Sql":
                            host.ToType = DbTypes.Sql;
                            host.Pk_Column.DefaultText = "@";
                            host.Pk_Column.TypeName = "Int";
                            host.Pk_Column.Length = "4";
                            host.Pk_Column.DefaultVal = "SqlDbType";
                            break;
                        case "MySql":
                            host.ToType = DbTypes.MySql;
                            host.Pk_Column.DefaultText = "?";
                            host.Pk_Column.TypeName = "Int32";
                            host.Pk_Column.Length = "4";
                            host.Pk_Column.DefaultVal = "MySqlDbType";
                            break;
                        case "Oracle":
                            host.ToType = DbTypes.Oracle;
                            host.Pk_Column.DefaultText = ":";
                            host.Pk_Column.TypeName = "Number";
                            host.Pk_Column.Length = "8";
                            host.Pk_Column.DefaultVal = "OracleType";
                            break;
                        case "OleDb":
                            host.ToType = DbTypes.OleDb;
                            host.Pk_Column.DefaultText = "@";
                            host.Pk_Column.TypeName = "Integer";
                            host.Pk_Column.Length = "4";
                            host.Pk_Column.DefaultVal = "OleDbType";
                            break;
                        case "SqlLite":
                            host.ToType = DbTypes.SqlLite;
                            host.Pk_Column.DefaultText = "@";
                            host.Pk_Column.TypeName = "Int32";
                            host.Pk_Column.Length = "4";
                            host.Pk_Column.DefaultVal = "DbType";
                            break;
                        case "PostgreSql":
                            host.ToType = DbTypes.PostgreSql;
                            host.Pk_Column.DefaultText = "@";
                            host.Pk_Column.TypeName = "Integer";
                            host.Pk_Column.Length = "4";
                            host.Pk_Column.DefaultVal = "NpgsqlDbType";
                            break;
                    }

                    foreach (DataRow row in host.Table.Rows)
                    {
                        string columnType = row["TypeName"] + "";
                        string columnInfo = row["CisNull"] + "";
                        string columnLength = row["Length"] + "";
                        if (columnInfo.Contains("pk,"))
                        {
                            host.Pk_Column.TypeName = GetTitleLetter(columnType);
                            host.Pk_Column.Length = columnLength;
                        }
                    }         

                    this.Invoke(new MethodInvoker(() => { lblStateMsg.Text = "正在生成..."; }));

                    string Message = string.Empty;

                    if (checkModel.Checked)
                    {
                        if (checkNormEntity.Checked)
                        {
                            CodeText = CreateModel.CreateNormEntity(host, TableName, out Message);
                        }
                        else
                        {
                            CodeText = CreateModel.CreateNormModel(host, TableName, out Message);
                        }

                        this.Invoke(new MethodInvoker(() => { this.txtOutMsg.Text += Message; }));

                    }

                    this.Invoke(new MethodInvoker(() => { textPad.SetCodeTabPageText(CodeText, "Model代码"); }));

                    backgroundWorker1.ReportProgress(1, 5);

                    CodeText = string.Empty;

                    if (checkDTO.Checked) 
                    {
                        if (checkNormEntity.Checked)
                        {
                            CodeText = CreateDTO.CreateEntityDTO(host, TableName, out Message);
                        }
                        else
                        {
                            CodeText = CreateDTO.CreateModelDTO(host, TableName, out Message);
                        }

                        this.Invoke(new MethodInvoker(() => { this.txtOutMsg.Text += Message; }));

                    }

                    this.Invoke(new MethodInvoker(() => { textPad.SetCodeTabPageText(CodeText, "DTO代码"); }));

                    backgroundWorker1.ReportProgress(2, 5);

                    CodeText = string.Empty;


                    if (checkIDAL.Checked)
                    {
                        if (checkNormEntity.Checked)
                        {
                            CodeText = CreateIDAL.CreateEntityIDAL(host, TableName, out Message);
                        }
                        else
                        {
                            CodeText = CreateIDAL.CreateModelDAL(host, TableName, out Message);
                        }

                        this.Invoke(new MethodInvoker(() => { this.txtOutMsg.Text += Message; }));

                    }


                    this.Invoke(new MethodInvoker(() => { textPad.SetCodeTabPageText(CodeText, "IDAL代码"); }));
                    
                    backgroundWorker1.ReportProgress(3, 5);

                    CodeText = string.Empty;


                    if (checkDAL.Checked)
                    {
                        if (checkNormEntity.Checked)
                        {
                            CodeText = CreateDAL.CreateEntityDAL(host, TableName, out Message);
                        }
                        else
                        {
                            CodeText = CreateDAL.CreateModelDAL(host, TableName, out Message);
                        }

                        this.Invoke(new MethodInvoker(() => { this.txtOutMsg.Text += Message; }));

                    }


                    this.Invoke(new MethodInvoker(() => { textPad.SetCodeTabPageText(CodeText, "DAL代码"); }));
                    
                    backgroundWorker1.ReportProgress(4, 5);

                    CodeText = string.Empty;


                    if (checkBLL.Checked)
                    {
                        if (checkNormEntity.Checked)
                        {
                            CodeText = CreateBLL.CreateEntityBLL(host, TableName, out Message);
                        }
                        else
                        {
                            CodeText = CreateBLL.CreateModelBLL(host, TableName, out Message);
                        }

                        this.Invoke(new MethodInvoker(() => { this.txtOutMsg.Text += Message; }));

                    }

                    this.Invoke(new MethodInvoker(() => { textPad.SetCodeTabPageText(CodeText, "BLL代码"); }));
                    backgroundWorker1.ReportProgress(5, 5);

                    e.Result = textPad;
                }
                else
                {
                    e.Result = null;
                }
            }
            catch (Exception ex)
            {
                e.Result = null;
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                CodeTextDocument document = (CodeTextDocument)e.Result;
                //document.SetShowGuidelines(true);
                lblStateMsg.Text = "生成完成";
                this.Close();
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int total = (int)e.UserState;
            int index = e.ProgressPercentage;
            double value = index * 1.0 / total * 100;
            this.progressBar1.Value = (int)value;
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color toolBackColor = SystemColors.Control;
            Color progressBackColor = SystemColors.ControlLight;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    textBackColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    progressBackColor = SystemColors.ControlLight;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(250, 250, 250);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    progressBackColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            txtTableName.XForeColor = foreColor;
            txtTableName.XBackColor = textBackColor;
            txtModelpath.XForeColor = foreColor;
            txtModelpath.XBackColor = textBackColor;
            txtPKey.XForeColor = foreColor;
            txtPKey.XBackColor = textBackColor;
            txtNameSpace.XForeColor = foreColor;
            txtNameSpace.XBackColor = textBackColor;
            txtOutMsg.XForeColor = foreColor;
            txtOutMsg.XBackColor = textBackColor;
            btnApplyOk.ForeColor = foreColor;
            btnApplyOk.BackColor = toolBackColor;
            btnCancle.ForeColor = foreColor;
            btnCancle.BackColor = toolBackColor;            
            panel1.ForeColor = foreColor;
            panel1.BackColor = toolBackColor;        
            panel2.ForeColor = foreColor;
            panel2.BackColor = toolBackColor;
            panel3.ForeColor = foreColor;
            panel3.BackColor = backColor;
            combTypes.ForeColor = foreColor;
            combTypes.BackColor = backColor;
            progressBar1.ForeColor = foreColor;
            progressBar1.BackColor = backColor;
            progressBar1.BaseColor = progressBackColor;            

            this.listView1.SetTheme(themeName);
        }
    }
}

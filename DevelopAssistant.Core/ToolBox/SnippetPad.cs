﻿using DevelopAssistant.Common;
using DevelopAssistant.Service;
using DevelopAssistant.Service.SnippetCode;
using ICSharpCode.WinFormsUI.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class SnippetPad : ICSharpCode.WinFormsUI.Docking.DockContent, IDocumentContent
    {
        private string SnippetId = "";

        public SnippetPad()
        {
            InitializeComponent();
        }

        public SnippetPad(Form mainForm)
            : this()
        {
            this.Load += SnippetPad_Load;
            this.EditorTextBox.OnCommandExecute += new CommandExecuteDelegate(CommandExecute_Click); 
        }

        private void SnippetPad_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }

        public override void OnThemeChanged(EventArgs e)
        {
            Color backColor = SystemColors.Control;
            Color foreColor = SystemColors.ControlText;

            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    backColor = SystemColors.Control;
                    foreColor = SystemColors.ControlText;
                    break;
                case "Black":
                    backColor = Color.FromArgb(045, 045, 045);
                    foreColor = Color.FromArgb(240, 240, 240);
                    break;
            }

            this.ForeColor = foreColor;
            this.BackColor = backColor;

            this.EditorTextBox.ForeColor = foreColor;
            this.EditorTextBox.BackColor = backColor;
            this.EditorTextBox.SetTheme(themeName);
        }

        private string DocumentTypeParse(string formats)
        {
            string result = "None";
            switch (formats)
            {
                case ".cs":
                    result = "Csharp";
                    break;
                case ".js":
                    result = "Javascript";
                    break;
                case ".json":
                    result = "Json";
                    break;
                case ".sql":
                    result = "TSql";
                    break;
                case ".py":
                    result = "Python";
                    break;
                case ".xml":
                    result = "XML";
                    break;
                case ".config":
                    result = "Configuation";
                    break;
            }
            return result;
        }

        public void LoadSnippet(object id, string text,string formats)
        {
            this.EditorTextBox.EditModel = false;
            this.SnippetId = id.ToString();
            this.EditorTextBox.Text = text;
            this.EditorTextBox.DocumentType = (DocumentType)Enum.Parse(typeof(DocumentType), DocumentTypeParse(formats));               
        }

        public void FindAndReplace(FindAndReplaceRequest request)
        {

        }

        public void CommandExecute_Click(object sender, StripButtonEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(SnippetId))
                    return;

                DocumentType docType = this.EditorTextBox.DocumentType;

                string formats = "";

                switch (docType)
                {
                    case DocumentType.TSql:
                        formats = ".sql";
                        break;
                    case DocumentType.Text:
                    case DocumentType.Word:
                        formats = ".text";
                        break;
                    case DocumentType.Csharp:
                        formats = ".cs";
                        break;
                    case DocumentType.Javascript:
                        formats = ".js";
                        break;
                    case DocumentType.Json:
                        formats = ".json";
                        break;
                }

                switch (e.name)
                {
                    case "toolStripButtonSave":
                        using (var db = NORM.DataBase.DataBaseFactory.Default)
                        {
                            string strSql = string.Empty;
                            string strContent = SnippetConvert.EncodeSnippetContent(EditorTextBox.Text);                           

                            strSql = " UPDATE [T_SnippetTree] SET Formats='" + formats + "', ";
                            strSql += " [Snippet]='" + strContent + "' WHERE [ID]='" + SnippetId + "' ";

                            db.Execute(CommandType.Text, strSql, null);

                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                new MessageDialog(DevelopAssistant.Core.Properties.Resources.error_32px, ex.Message).ShowDialog(this);
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }                       
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.EditorTextBox.ReleaseDispose();
            base.OnClosing(e);
        }

    }
}

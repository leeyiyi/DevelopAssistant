﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class SnippetTree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SnippetTree));
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnReplace = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnLook = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SnippetTreeView = new ICSharpCode.WinFormsUI.Controls.NTreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.NToolBar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Controls.Add(this.btnReplace);
            this.NToolBar.Controls.Add(this.btnLook);
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(6, 417);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(250, 67);
            this.NToolBar.TabIndex = 12;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnReplace
            // 
            this.btnReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplace.Location = new System.Drawing.Point(154, 16);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(75, 32);
            this.btnReplace.TabIndex = 4;
            this.btnReplace.Text = "取消";
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // btnLook
            // 
            this.btnLook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLook.Location = new System.Drawing.Point(64, 16);
            this.btnLook.Name = "btnLook";
            this.btnLook.Size = new System.Drawing.Size(75, 32);
            this.btnLook.TabIndex = 3;
            this.btnLook.Text = "确定";
            this.btnLook.UseVisualStyleBackColor = true;
            this.btnLook.Click += new System.EventHandler(this.btnLook_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.SnippetTreeView);
            this.groupBox1.Location = new System.Drawing.Point(6, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 374);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // SnippetTreeView
            // 
            this.SnippetTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SnippetTreeView.BackColor = System.Drawing.SystemColors.Window;
            this.SnippetTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SnippetTreeView.Cursor = System.Windows.Forms.Cursors.Default;
            this.SnippetTreeView.DragDropMarkColor = System.Drawing.Color.Black;
            this.SnippetTreeView.ImageList = this.imageList1;
            this.SnippetTreeView.LineColor = System.Drawing.SystemColors.ControlDark;
            this.SnippetTreeView.Location = new System.Drawing.Point(6, 12);
            this.SnippetTreeView.Model = null;
            this.SnippetTreeView.Name = "SnippetTreeView";
            this.SnippetTreeView.NBorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.SnippetTreeView.SelectedNode = null;
            this.SnippetTreeView.Size = new System.Drawing.Size(237, 356);
            this.SnippetTreeView.TabIndex = 0;
            this.SnippetTreeView.NodeMouseClick += new ICSharpCode.WinFormsUI.Controls.NTreeNodeMouseClickEventHandler(this.SnippetTree_NodeMouseClick);
            this.SnippetTreeView.Collapsed += new ICSharpCode.WinFormsUI.Controls.NTreeViewEventHandler(this.SnippetTree_Collapsed);
            this.SnippetTreeView.Expanded += new ICSharpCode.WinFormsUI.Controls.NTreeViewEventHandler(this.SnippetTree_Expanded);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "zTree02.png");
            this.imageList1.Images.SetKeyName(1, "zTree04.png");
            this.imageList1.Images.SetKeyName(2, "zTree05.png");
            this.imageList1.Images.SetKeyName(3, "zTree06.png");
            // 
            // SnippetTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 490);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SnippetTree";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "请选择";
            this.Load += new System.EventHandler(this.SnippetTreeSelect_Load);
            this.NToolBar.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton btnReplace;
        private ICSharpCode.WinFormsUI.Controls.NButton btnLook;
        private System.Windows.Forms.GroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTreeView SnippetTreeView;
        private System.Windows.Forms.ImageList imageList1;

    }
}
﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class ShowCellValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowCellValue));
            this.txtValueShow = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtValueShow
            // 
            this.txtValueShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValueShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValueShow.Location = new System.Drawing.Point(14, 46);
            this.txtValueShow.Multiline = true;
            this.txtValueShow.Name = "txtValueShow";
            this.txtValueShow.ReadOnly = true;
            this.txtValueShow.Size = new System.Drawing.Size(405, 349);
            this.txtValueShow.TabIndex = 0;
            // 
            // ShowCellValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 411);
            this.Controls.Add(this.txtValueShow);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShowCellValue";
            this.Text = "Cell 值";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtValueShow;
    }
}
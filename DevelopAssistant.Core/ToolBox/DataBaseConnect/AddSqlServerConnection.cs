﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Drawing;

namespace DevelopAssistant.Core.ToolBox
{
    public class AddSqlServerConnection : AddDataBaseConnection
    {
        private ICSharpCode.WinFormsUI.Controls.NComboBox combServer;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPort;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtInstance;
        private System.Windows.Forms.CheckBox chkInstance;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox gboxLoginPanel;
        private System.Windows.Forms.Label label7;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUser_ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkLoginStyle;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combDataBases;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkEfficient;
        private System.Windows.Forms.Label tester;
        private Panel panel2;
        private System.Windows.Forms.Label label1;

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddSqlServerConnection));
            this.label1 = new System.Windows.Forms.Label();
            this.combServer = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPort = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtInstance = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.chkInstance = new System.Windows.Forms.CheckBox();
            this.gboxLoginPanel = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtUser_ID = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkLoginStyle = new System.Windows.Forms.CheckBox();
            this.combDataBases = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkEfficient = new System.Windows.Forms.CheckBox();
            this.tester = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gboxLoginPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "服务器地址：";
            // 
            // combServer
            // 
            this.combServer.BackColor = System.Drawing.Color.White;
            this.combServer.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combServer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combServer.DropDownButtonWidth = 20;
            this.combServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combServer.DropDownWidth = 0;
            this.combServer.Font = new System.Drawing.Font("宋体", 9F);
            this.combServer.FormattingEnabled = true;
            this.combServer.ItemHeight = 18;
            this.combServer.ItemIcon = null;
            this.combServer.Location = new System.Drawing.Point(83, 18);
            this.combServer.Name = "combServer";
            this.combServer.SelectedIndex = -1;
            this.combServer.SelectedItem = null;
            this.combServer.Size = new System.Drawing.Size(131, 24);
            this.combServer.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(224, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "端口：";
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.Color.White;
            this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPort.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPort.FousedColor = System.Drawing.Color.Orange;
            this.txtPort.Icon = null;
            this.txtPort.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPort.IsButtonTextBox = false;
            this.txtPort.IsClearTextBox = false;
            this.txtPort.IsPasswordTextBox = false;
            this.txtPort.Location = new System.Drawing.Point(262, 18);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.PasswordChar = '\0';
            this.txtPort.Placeholder = null;
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(63, 24);
            this.txtPort.TabIndex = 4;
            this.txtPort.UseSystemPasswordChar = false;
            this.txtPort.XBackColor = System.Drawing.Color.White;
            this.txtPort.XDisableColor = System.Drawing.Color.Empty;
            this.txtPort.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(47, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "实例：";
            // 
            // txtInstance
            // 
            this.txtInstance.BackColor = System.Drawing.Color.White;
            this.txtInstance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInstance.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtInstance.FousedColor = System.Drawing.Color.Orange;
            this.txtInstance.Icon = null;
            this.txtInstance.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtInstance.IsButtonTextBox = false;
            this.txtInstance.IsClearTextBox = false;
            this.txtInstance.IsPasswordTextBox = false;
            this.txtInstance.Location = new System.Drawing.Point(83, 50);
            this.txtInstance.MaxLength = 32767;
            this.txtInstance.Multiline = false;
            this.txtInstance.Name = "txtInstance";
            this.txtInstance.PasswordChar = '\0';
            this.txtInstance.Placeholder = null;
            this.txtInstance.ReadOnly = false;
            this.txtInstance.Size = new System.Drawing.Size(131, 24);
            this.txtInstance.TabIndex = 6;
            this.txtInstance.UseSystemPasswordChar = false;
            this.txtInstance.XBackColor = System.Drawing.Color.White;
            this.txtInstance.XDisableColor = System.Drawing.Color.Empty;
            this.txtInstance.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // chkInstance
            // 
            this.chkInstance.AutoSize = true;
            this.chkInstance.BackColor = System.Drawing.Color.Transparent;
            this.chkInstance.Location = new System.Drawing.Point(226, 54);
            this.chkInstance.Name = "chkInstance";
            this.chkInstance.Size = new System.Drawing.Size(48, 16);
            this.chkInstance.TabIndex = 7;
            this.chkInstance.Text = "实例";
            this.chkInstance.UseVisualStyleBackColor = false;
            this.chkInstance.Click += new System.EventHandler(this.chkInstance_Click);
            // 
            // gboxLoginPanel
            // 
            this.gboxLoginPanel.BackColor = System.Drawing.Color.Transparent;
            this.gboxLoginPanel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.gboxLoginPanel.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.gboxLoginPanel.BottomBlackColor = System.Drawing.Color.Empty;
            this.gboxLoginPanel.Controls.Add(this.label7);
            this.gboxLoginPanel.Controls.Add(this.txtPassword);
            this.gboxLoginPanel.Controls.Add(this.txtUser_ID);
            this.gboxLoginPanel.Controls.Add(this.label5);
            this.gboxLoginPanel.Controls.Add(this.label4);
            this.gboxLoginPanel.Enabled = false;
            this.gboxLoginPanel.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.gboxLoginPanel.Location = new System.Drawing.Point(13, 90);
            this.gboxLoginPanel.MarginWidth = 0;
            this.gboxLoginPanel.Name = "gboxLoginPanel";
            this.gboxLoginPanel.Size = new System.Drawing.Size(313, 107);
            this.gboxLoginPanel.TabIndex = 8;
            this.gboxLoginPanel.TabStop = false;
            this.gboxLoginPanel.Title = "";
            this.gboxLoginPanel.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(257, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "如：sa";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.White;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(92, 65);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Placeholder = null;
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(159, 24);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.UseSystemPasswordChar = false;
            this.txtPassword.XBackColor = System.Drawing.Color.White;
            this.txtPassword.XDisableColor = System.Drawing.Color.Empty;
            this.txtPassword.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // txtUser_ID
            // 
            this.txtUser_ID.BackColor = System.Drawing.Color.White;
            this.txtUser_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUser_ID.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtUser_ID.FousedColor = System.Drawing.Color.Orange;
            this.txtUser_ID.Icon = null;
            this.txtUser_ID.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUser_ID.IsButtonTextBox = false;
            this.txtUser_ID.IsClearTextBox = false;
            this.txtUser_ID.IsPasswordTextBox = false;
            this.txtUser_ID.Location = new System.Drawing.Point(92, 27);
            this.txtUser_ID.MaxLength = 32767;
            this.txtUser_ID.Multiline = false;
            this.txtUser_ID.Name = "txtUser_ID";
            this.txtUser_ID.PasswordChar = '\0';
            this.txtUser_ID.Placeholder = null;
            this.txtUser_ID.ReadOnly = false;
            this.txtUser_ID.Size = new System.Drawing.Size(159, 24);
            this.txtUser_ID.TabIndex = 3;
            this.txtUser_ID.UseSystemPasswordChar = false;
            this.txtUser_ID.XBackColor = System.Drawing.Color.White;
            this.txtUser_ID.XDisableColor = System.Drawing.Color.Empty;
            this.txtUser_ID.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "密码：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "用户名：";
            // 
            // chkLoginStyle
            // 
            this.chkLoginStyle.AutoSize = true;
            this.chkLoginStyle.BackColor = System.Drawing.Color.Transparent;
            this.chkLoginStyle.Checked = true;
            this.chkLoginStyle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoginStyle.Location = new System.Drawing.Point(83, 86);
            this.chkLoginStyle.Name = "chkLoginStyle";
            this.chkLoginStyle.Size = new System.Drawing.Size(156, 16);
            this.chkLoginStyle.TabIndex = 0;
            this.chkLoginStyle.Text = "Windows 集成身份登陆：";
            this.chkLoginStyle.UseVisualStyleBackColor = false;
            this.chkLoginStyle.Click += new System.EventHandler(this.chkLoginStyle_Click);
            // 
            // combDataBases
            // 
            this.combDataBases.BackColor = System.Drawing.SystemColors.Control;
            this.combDataBases.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combDataBases.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combDataBases.DropDownButtonWidth = 20;
            this.combDataBases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combDataBases.DropDownWidth = 0;
            this.combDataBases.Font = new System.Drawing.Font("宋体", 9F);
            this.combDataBases.FormattingEnabled = true;
            this.combDataBases.ItemHeight = 18;
            this.combDataBases.ItemIcon = null;
            this.combDataBases.Location = new System.Drawing.Point(106, 211);
            this.combDataBases.Name = "combDataBases";
            this.combDataBases.SelectedIndex = -1;
            this.combDataBases.SelectedItem = null;
            this.combDataBases.Size = new System.Drawing.Size(159, 22);
            this.combDataBases.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(23, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "数据库名称：";
            // 
            // chkEfficient
            // 
            this.chkEfficient.AutoSize = true;
            this.chkEfficient.BackColor = System.Drawing.Color.Transparent;
            this.chkEfficient.Location = new System.Drawing.Point(106, 240);
            this.chkEfficient.Name = "chkEfficient";
            this.chkEfficient.Size = new System.Drawing.Size(96, 16);
            this.chkEfficient.TabIndex = 11;
            this.chkEfficient.Text = "高效连接模式";
            this.chkEfficient.UseVisualStyleBackColor = false;
            // 
            // tester
            // 
            this.tester.AutoSize = true;
            this.tester.BackColor = System.Drawing.Color.Transparent;
            this.tester.ForeColor = System.Drawing.Color.Blue;
            this.tester.Location = new System.Drawing.Point(212, 242);
            this.tester.Name = "tester";
            this.tester.Size = new System.Drawing.Size(53, 12);
            this.tester.TabIndex = 12;
            this.tester.Text = "测试连接";
            this.tester.Click += new System.EventHandler(this.tester_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.chkLoginStyle);
            this.panel2.Controls.Add(this.txtInstance);
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Controls.Add(this.combServer);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tester);
            this.panel2.Controls.Add(this.gboxLoginPanel);
            this.panel2.Controls.Add(this.chkEfficient);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.combDataBases);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.chkInstance);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(342, 273);
            this.panel2.TabIndex = 13;
            // 
            // AddSqlServerConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(354, 373);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(187, 75);
            this.Name = "AddSqlServerConnection";
            this.Text = "Sql数据库连接";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.gboxLoginPanel.ResumeLayout(false);
            this.gboxLoginPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        private void AddSqlServerConnection_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new ThemeEventArgs());
            txtInstance.Enabled = false;
            txtUser_ID.Enabled = gboxLoginPanel.Enabled;
            txtPassword.Enabled = gboxLoginPanel.Enabled;
        }

        public AddSqlServerConnection()
        {
            InitializeComponent();
            InitializeControls();
        }

        public AddSqlServerConnection(MainForm OwnerForm)
            : this()
        {            
            this.Apply = new EventHandler(Apply_Click); 
        }

        private void InitializeControls()
        {
            this.txtPort.Text = "1433";
            this.Load += AddSqlServerConnection_Load;
            SetBtnApplyEnabled(false);
            var serverslist = GetServersRecords("System.Data.Sql");
            combServer.DropDownStyle = ComboBoxStyle.DropDown;
            combServer.Items.Clear();
            combServer.Items.AddRange(serverslist.ToArray());
            if (combServer.Items.Count > 0)
                combServer.SelectedIndex = 0;
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            if(combDataBases.Items .Count <1)
            {
                MessageBox.Show("请选择要连接的数据库名");
                return;
            }

            string connectionString = "";
            connectionString += "Server=" + combServer.Text.ToString();
            if (txtPort.Text != "默认")
            {
                connectionString += "," + txtPort.Text;
            }
            if (!string.IsNullOrEmpty(txtInstance.Text))
            {
                connectionString += "\\" + txtInstance.Text;
            }
            connectionString += ";";
            if (!string.IsNullOrEmpty(combDataBases.Text.ToString()))
            {
                connectionString += "Initial Catalog=" + combDataBases.Text.ToString() + ";";
            }
            else
            {
                connectionString += "Initial Catalog=" + "master;";
            }
            if (!chkLoginStyle.Checked)
            {
                connectionString += "User ID=" + txtUser_ID.Text+";";
                connectionString += "Pwd=" + txtPassword.Text + ";";
            }
            else
            {
                connectionString += "Integrated Security=SSPI;";
            }

            ProviderName = "System.Data.SQL";
            ConnectionString = connectionString.Clone().ToString();
            AllObjectLoad = !chkEfficient.Checked;

        }
       
        private void tester_Click(object sender, EventArgs e)
        {
            combDataBases.Items.Clear();

            try
            {
                DataBaseServer dbserver = new DataBaseServer();
                dbserver.Server = combServer.Text;
                string connectionString = "";
                connectionString += "Server=" + combServer.Text.ToString();
                if (txtPort.Text != "默认")
                {
                    connectionString += "," + txtPort.Text;
                }
                if (!string.IsNullOrEmpty(txtInstance.Text))
                {
                    connectionString += "\\" + txtInstance.Text;
                }
                connectionString += ";";
                if (!string.IsNullOrEmpty(combDataBases.Text))
                {
                    connectionString += "Initial Catalog=" + combDataBases.Text + ";";
                }
                else
                {
                    connectionString += "Initial Catalog=" + "master;";
                }
                if (!chkLoginStyle.Checked)
                {
                    connectionString += "User ID=" + txtUser_ID.Text+";";
                    connectionString += "Pwd=" + txtPassword.Text + ";";
                }
                else
                {
                    connectionString += "Integrated Security=SSPI;";
                }

                ProviderName = "System.Data.SQL";
                ConnectionString = connectionString.Clone().ToString();
                AllObjectLoad = chkEfficient.Checked;

                dbserver.ConnectionString = ConnectionString;
                dbserver.ProviderName = ProviderName;

                using (var db = DevelopAssistant.Service.Utility.GetAdohelper(dbserver))
                {
                    string SqlText = "select name from sysdatabases order by name";
                    DataTable dt = db.QueryDataSet(System.Data.CommandType.Text, SqlText, null).Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        string databaseName = dr["name"] + "";
                        combDataBases.Items.Add(databaseName.Trim());
                    }

                    if (combDataBases.Items.Count > 0)
                        combDataBases.SelectedIndex = 0;

                }

                SetBtnApplyEnabled(true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                SetBtnApplyEnabled(false);
            }           

        }

        private void chkInstance_Click(object sender, EventArgs e)
        {
            if (chkInstance.Checked)
            {
                txtInstance.Enabled = true;
                txtInstance.BackColor = System.Drawing.Color.White;
            }
            else
            {
                txtInstance.Enabled = false;
                txtInstance.BackColor = System.Drawing .SystemColors.Control;
            }
        }

        private void chkLoginStyle_Click(object sender, EventArgs e)
        {
            if (chkLoginStyle.Checked)
            {
                gboxLoginPanel.Enabled = false;
            }
            else
            {
                gboxLoginPanel.Enabled = true;
            }
            txtUser_ID.Enabled = gboxLoginPanel.Enabled;
            txtPassword.Enabled = gboxLoginPanel.Enabled;
        }

        protected override void OnThemeChanged(ThemeEventArgs e)
        {           
            base.OnThemeChanged(e);
            string themeName = e.themeName;

            Color linkColor = Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color disableColor = SystemColors.Control;
            switch(themeName)
            {
                case "Default":
                    linkColor = Color.Blue;
                    textBackColor = SystemColors.Window;
                    disableColor = SystemColors.Control;
                    break;
                case "Black":
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            txtInstance.XForeColor = foreColor;
            txtInstance.XBackColor = textBackColor;
            txtInstance.XDisableColor = disableColor;
            txtPort.XForeColor = foreColor;
            txtPort.XBackColor = textBackColor;
            txtUser_ID.XForeColor = foreColor;
            txtUser_ID.XBackColor = textBackColor;
            txtUser_ID.XDisableColor = disableColor;
            txtPassword.XForeColor = foreColor;
            txtPassword.XBackColor = textBackColor;
            txtPassword.XDisableColor = disableColor;
            combDataBases.ForeColor = foreColor;
            combDataBases.BackColor = textBackColor;
            combServer.ForeColor = foreColor;
            combServer.BackColor = textBackColor;
            tester.ForeColor = linkColor;

        }

    }
}

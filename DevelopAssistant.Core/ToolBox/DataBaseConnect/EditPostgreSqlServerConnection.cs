﻿using DevelopAssistant.Core.ToolBar;
using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class EditPostgreSqlServerConnection : EditDataBaseConnection
    {

        public EditPostgreSqlServerConnection()
        {
            InitializeComponent();
            InitializeControls();
        }

        public EditPostgreSqlServerConnection(DataBaseServer dbserver, MenuBar menubar) : this()
        {
            SetBtnApplyEnabled(false);

            this.ConnectionId = dbserver.ID;
            this.dbserverName = dbserver.Name;
            this.txtConnectionName.Text = dbserver.Name;
            this.txtUserID.Text = dbserver.UserID;
            this.txtPassword.Text = dbserver.Password;
            this.txtPort.Text = dbserver.Port;
            this.txtDataBaseName.Text = dbserver.DataBaseName;
        }

        private void InitializeControls()
        {            
            List<string> ServersRecords = GetServersRecords("System.Data.PostgreSql");
            combServers.Items.AddRange(ServersRecords.ToArray());
            if (combServers.Items.Count > 0)
                combServers.SelectedIndex = 0;
            this.Apply = new EventHandler(Apply_Click);
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            string _connectionString = string.Empty;
            string serverText = combServers.Text;
            if (string.IsNullOrEmpty(serverText))
            {
                if (combServers.SelectedItem != null)
                    serverText = combServers.SelectedItem.ToString();
            }
            _connectionString += "Server=" + serverText + ";";
            _connectionString += "Port=" + txtPort.Text + ";";
            _connectionString += "User Id=" + txtUserID.Text + ";";
            _connectionString += "Password=" + txtPassword.Text + ";";
            _connectionString += "Database=" + txtDataBaseName.Text + ";";
            _connectionString += "CommandTimeout=0;";
            //_connectionString += "ConnectionLifeTime=0;";
            ConnectionName = txtConnectionName.Text;
            ProviderName = "System.Data.PostgreSql";
            ConnectionString = _connectionString;
            ProviderName = "System.Data.PostgreSql";
            AllObjectLoad = !chkEfficient.Checked;
        }

        private void tester_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConnectionName.Text))
            {
                MessageBox.Show("连接名称不能为空");
                return;
            }

            if (string.IsNullOrEmpty(txtDataBaseName.Text))
            {
                MessageBox.Show("数据库名称不能为空");
                return;
            }

            if (string.IsNullOrEmpty(txtUserID.Text))
            {
                MessageBox.Show("用户名不能为空");
                return;
            }

            string _connectionString = string.Empty;
            _connectionString += "Server=" + combServers.Text + ";";
            _connectionString += "Port=" + txtPort.Text + ";";
            _connectionString += "User Id=" + txtUserID.Text + ";";
            _connectionString += "Password=" + txtPassword.Text + ";";
            _connectionString += "Database=" + txtDataBaseName.Text + ";";
            _connectionString += "CommandTimeout=0;";
            //_connectionString += "ConnectionLifeTime=0;";
            ConnectionName = txtConnectionName.Text;
            ProviderName = "System.Data.PostgreSql";
            ConnectionString = _connectionString;

            try
            {
                DataBaseServer dbserver = new DataBaseServer(ConnectionString, ProviderName);
                using (var db = Utility.GetAdohelper(dbserver))
                {
                    DataTable dt = db.GetDataBaseObject();
                    SetBtnApplyEnabled(true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        protected override void OnThemeChanged(ThemeEventArgs e)
        {
            base.OnThemeChanged(e);

            string themeName = e.themeName;

            Color linkColor = Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color disableColor = SystemColors.Control;
            switch (themeName)
            {
                case "Default":
                    linkColor = Color.Blue;
                    textBackColor = SystemColors.Window;
                    disableColor = SystemColors.Control;
                    break;
                case "Black":
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            txtConnectionName.XForeColor = foreColor;
            txtConnectionName.XBackColor = textBackColor;
            txtConnectionName.XDisableColor = disableColor;
            txtPort.XForeColor = foreColor;
            txtPort.XBackColor = textBackColor;
            txtDataBaseName.XForeColor = foreColor;
            txtDataBaseName.XBackColor = textBackColor;
            txtDataBaseName.XDisableColor = disableColor;
            txtUserID.XForeColor = foreColor;
            txtUserID.XBackColor = textBackColor;
            txtUserID.XDisableColor = disableColor;
            txtPassword.XForeColor = foreColor;
            txtPassword.XBackColor = textBackColor;
            txtPassword.XDisableColor = disableColor;
            combServers.ForeColor = foreColor;
            combServers.BackColor = backColor;
            tester.ForeColor = linkColor;


        }

        private void AddPostgreSqlConnection_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new ThemeEventArgs());
            txtConnectionName.Enabled = false;
            txtDataBaseName.Enabled = false;
        }

        private void label7_Click(object sender, EventArgs e)
        {
            if (txtPassword.PasswordChar == '*')
            {
                txtPassword.PasswordChar = txtUserID.PasswordChar;
                label7.Image = DevelopAssistant.Core.Properties.Resources.closeeye;
                //txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                label7.Image = DevelopAssistant.Core.Properties.Resources.openeye;
                //txtPassword.UseSystemPasswordChar = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class AddMySqlServerConnection : AddDataBaseConnection
    {
        public AddMySqlServerConnection()
        {
            InitializeComponent();
        }      

        public AddMySqlServerConnection(MainForm mainform)
            : this()
        {
            this.Apply = new EventHandler(Apply_Click);
            SetBtnApplyEnabled(false);
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            NORM.Common.ConnectionString connection = new NORM.Common.ConnectionString(NORM.DataBase.DataBaseTypes.MySqlDataBase);
            connection.Server = txtServer.Text.Trim();
            connection.Port = txtPort.Text.Trim();
            connection.DataBaseName = txtDataBaseName.Text.Trim();
            connection.UserId = txtUser.Text.Trim();
            connection.Password = txtPassword.Text.Trim();
            connection.Encoding = "utf8";

            using (var db = NORM.DataBase.DataBaseFactory.Create(connection.connectionString, NORM.DataBase.DataBaseTypes.MySqlDataBase))
            {
                string Message = string.Empty;
                if (!db.TestConnect(out Message))
                {
                    MessageBox.Show(Message);
                    return;
                }
            }

            ConnectionString = connection.connectionString;
            ProviderName = "System.Data.MySql";
            ConnectionName = txtName.Text;
            AllObjectLoad = !checkBox1.Checked;

        }

        private void lblTest_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("连接名称不能为空");
                return;
            }

            if (string.IsNullOrEmpty(txtServer.Text))
            {
                MessageBox.Show("服务器地址不能为空");
                return;
            }

            if (string.IsNullOrEmpty(txtDataBaseName.Text))
            {
                MessageBox.Show("数据名称不能为空");
                return;
            }

            NORM.Common.ConnectionString connection = new NORM.Common.ConnectionString(NORM.DataBase.DataBaseTypes.MySqlDataBase);
            connection.Server = txtServer.Text.Trim();
            connection.Port = txtPort.Text.Trim();
            connection.DataBaseName = txtDataBaseName.Text.Trim();
            connection.UserId = txtUser.Text.Trim();
            connection.Password = txtPassword.Text.Trim();
            connection.Encoding = "utf8";

            using (var db = NORM.DataBase.DataBaseFactory.Create(connection.connectionString, NORM.DataBase.DataBaseTypes.MySqlDataBase))
            {
                string Message = string.Empty;
                if (!db.TestConnect(out Message))
                {
                    MessageBox.Show(Message);
                    return;
                }
                else
                {
                    SetBtnApplyEnabled(true);
                }
            }
        }

        protected override void OnThemeChanged(ThemeEventArgs e)
        {
            base.OnThemeChanged(e);

            string themeName = e.themeName;

            Color linkColor = Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color disableColor = SystemColors.Control;
            switch (themeName)
            {
                case "Default":
                    linkColor = Color.Blue;
                    textBackColor = SystemColors.Window;
                    disableColor = SystemColors.Control;
                    break;
                case "Black":
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            txtDataBaseName.XForeColor = foreColor;
            txtDataBaseName.XBackColor = textBackColor;           
            txtServer.XForeColor = foreColor;
            txtServer.XBackColor = textBackColor;            
            txtPort.XForeColor = foreColor;
            txtPort.XBackColor = textBackColor;
            txtUser.XForeColor = foreColor;
            txtUser.XBackColor = textBackColor;
            txtUser.XDisableColor = disableColor; 
            txtName.XForeColor = foreColor;
            txtName.XBackColor = textBackColor;
            txtName.XDisableColor = disableColor;
            txtPassword.XForeColor = foreColor;
            txtPassword.XBackColor = textBackColor;
            txtPassword.XDisableColor = disableColor;
            lblTest.ForeColor = linkColor;

        }

        private void AddMySqlServerConnection_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new ThemeEventArgs());
        }
    }
}

﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class EditMySqlServerConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMySqlServerConnection));
            this.label1 = new System.Windows.Forms.Label();
            this.txtServer = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUser = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPort = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.lblTest = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDataBaseName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "服务器地址：";
            // 
            // txtServer
            // 
            this.txtServer.BackColor = System.Drawing.SystemColors.Window;
            this.txtServer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtServer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtServer.FousedColor = System.Drawing.Color.Orange;
            this.txtServer.Icon = null;
            this.txtServer.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtServer.IsButtonTextBox = false;
            this.txtServer.IsClearTextBox = false;
            this.txtServer.IsPasswordTextBox = false;
            this.txtServer.Location = new System.Drawing.Point(125, 61);
            this.txtServer.MaxLength = 32767;
            this.txtServer.Multiline = false;
            this.txtServer.Name = "txtServer";
            this.txtServer.PasswordChar = '\0';
            this.txtServer.Placeholder = null;
            this.txtServer.ReadOnly = false;
            this.txtServer.Size = new System.Drawing.Size(172, 24);
            this.txtServer.TabIndex = 2;
            this.txtServer.UseSystemPasswordChar = false;
            this.txtServer.XBackColor = System.Drawing.SystemColors.Window;
            this.txtServer.XDisableColor = System.Drawing.Color.Empty;
            this.txtServer.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "用户名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "密码：";
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUser.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtUser.FousedColor = System.Drawing.Color.Orange;
            this.txtUser.Icon = null;
            this.txtUser.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUser.IsButtonTextBox = false;
            this.txtUser.IsClearTextBox = false;
            this.txtUser.IsPasswordTextBox = false;
            this.txtUser.Location = new System.Drawing.Point(124, 177);
            this.txtUser.MaxLength = 32767;
            this.txtUser.Multiline = false;
            this.txtUser.Name = "txtUser";
            this.txtUser.PasswordChar = '\0';
            this.txtUser.Placeholder = null;
            this.txtUser.ReadOnly = false;
            this.txtUser.Size = new System.Drawing.Size(123, 24);
            this.txtUser.TabIndex = 5;
            this.txtUser.UseSystemPasswordChar = false;
            this.txtUser.XBackColor = System.Drawing.SystemColors.Window;
            this.txtUser.XDisableColor = System.Drawing.Color.Empty;
            this.txtUser.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(124, 215);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '\0';
            this.txtPassword.Placeholder = null;
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(123, 24);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.UseSystemPasswordChar = false;
            this.txtPassword.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.XDisableColor = System.Drawing.Color.Empty;
            this.txtPassword.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "端口号：";
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPort.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPort.FousedColor = System.Drawing.Color.Orange;
            this.txtPort.Icon = null;
            this.txtPort.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPort.IsButtonTextBox = false;
            this.txtPort.IsClearTextBox = false;
            this.txtPort.IsPasswordTextBox = false;
            this.txtPort.Location = new System.Drawing.Point(125, 101);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.PasswordChar = '\0';
            this.txtPort.Placeholder = null;
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(100, 24);
            this.txtPort.TabIndex = 8;
            this.txtPort.UseSystemPasswordChar = false;
            this.txtPort.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPort.XDisableColor = System.Drawing.Color.Empty;
            this.txtPort.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // lblTest
            // 
            this.lblTest.AutoSize = true;
            this.lblTest.ForeColor = System.Drawing.Color.Blue;
            this.lblTest.Location = new System.Drawing.Point(230, 261);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(53, 12);
            this.lblTest.TabIndex = 9;
            this.lblTest.Text = "测试连接";
            this.lblTest.Click += new System.EventHandler(this.lblTest_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "连接名称：";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtName.FousedColor = System.Drawing.Color.Orange;
            this.txtName.Icon = null;
            this.txtName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtName.IsButtonTextBox = false;
            this.txtName.IsClearTextBox = false;
            this.txtName.IsPasswordTextBox = false;
            this.txtName.Location = new System.Drawing.Point(125, 22);
            this.txtName.MaxLength = 32767;
            this.txtName.Multiline = false;
            this.txtName.Name = "txtName";
            this.txtName.PasswordChar = '\0';
            this.txtName.Placeholder = null;
            this.txtName.ReadOnly = false;
            this.txtName.Size = new System.Drawing.Size(172, 24);
            this.txtName.TabIndex = 11;
            this.txtName.UseSystemPasswordChar = false;
            this.txtName.XBackColor = System.Drawing.SystemColors.Window;
            this.txtName.XDisableColor = System.Drawing.Color.Empty;
            this.txtName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(124, 259);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 16);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "高效连接模式";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "数据库名称：";
            // 
            // txtDataBaseName
            // 
            this.txtDataBaseName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDataBaseName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDataBaseName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDataBaseName.FousedColor = System.Drawing.Color.Orange;
            this.txtDataBaseName.Icon = null;
            this.txtDataBaseName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtDataBaseName.IsButtonTextBox = false;
            this.txtDataBaseName.IsClearTextBox = false;
            this.txtDataBaseName.IsPasswordTextBox = false;
            this.txtDataBaseName.Location = new System.Drawing.Point(125, 139);
            this.txtDataBaseName.MaxLength = 32767;
            this.txtDataBaseName.Multiline = false;
            this.txtDataBaseName.Name = "txtDataBaseName";
            this.txtDataBaseName.PasswordChar = '\0';
            this.txtDataBaseName.Placeholder = null;
            this.txtDataBaseName.ReadOnly = false;
            this.txtDataBaseName.Size = new System.Drawing.Size(172, 24);
            this.txtDataBaseName.TabIndex = 14;
            this.txtDataBaseName.UseSystemPasswordChar = false;
            this.txtDataBaseName.XBackColor = System.Drawing.SystemColors.Window;
            this.txtDataBaseName.XDisableColor = System.Drawing.Color.Empty;
            this.txtDataBaseName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "默认：3306";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtDataBaseName);
            this.panel2.Controls.Add(this.txtServer);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Controls.Add(this.txtUser);
            this.panel2.Controls.Add(this.txtPassword);
            this.panel2.Controls.Add(this.lblTest);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(373, 295);
            this.panel2.TabIndex = 16;
            // 
            // AddMySqlServerConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 395);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddMySqlServerConnection";
            this.Text = "MySql数据库连接";
            this.Load += new System.EventHandler(this.AddMySqlServerConnection_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUser;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPort; 
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.Label label6;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtName;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtDataBaseName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
    }
}
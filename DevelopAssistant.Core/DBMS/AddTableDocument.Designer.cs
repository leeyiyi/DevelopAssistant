﻿namespace DevelopAssistant.Core.DBMS
{
    partial class AddTableDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddTableDocument));
            this.dgv_design = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.ColumnOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDataLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPrimaryKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIsnull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnIdentity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDescrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIcon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonPrimaryKey = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddColumn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeleteColumn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonToUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonToDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReturn = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加列ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除列ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.上移ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.下移ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设为主键ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_design)).BeginInit();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_design
            // 
            this.dgv_design.AllowUserToAddRows = false;
            this.dgv_design.AllowUserToOrderColumns = true;
            this.dgv_design.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_design.BackgroundColor = System.Drawing.Color.White;
            this.dgv_design.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_design.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_design.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_design.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnOrder,
            this.ColumnName,
            this.ColumnDataType,
            this.ColumnDataLength,
            this.ColumnPrimaryKey,
            this.ColumnIsnull,
            this.ColumnIdentity,
            this.ColumnDescrib,
            this.ColumnIcon});
            this.dgv_design.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dgv_design.EnableHeadersVisualStyles = false;
            this.dgv_design.Location = new System.Drawing.Point(7, 69);
            this.dgv_design.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_design.Name = "dgv_design";
            this.dgv_design.RowHeadersWidth = 42;
            this.dgv_design.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_design.RowTemplate.Height = 23;
            this.dgv_design.ShowRowNumber = false;
            this.dgv_design.Size = new System.Drawing.Size(607, 320);
            this.dgv_design.TabIndex = 12;
            this.dgv_design.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dgv_design.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_design_CellMouseClick);
            this.dgv_design.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // ColumnOrder
            // 
            this.ColumnOrder.DataPropertyName = "序号";
            this.ColumnOrder.HeaderText = "序号";
            this.ColumnOrder.Name = "ColumnOrder";
            this.ColumnOrder.ReadOnly = true;
            this.ColumnOrder.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnOrder.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnOrder.Visible = false;
            this.ColumnOrder.Width = 60;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "名称";
            this.ColumnName.HeaderText = "名称";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDataType
            // 
            this.ColumnDataType.DataPropertyName = "数据类型";
            this.ColumnDataType.HeaderText = "数据类型";
            this.ColumnDataType.Name = "ColumnDataType";
            this.ColumnDataType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDataLength
            // 
            this.ColumnDataLength.DataPropertyName = "长度";
            this.ColumnDataLength.HeaderText = "长度";
            this.ColumnDataLength.Name = "ColumnDataLength";
            this.ColumnDataLength.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDataLength.Width = 80;
            // 
            // ColumnPrimaryKey
            // 
            this.ColumnPrimaryKey.DataPropertyName = "主键标识";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnPrimaryKey.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnPrimaryKey.HeaderText = "主键标识";
            this.ColumnPrimaryKey.Name = "ColumnPrimaryKey";
            this.ColumnPrimaryKey.ReadOnly = true;
            this.ColumnPrimaryKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnIsnull
            // 
            this.ColumnIsnull.DataPropertyName = "允许为空";
            this.ColumnIsnull.HeaderText = "允许为空";
            this.ColumnIsnull.Name = "ColumnIsnull";
            this.ColumnIsnull.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnIsnull.Width = 84;
            // 
            // ColumnIdentity
            // 
            this.ColumnIdentity.DataPropertyName = "自增标识";
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnIdentity.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnIdentity.HeaderText = "自增标识";
            this.ColumnIdentity.Name = "ColumnIdentity";
            this.ColumnIdentity.ReadOnly = true;
            this.ColumnIdentity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDescrib
            // 
            this.ColumnDescrib.DataPropertyName = "描述";
            this.ColumnDescrib.HeaderText = "描述";
            this.ColumnDescrib.Name = "ColumnDescrib";
            this.ColumnDescrib.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDescrib.Width = 120;
            // 
            // ColumnIcon
            // 
            this.ColumnIcon.DataPropertyName = "全标识";
            this.ColumnIcon.HeaderText = "全标识";
            this.ColumnIcon.Name = "ColumnIcon";
            this.ColumnIcon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnIcon.Visible = false;
            // 
            // txtTableName
            // 
            this.txtTableName.Location = new System.Drawing.Point(62, 13);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(169, 21);
            this.txtTableName.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "表名：";
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 397);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 52);
            this.panel1.TabIndex = 9;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(281, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "提示：在表有超过 1000000 (百万) 条数据时请慎用";
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(436, 8);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 34);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "保存";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(528, 8);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 34);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPrimaryKey,
            this.toolStripButtonAddColumn,
            this.toolStripButtonDeleteColumn,
            this.toolStripSeparator1,
            this.toolStripButtonToUp,
            this.toolStripButtonToDown,
            this.toolStripButtonSave,
            this.toolStripButtonReturn});
            this.toolStrip1.Location = new System.Drawing.Point(5, 41);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 2, 1, 2);
            this.toolStrip1.Size = new System.Drawing.Size(609, 25);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonPrimaryKey
            // 
            this.toolStripButtonPrimaryKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPrimaryKey.Image = global::DevelopAssistant.Core.Properties.Resources.pk;
            this.toolStripButtonPrimaryKey.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrimaryKey.Name = "toolStripButtonPrimaryKey";
            this.toolStripButtonPrimaryKey.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonPrimaryKey.Text = "设为主键";
            this.toolStripButtonPrimaryKey.Click += new System.EventHandler(this.设为主键ToolStripMenuItem_Click);
            // 
            // toolStripButtonAddColumn
            // 
            this.toolStripButtonAddColumn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddColumn.Image = global::DevelopAssistant.Core.Properties.Resources.add;
            this.toolStripButtonAddColumn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddColumn.Name = "toolStripButtonAddColumn";
            this.toolStripButtonAddColumn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonAddColumn.Text = "添加";
            this.toolStripButtonAddColumn.Click += new System.EventHandler(this.添加列ToolStripMenuItem_Click);
            // 
            // toolStripButtonDeleteColumn
            // 
            this.toolStripButtonDeleteColumn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDeleteColumn.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolStripButtonDeleteColumn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteColumn.Name = "toolStripButtonDeleteColumn";
            this.toolStripButtonDeleteColumn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonDeleteColumn.Text = "删除";
            this.toolStripButtonDeleteColumn.Click += new System.EventHandler(this.删除列ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 21);
            // 
            // toolStripButtonToUp
            // 
            this.toolStripButtonToUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToUp.Image = global::DevelopAssistant.Core.Properties.Resources.toup_sml;
            this.toolStripButtonToUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToUp.Name = "toolStripButtonToUp";
            this.toolStripButtonToUp.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonToUp.Text = "上移";
            this.toolStripButtonToUp.Click += new System.EventHandler(this.上移ToolStripMenuItem_Click);
            // 
            // toolStripButtonToDown
            // 
            this.toolStripButtonToDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToDown.Image = global::DevelopAssistant.Core.Properties.Resources.todown_sml;
            this.toolStripButtonToDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToDown.Name = "toolStripButtonToDown";
            this.toolStripButtonToDown.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonToDown.Text = "下移";
            this.toolStripButtonToDown.Click += new System.EventHandler(this.下移ToolStripMenuItem_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = global::DevelopAssistant.Core.Properties.Resources.save;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonSave.Text = "保存";
            // 
            // toolStripButtonReturn
            // 
            this.toolStripButtonReturn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonReturn.Image = global::DevelopAssistant.Core.Properties.Resources._return;
            this.toolStripButtonReturn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReturn.Name = "toolStripButtonReturn";
            this.toolStripButtonReturn.Size = new System.Drawing.Size(23, 18);
            this.toolStripButtonReturn.Text = "撤销";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加列ToolStripMenuItem,
            this.删除列ToolStripMenuItem,
            this.上移ToolStripMenuItem,
            this.下移ToolStripMenuItem,
            this.设为主键ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 114);
            // 
            // 添加列ToolStripMenuItem
            // 
            this.添加列ToolStripMenuItem.Name = "添加列ToolStripMenuItem";
            this.添加列ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加列ToolStripMenuItem.Text = "添加列";
            // 
            // 删除列ToolStripMenuItem
            // 
            this.删除列ToolStripMenuItem.Name = "删除列ToolStripMenuItem";
            this.删除列ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除列ToolStripMenuItem.Text = "删除列";
            // 
            // 上移ToolStripMenuItem
            // 
            this.上移ToolStripMenuItem.Name = "上移ToolStripMenuItem";
            this.上移ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.上移ToolStripMenuItem.Text = "上移";
            // 
            // 下移ToolStripMenuItem
            // 
            this.下移ToolStripMenuItem.Name = "下移ToolStripMenuItem";
            this.下移ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.下移ToolStripMenuItem.Text = "下移";
            // 
            // 设为主键ToolStripMenuItem
            // 
            this.设为主键ToolStripMenuItem.Name = "设为主键ToolStripMenuItem";
            this.设为主键ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.设为主键ToolStripMenuItem.Text = "设为主键";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pk.gif");
            // 
            // AddTableDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 449);
            this.Controls.Add(this.dgv_design);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddTableDocument";
            this.Text = "AddTableDocument";
            this.Load += new System.EventHandler(this.AddTableDocument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_design)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NDataGridView dgv_design;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDataLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPrimaryKey;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnIsnull;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdentity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIcon;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrimaryKey;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddColumn;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteColumn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonToUp;
        private System.Windows.Forms.ToolStripButton toolStripButtonToDown;
        private System.Windows.Forms.ToolStripButton toolStripButtonSave;
        private System.Windows.Forms.ToolStripButton toolStripButtonReturn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 添加列ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除列ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 上移ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 下移ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设为主键ToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
    }
}
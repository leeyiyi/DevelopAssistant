﻿namespace DevelopAssistant.Core.DBMS
{
    partial class DeleteTableDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteTableDocument));
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.txtTableName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.listBox1 = new ICSharpCode.WinFormsUI.Controls.NListBox();
            this.NToolBar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Controls.Add(this.btnCancel);
            this.NToolBar.Controls.Add(this.btnApply);
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(0, 349);
            this.NToolBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(474, 61);
            this.NToolBar.TabIndex = 12;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancel.FouseColor = System.Drawing.Color.White;
            this.btnCancel.Foused = false;
            this.btnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancel.Icon = null;
            this.btnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(361, 19);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Radius = 0;
            this.btnCancel.Size = new System.Drawing.Size(87, 29);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消";
            this.btnCancel.UnableIcon = null;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(256, 19);
            this.btnApply.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(87, 29);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "确定";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.txtTableName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(450, 80);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // txtTableName
            // 
            this.txtTableName.BackColor = System.Drawing.Color.White;
            this.txtTableName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTableName.Enabled = true;
            this.txtTableName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtTableName.FousedColor = System.Drawing.Color.Orange;
            this.txtTableName.Icon = null;
            this.txtTableName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtTableName.IsButtonTextBox = false;
            this.txtTableName.IsClearTextBox = false;
            this.txtTableName.IsPasswordTextBox = false;
            this.txtTableName.Location = new System.Drawing.Point(71, 31);
            this.txtTableName.MaxLength = 32767;
            this.txtTableName.Multiline = false;
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.PasswordChar = '\0';
            this.txtTableName.Placeholder = null;
            this.txtTableName.ReadOnly = false;
            this.txtTableName.Size = new System.Drawing.Size(218, 24);
            this.txtTableName.TabIndex = 1;
            this.txtTableName.UseSystemPasswordChar = false;
            this.txtTableName.XBackColor = System.Drawing.Color.White;
            this.txtTableName.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "名称：";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox2.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Location = new System.Drawing.Point(12, 99);
            this.groupBox2.MarginWidth = 0;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(20);
            this.groupBox2.Size = new System.Drawing.Size(450, 236);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Title = "";
            this.groupBox2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // listBox1
            // 
            this.listBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.listBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.listBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.Normal;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(20, 20);
            this.listBox1.MarginWidth = 0;
            this.listBox1.MultiColumn = false;
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectedIndex = 0;
            this.listBox1.SelectedItem = null;
            this.listBox1.Size = new System.Drawing.Size(410, 196);
            this.listBox1.TabIndex = 0;
            this.listBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // DeleteTableDocument
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(474, 410);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NToolBar);
            this.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeleteTableDocument";
            this.Text = "DeleteTableDocument";
            this.Load += new System.EventHandler(this.DeleteTableDocument_Load);
            this.NToolBar.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancel;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtTableName;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox2;
        private ICSharpCode.WinFormsUI.Controls.NListBox listBox1;
    }
}
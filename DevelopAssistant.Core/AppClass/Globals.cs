﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing; 
using System.Text; 

namespace DevelopAssistant.Core
{
    public delegate object DelegateDoSomethingComplete(params object[] Parameters); 
    public delegate object DelegateDisplayMessage(string Message, string ToolTipText = "");

    public class G_App
    {
        public class SessionInfo
        {
            public static int UserID { set; get; }
            public static string UserName { set; get; }
            public static string Name { set; get; }
            public static string RoleID { set; get; }
            public static string RoleName { set; get; }
            public static int DepartmentID { set; get; }
            public static string DepartmentName { set; get; }
        }
    }

}

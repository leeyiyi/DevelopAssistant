﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace DevelopAssistant.Common
{
    using System.Text.RegularExpressions; 

    public partial class StringPlus : ISerializable, IDisposable
    {
        private StringBuilder sb = new StringBuilder();

        public StringPlus()
        {
        }

        public StringPlus(string value)
            : this()
        {
            sb.Append(value);
        }

        [SecurityPermission(SecurityAction.LinkDemand,Flags = SecurityPermissionFlag.SerializationFormatter)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            info.AddValue("Value", Value);
            info.AddValue("StringValue", Value);

        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            //sb.Clear(); 
            sb = new StringBuilder();
        }

        public StringPlus AppendLine()
        {
            sb.Append(System.Environment.NewLine);
            return this; 
        }

        public StringPlus Append(string value)
        {
            sb.Append(value);
            return this;           
        }

        public StringPlus AppendLine(string value)
        {
            sb.Append(value);
            sb.Append(System.Environment.NewLine);
            return this; 
        }

        public StringPlus Replace(string oldValue, string newValue)
        {
            sb.Replace(oldValue, newValue);
            return this;
        }

        /// <summary>
        /// 字符串值
        /// </summary>
        public string Value
        {
            get { return sb.ToString(); }
        }

        /// <summary>
        /// 字符串值
        /// </summary>
        public string StringValue
        {
            get { return sb.ToString(); }
        }

        public string d
        {
            get { return sb.ToString(); }
        }
       
    }

    public partial class StringPlus
    {
        public bool StringCompare(string value1, string value2)
        {
            bool val = false;
            if (value2.Equals(value1))
            {
                val = true;
            }
            return val;
        }

        public string ToShortPinyin(string value)
        {
            string _tempString = string.Empty;

            foreach (char c in value)
            {
                if (((int)c) >= 33 && ((int)c) <= 126)
                {
                    _tempString += c.ToString(); //字母和符号原样保留
                }
                else
                {
                    _tempString += ConvertChar(c.ToString()); //累加拼音声母
                }
            }
            return _tempString;
        }

        public string ToToChineseMoney(double money)
        {
            string s = money.ToString("#L#E#D#C#K#E#D#C#J#E#D#C#I#E#D#C#H#E#D#C#G#E#D#C#F#E#D#C#.0B0A");
            string d = System.Text.RegularExpressions.Regex.Replace(s, @"((?<=-|^)[^1-9]*)|((?'z'0)[0A-E]*((?=[1-9])|(?'-z'(?=[F-L\.]|$))))|((?'b'[F-L])(?'z'0)[0A-L]*((?=[1-9])|(?'-z'(?=[\.]|$))))", "${b}${z}");
            string str = System.Text.RegularExpressions.Regex.Replace(d, ".", delegate(Match m)
            {
                return
                    "负元空零壹贰叁肆伍陆柒捌玖空空空空空空空分角拾佰仟萬億兆京垓秭穰"
                    [m.Value[0] - '-'].ToString();
            });
            return str = string.IsNullOrEmpty(str) ? "零元" : str;
        }

        /// <summary>
        /// 获取中文指定长度字符串
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <param name="isDots"></param>
        /// <returns></returns>
        public string CutShortStringCn(string text, int length, bool isDots)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            byte[] textBytes = Encoding.Default.GetBytes(text);

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < text.Length; i++)
            {
                byte[] TempBytes = Encoding.Default.GetBytes(result.ToString());

                if (TempBytes.Length < length * 2)
                {
                    result.Append(text.Substring(i, 1));
                }
                else
                {
                    if (isDots)
                        result.Append("...");
                    break;
                }

            }
            return result.ToString();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <param name="isDots"></param>
        /// <returns></returns>
        public string CutShortStringEn(string text, int length, bool isDots)
        {             
            if (!string.IsNullOrEmpty(text))
            {
                if (text.Length >= length)
                {
                    text = text.Substring(0, length);
                    if (isDots)
                        text += "...";
                }
            }
            return text;
        }

        /// <summary>
        /// 下划线转驼峰 
        /// </summary>
        /// <param name="input">输入文本</param>
        /// <param name="smallCamel">小驼峰</param>
        /// <returns></returns>
        public string Underline2Camel(string input, bool smallCamel)
        {
            string result = input;
            if (string.IsNullOrEmpty(input))
                return result;

            int word_index = 0;
            StringBuilder sb = new StringBuilder();
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("([A-Za-z\\d]+)(_)?");
            System.Text.RegularExpressions.Match match = regex.Match(input);
            while (match.Success)
            {
                string value = match.Value;
                char[] charts = value.Trim('_').ToLower().ToCharArray();
                if ((!smallCamel || word_index > 0) && charts.Length > 0)
                {
                    charts[0] = Char.ToUpper(charts[0]);
                }
                sb.Append(new String(charts));
                match = match.NextMatch();
                word_index++;
            }

            result = sb.ToString();

            return result;

        }

        /// <summary>
        /// 驼峰转下划线
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns></returns>
        public string Camel2Underline(string input)
        {
            string result = input;
            if (string.IsNullOrEmpty(input))
                return result;

            char[] charts = input.ToCharArray();
            if (charts.Length > 0)
            {
                charts[0] = Char.ToUpper(charts[0]);
            }
            input = new String(charts);

            StringBuilder sb = new StringBuilder();
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[A-Z]([a-z\\d]+)?");
            System.Text.RegularExpressions.Match match = regex.Match(input);

            int word_index = 0;
            while (match.Success)
            {
                if (word_index > 0)
                {
                    sb.Append("_");
                }
                string value = match.Value;
                sb.Append(value.ToLower());
                match = match.NextMatch();
                word_index++;
            }

            result = sb.ToString();

            return result;

        }

        /// <summary>
        /// 取单个字符的拼音声母
        /// </summary>
        /// <param name="Characters">单个汉字</param>
        /// <returns></returns>
        private string ConvertChar(string Characters)
        {
            byte[] array = new byte[2];
            array = System.Text.Encoding.Default.GetBytes(Characters);
            int i = (short)(array[0] - '\0') * 256 + ((short)(array[1] - '\0'));
            if (i < 0xB0A1) return "*";
            if (i < 0xB0C5) return "A";
            if (i < 0xB2C1) return "B";
            if (i < 0xB4EE) return "C";
            if (i < 0xB6EA) return "D";
            if (i < 0xB7A2) return "E";
            if (i < 0xB8C1) return "F";
            if (i < 0xB9FE) return "G";
            if (i < 0xBBF7) return "H";
            if (i < 0xBFA6) return "J";
            if (i < 0xC0AC) return "K";
            if (i < 0xC2E8) return "L";
            if (i < 0xC4C3) return "M";
            if (i < 0xC5B6) return "N";
            if (i < 0xC5BE) return "O";
            if (i < 0xC6DA) return "P";
            if (i < 0xC8BB) return "Q";
            if (i < 0xC8F6) return "R";
            if (i < 0xCBFA) return "S";
            if (i < 0xCDDA) return "T";
            if (i < 0xCEF4) return "W";
            if (i < 0xD1B9) return "X";
            if (i < 0xD4D1) return "Y";
            if (i < 0xD7FA) return "Z";
            return "*";
        }
    }

    public static class Extension
    {
        //public static string format(this string s, string match)
        //{
        //    string rvl = string.Empty;
        //    return rvl;
        //}
    } 
}

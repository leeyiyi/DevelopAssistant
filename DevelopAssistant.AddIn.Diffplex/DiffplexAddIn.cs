﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;

namespace DevelopAssistant.AddIn.Diffplex
{
    using DevelopAssistant.AddIn.Diffplex.Properties;
    using ICSharpCode.WinFormsUI.Docking;

    public class DiffplexAddIn : DockContentAddIn 
    {
        public DiffplexAddIn()
        {            
            this.IdentityID = "ee912234-a189-41c0-97c6-c8c7226e067d";
            this.Name = "文件对比1.0";
            this.Text = "文件内容对比";
            this.Tooltip = "对比文件内容找出不同之处";            
            this.Icon = Resources.plus_shield;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((Form)Parameter[0], Parameter[3].ToString(), this){ 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}

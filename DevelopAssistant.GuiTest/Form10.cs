﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "*.png|*.png";
            if (openDialog.ShowDialog(this).Equals(DialogResult.OK))
            {
                string filePath = openDialog.FileName;
                Image img = Image.FromFile(filePath);
                pictureBox1.Image= GetSomthingImage((Bitmap)img);
            }
            //this.pictureBox1.Image = doSomthing();
        }

        private Bitmap GetSomthingImage(System.Drawing.Bitmap image)
        {
            Bitmap IconImage = new Bitmap(image.Width, image.Height);

            using (ImageAttributes imageAttributes = new ImageAttributes())
            {
                string[] colors = this.textBox1.Text.Split(',');

                ColorMap[] colorMap = new ColorMap[2];
                colorMap[0] = new ColorMap();
                colorMap[0].OldColor = Color.FromArgb(0, 0, 0);
                colorMap[0].NewColor = Color.FromArgb(Int32.Parse(colors[0]), Int32.Parse(colors[1]), Int32.Parse(colors[2]));
                colorMap[1] = new ColorMap();
                colorMap[1].OldColor = image.GetPixel(0, 0);
                colorMap[1].NewColor = Color.Transparent;

                imageAttributes.SetRemapTable(colorMap);

                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(IconImage);

                g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height),
                   0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);

                g.Dispose();
            }

            IconImage.Save(@"C:\Users\Administrator\Desktop\新建文件夹\out\demo.png");

            return IconImage;
        }
    }
}

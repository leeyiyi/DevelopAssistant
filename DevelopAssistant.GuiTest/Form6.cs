﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();             
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            //2
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            this.tabControl1.Radius = 0;
            //this.tabControl1.ShowClose = true;
            //this.tabControl1.TabCaptionLm = 0;
            //this.tabControl1.ShowWaitMessage = true;
            this.tabControl1.BorderColor = SystemColors.ControlDark;
            //this.tabControl1.ArrowColor = Color.White;
            this.tabControl1.BaseColor = SystemColors.Control;
            //this.tabControl1.BaseColor = Color.White;
            this.tabControl1.BackColor = SystemColors.Control;
            this.tabControl1.SelectedColor = SystemColors.Control;

            this.tabControl1.BaseTabColor = Color.White; //SystemColors.Control;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TabPage tabPage = new System.Windows.Forms.TabPage("newTab");
            tabControl1.TabPages.Add(tabPage);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.Alignment = TabAlignment.Left;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.Alignment = TabAlignment.Right;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tabControl1.Alignment = TabAlignment.Bottom;
        }
    }
}

﻿namespace DevelopAssistant.GuiTest
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.treeViewAdv1 = new ICSharpCode.WinFormsUI.Controls.NTreeView();
            this.nTreeView1 = new ICSharpCode.WinFormsUI.Controls.NTreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeViewAdv1
            // 
            this.treeViewAdv1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewAdv1.BackColor = System.Drawing.SystemColors.Window;
            this.treeViewAdv1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewAdv1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeViewAdv1.DragDropMarkColor = System.Drawing.Color.Black;
            this.treeViewAdv1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.treeViewAdv1.ImageList = null;
            this.treeViewAdv1.LineColor = System.Drawing.SystemColors.ControlDark;
            this.treeViewAdv1.Location = new System.Drawing.Point(12, 37);
            this.treeViewAdv1.Model = null;
            this.treeViewAdv1.Name = "treeViewAdv1";
            this.treeViewAdv1.NBorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.treeViewAdv1.SelectedNode = null;
            this.treeViewAdv1.Size = new System.Drawing.Size(189, 444);
            this.treeViewAdv1.TabIndex = 0;
            this.treeViewAdv1.Text = "treeViewAdv1";
            // 
            // nTreeView1
            // 
            this.nTreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.nTreeView1.BackColor = System.Drawing.SystemColors.Window;
            this.nTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nTreeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.nTreeView1.DragDropMarkColor = System.Drawing.Color.Black;
            this.nTreeView1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.nTreeView1.ImageList = this.imageList1;
            this.nTreeView1.LineColor = System.Drawing.SystemColors.ControlDark;
            this.nTreeView1.Location = new System.Drawing.Point(224, 37);
            this.nTreeView1.Model = null;
            this.nTreeView1.Name = "nTreeView1";
            this.nTreeView1.NBorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.nTreeView1.SelectedNode = null;
            this.nTreeView1.Size = new System.Drawing.Size(188, 444);
            this.nTreeView1.TabIndex = 1;
            this.nTreeView1.Text = "nTreeView1";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "00654.png");
            this.imageList1.Images.SetKeyName(1, "aio_quickbar.png");
            this.imageList1.Images.SetKeyName(2, "award_gold_medal_star.png");
            this.imageList1.Images.SetKeyName(3, "client_account_template.png");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 493);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nTreeView1);
            this.Controls.Add(this.treeViewAdv1);
            this.Name = "Form5";
            this.Text = "Form5";
            this.Load += new System.EventHandler(this.Form5_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NTreeView treeViewAdv1;
        private ICSharpCode.WinFormsUI.Controls.NTreeView nTreeView1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button button1;
    }
}
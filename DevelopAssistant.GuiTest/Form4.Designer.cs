﻿namespace DevelopAssistant.GuiTest
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nListBox1 = new ICSharpCode.WinFormsUI.Controls.NListBox();
            this.SuspendLayout();
            // 
            // nListBox1
            // 
            this.nListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nListBox1.BackColor = System.Drawing.Color.White;
            this.nListBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.nListBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.nListBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.nListBox1.DrawMode = System.Windows.Forms.DrawMode.Normal;
            this.nListBox1.ItemHeight = 16;
            this.nListBox1.Location = new System.Drawing.Point(23, 24);
            this.nListBox1.MarginWidth = 0;
            this.nListBox1.MultiColumn = false;
            this.nListBox1.Name = "nListBox1";
            this.nListBox1.SelectedIndex = 0;
            this.nListBox1.SelectedItem = null;
            this.nListBox1.Size = new System.Drawing.Size(239, 232);
            this.nListBox1.TabIndex = 0;
            this.nListBox1.Text = "nListBox1";
            this.nListBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 268);
            this.Controls.Add(this.nListBox1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NListBox nListBox1;
    }
}
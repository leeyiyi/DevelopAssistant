﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            this.textEditorControl1.SetHighlighting("C#");
            textEditorControl1.Font = new Font(this.Font.FontFamily,14.0f);
            textEditorControl1.TextChanged += TextEditorControl1_TextChanged;
            textEditorControl1.ActiveTextAreaControl.VScrollBar.ValueChanged += VScrollBar_ValueChanged;
            textEditorControl1.ActiveTextAreaControl.HScrollBar.ValueChanged += HScrollBar_ValueChanged;
        }

        private void TextEditorControl1_TextChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void HScrollBar_ValueChanged(object sender, EventArgs e)
        {
            if (textEditorControl1.ComparisonState)
                textEditorControl1.ActiveTextAreaControl.UpdateVirtualTop();
        }

        private void VScrollBar_ValueChanged(object sender, EventArgs e)
        {
            if (textEditorControl1.ComparisonState)
                textEditorControl1.ActiveTextAreaControl.UpdateVirtualTop();
        }

        private void Test1()
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo("demo");
            foreach (System.IO.FileInfo fi in di.GetFiles())
            {
                string fileContent = string.Empty;
                string fileFormat = string.Empty;
                using (System.IO.FileStream stream = fi.OpenRead())
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    fileContent = System.Text.Encoding.UTF8.GetString(buffer);
                    Array.Clear(buffer, 0, buffer.Length);
                    buffer = null;
                }
                fileFormat = fi.Extension;
                SetContent(fileContent, fileFormat);
            }
        }

        private void Test2()
        {
            
            string SqlText = this.textEditorControl1.Text;
            string ExpressString = @"[\n|\s| ](delete|insert)[\s|\t| ].*";
            ExpressString = @"[\s](delete|insert|update)[\s]+?";
            ExpressString = @"[\s](delete|insert|update)[\s]{1,}";

            try
            {
                System.Text.RegularExpressions.Regex regex =
               new System.Text.RegularExpressions.Regex(ExpressString, System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                if (regex.IsMatch(SqlText))
                {
                    var match = regex.Match(SqlText);
                    string value = match.Groups[0].Value;
                    while (match.Success)
                    {
                        match = match.NextMatch();
                        value = match.Groups[0].Value;
                    }

                }
            }
            catch (Exception ex)
            {
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.textEditorControl1.Text = "";
        }

        private void SetContent(string content, string format)
        {
            string HighlightingName = "XML";

            switch (format.ToUpper())
            {
                case ".JS":
                    HighlightingName = "JavaScript";
                    break;
            }

            this.textEditorControl1.Text = content;
            this.textEditorControl1.SetHighlighting(HighlightingName);
            this.textEditorControl1.EnableFolding = true;

            this.textEditorControl1.Invalidate();
        }

        public void Test3()
        {
            string HighlightingName = "XML";
            this.textEditorControl1.Text = "(fdsafds)";            
            this.textEditorControl1.SetHighlighting("Black", HighlightingName);
            this.textEditorControl1.EnableFolding = true;
            this.textEditorControl1.Document.ReadOnly = true;
        }


        public void Test4()
        {
            //this.textEditorControl1.ImageLayout = ImageLayout.Zoom;
            //this.textEditorControl1.BackgroundImage = Properties.Resources.t01fe533be602b13943;

            this.textEditorControl1.ComparisonState = true;

            this.textEditorControl1.EnableFolding = true;

            this.textEditorControl1.ShowGuidelines = true;

            //textEditorControl1.SetHighlighting("Java");
            string HighlightingName = "Java";
            //string HighlightingName = "JavaScript";

            this.textEditorControl1.ShowVRuler = true;
            //this.textEditorControl1.ShowVRuler = false;
            this.textEditorControl1.LineViewerStyle = ICSharpCode.TextEditor.Document.LineViewerStyle.FullRow;

            this.textEditorControl1.Text = "(fdsafds)\nasdfsdaf\n2\n4\n6\nasdf";

            string path = "demo\\123.java";  
            string context = string.Empty;

            if (!System.IO.File.Exists(path))
            {
                MessageBox.Show("文件不存在");
                return;
            }

            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                context = sr.ReadToEnd();
            }

            string theme = "Black";
            //string theme = "Default";

            this.textEditorControl1.SetHighlighting(theme, HighlightingName);

            this.textEditorControl1.Text = context;

            //Color yellow = Color.FromArgb(204, 204, 51);
            Color yellow = Color.FromArgb(153, 153, 0);
            Color red = Color.FromArgb(045, 000, 000);
            Color green = Color.FromArgb(038, 094, 077);

            this.textEditorControl1.Document.FoldingManager.ClearFoldings();

            this.textEditorControl1.Document.FoldingManager.FoldingStrategy = new JavaFoldingStrategy();

            this.textEditorControl1.Document.FoldingManager.UpdateFoldings();

            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(green), 0);

            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(red), 2);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(yellow), 5);

            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(yellow), 8);

            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(green), 10);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(red), 12);


            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(green), 25);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(red), 26);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(green), 27);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(red), 28);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(yellow), 29);
            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(red), 30);

            this.textEditorControl1.ActiveTextAreaControl.AddLineCustomColor(CustomLineColor.Create(yellow), 40);

            this.textEditorControl1.Update();

        }

        public void Test5()
        {
            string path = "demo\\123.java";
            string context = string.Empty;

            if (!System.IO.File.Exists(path))
            {
                MessageBox.Show("文件不存在");
                return;
            }

            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                context = sr.ReadToEnd();
            }

            textEditorControl1.SetHighlighting("Java");
            textEditorControl1.Text = context;

            this.textEditorVScrollBar1.Scroll += TextEditorVScrollBar1_Scroll;
            this.textEditorControl1.ActiveTextAreaControl.VScrollBar.Scroll += VScrollBar_Scroll;
            this.textEditorVScrollBar1.Maximum = textEditorControl1.ActiveTextAreaControl.VScrollBar.Maximum;
            this.textEditorVScrollBar1.LargeChange= textEditorControl1.ActiveTextAreaControl.VScrollBar.LargeChange;
            this.textEditorVScrollBar1.SmallChange = textEditorControl1.ActiveTextAreaControl.VScrollBar.SmallChange;
            this.textEditorVScrollBar1.BorderVisable = true;

        }

        private void VScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            textEditorVScrollBar1.Value = textEditorControl1.ActiveTextAreaControl.VScrollBar.Value;
            this.textBox1.Text = textEditorControl1.ActiveTextAreaControl.VScrollBar.Value + " ; " + textEditorVScrollBar1.Value;
        }

        private void TextEditorVScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            textEditorControl1.ActiveTextAreaControl.VScrollBar.Value = textEditorVScrollBar1.Value;
            this.textBox1.Text = textEditorControl1.ActiveTextAreaControl.VScrollBar.Value + " ; " + textEditorVScrollBar1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Test5();
            Test4();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.textEditorControl1.ComparisonState = false;
            this.textEditorControl1.Invalidate();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class DataTableColumnInfo : ICloneable
    {
        public string IdType { get; set; }
        public string ColumnName { get; set; }
        public string PropertyName { get; set; }
        public string Description { get; set; }
        public bool IsParmaryKey { get; set; }
        public bool IsIdentity { get; set; }
        public string JdbcType { get; set; }
        public string ColumnDataType { get; set; }
        public string PropertyDataType { get; set; }
        public string JsonFormat { get; set; }
        public string DefaultValue { get; set; } 
        
        public object Clone()
        {
           return (DataTableColumnInfo)this.MemberwiseClone();
        }

    }
}

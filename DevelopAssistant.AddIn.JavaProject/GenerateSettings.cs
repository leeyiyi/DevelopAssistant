﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class GenerateSettings
    {
        public string NameSpace { get; set; }
        public string Modelpath { get; set; }
        public string ParamaryKey { get; set; }
        public bool CamelCase { get; set; }
        public string DataBaseType { get; set; }
        public bool AutoJdbcType { get; set; }
    }
}

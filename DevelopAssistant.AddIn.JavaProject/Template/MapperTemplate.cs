﻿<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${PakageName}.mapper.I${EntityName}Mapper">
  
  <resultMap id="Base${EntityName}ResultMap" type="${PakageName}.entity.${EntityName}Entity" >
#foreach($EntityProperty in $EntityProperties)
#if($EntityProperty.IsParmaryKey)
    <id column="$EntityProperty.ColumnName" property="$EntityProperty.PropertyName" jdbcType="$EntityProperty.JdbcType" />
#else
    <result column="$EntityProperty.ColumnName" property="$EntityProperty.PropertyName" jdbcType="$EntityProperty.JdbcType" />
#end
#end
  </resultMap>

  <!--分页查询列表-->
  <select id="select${EntityName}PageList"  resultMap="Base${EntityName}ResultMap">
#set($index = 0)
#foreach($EntityProperty in $EntityProperties)
#if($index==0)
    select $EntityProperty.ColumnName
#else
      ,$EntityProperty.ColumnName
#end
#set($index = $index + 1)
#end
    from ${TableName}
    <where>
      1=1
#foreach($QueryEntity in $QueryWrapper)
      <if test="$QueryEntity.Column!=null and $QueryEntity.Column!=''">
        and  $QueryEntity.Column=#{params.$QueryEntity.Value}
      </if>
#end
    </where>
#if(${OrderWrapper})
    order by ${OrderWrapper}
#end
  </select>

  <!--查询列表数据-->
  <select id="select${EntityName}ArrayList"  parameterType="java.util.HashMap"  resultMap="Base${EntityName}ResultMap">
#set($index = 0)
#foreach($EntityProperty in $EntityProperties)
#if($index==0)
    select $EntityProperty.ColumnName
#else
      ,$EntityProperty.ColumnName
#end
#set($index = $index + 1)
#end
    from ${TableName}
    <where>
      1=1
#foreach($QueryEntity in $QueryWrapper)
      <if test="$QueryEntity.Column!=null and $QueryEntity.Column!=''">
        and  $QueryEntity.Column=#{$QueryEntity.Value}
      </if>
#end
    </where>
#if(${OrderWrapper})
    order by ${OrderWrapper}
#end
  </select>

  <insert id="add${EntityName}" parameterType="${PakageName}.entity.${EntityName}Entity" resultType="java.lang.Integer">
    insert into ${TableName}
    (
      ${ColumnsString}
    )
    values
    (
      ${ValuesString}
    )
  </insert>

  <update id="update${EntityName}" parameterType="${PakageName}.entity.${EntityName}Entity" resultType="java.lang.Integer">
    update ${TableName}
#set($index = 0)
#foreach($EntityProperty in $EntityProperties)
#if($index==0)
    set $EntityProperty.ColumnName=#{$EntityProperty.PropertyName}
#else
       ,$EntityProperty.ColumnName=#{$EntityProperty.PropertyName}
#end
#set($index = $index + 1)
#end
#if(${ParmaryKeyColumnInfo})
    where ${ParmaryKeyColumnInfo.ColumnName}=#{${ParmaryKeyColumnInfo.PropertyName}}
#else
    where ${ParamaryKey}=''
#end
  </update>

  <delete id="delete${EntityName}" parameterType="${PakageName}.entity.${EntityName}Entity" resultType="java.lang.Integer">
    delete from ${TableName}
#if(${ParmaryKeyColumnInfo})
    where ${ParmaryKeyColumnInfo.ColumnName}=#{${ParmaryKeyColumnInfo.PropertyName}}
#else
    where ${ParamaryKey}=''
#end
  </delete>
  
  
</mapper>
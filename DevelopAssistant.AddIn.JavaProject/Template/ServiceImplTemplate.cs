﻿package ${PakageName}.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.sql.visitor.functions.Now;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${PakageName}.exception.BizException;
import ${PakageName}.entity.${EntityName}Entity;
import ${PakageName}.mapper.I${EntityName}Mapper;
import ${PakageName}.demo.service.I${EntityName}Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("${EntityName}Service")
public class ${EntityName}ServiceImpl extends ServiceImpl<I${EntityName}Mapper, ${EntityName}Entity> implements I${EntityName}Service {

	@Autowired
	public I${EntityName}Mapper ${InstanceName}Mapper;
	
	/**
	*分页获取数据
	*/
	@SuppressWarnings("unchecked")
	public Page<Map<String, Object>> query${EntityName}PageList(Map<String,Object> params) throws BizException{
		Page<Map<String, Object>> result = null;
		try {			
			int pageSize=(int)params.get("pageSize");
		    int currentPage=(int)params.get("currentPage");		
		    result = new Page<>(currentPage,pageSize);			 			
			List<Map<String, Object>> records = ${InstanceName}Mapper.select${EntityName}PageList(result,params);			
			result.setRecords(records);			
		} 
		catch (Exception e) {
			// TODO: handle exception
			log.error("查询 query${EntityName}PageList 数据出错："+whereMap,e); 
			throw new BizException("查询 query${EntityName}PageList 数据出错:"+e.getMessage(),e);
		}		
		return result;
	}

	/**
	*获取数据列表
	*/
	public List<Map<String,Object>> query${EntityName}ArrayList(Map<String,Object> params) throws BizException{
		List<Map<String,Object>> result = null;
		try {				 			
			result = ${InstanceName}Mapper.select${EntityName}ArrayList(params);				
		} 
		catch (Exception e) {
			// TODO: handle exception
			log.error("查询 query${EntityName}PageList 数据出错："+whereMap,e); 
			throw new BizException("查询 query${EntityName}PageList 数据出错:"+e.getMessage(),e);
		}		
		return result;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public String add${EntityName}(${EntityName}Entity entity) throws BizException {
		String result="";		
		try {			
			//添加数据			 
			entity.insert();			 
		}
		catch (Exception ex) {
			// TODO: handle exception
			throw new BizException("添加${EntityName}时出错"+ex.getMessage(), ex);
		}		
		return result;
	}
	
}

namespace ICSharpCode.WinFormsUI.Docking
{
    /// <summary>
    /// DockPanel Suite theme interface.
    /// </summary>
    public interface IDockPanelTheme
    {
        /// <summary>
        /// Applies the specified theme to the dock panel.
        /// </summary>
        /// <param name="dockPanel">The dock panel.</param>
        void Apply(DockPanel dockPanel);
    }
    

}
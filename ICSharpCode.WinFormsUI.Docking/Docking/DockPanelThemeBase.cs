﻿using System.ComponentModel;

namespace ICSharpCode.WinFormsUI.Docking
{
    public abstract class DockPanelThemeBase : Component, IDockPanelTheme
	{
	    public abstract void Apply(DockPanel dockPanel);

        public abstract void Apply(DockPanel dockPanel, string colorStyle);        
	}
}

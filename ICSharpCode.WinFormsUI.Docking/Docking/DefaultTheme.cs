﻿using System;
using System.Collections.Generic;
using System.Drawing; 

namespace ICSharpCode.WinFormsUI.Docking
{
    public class DefaultTheme : DockPanelThemeBase
    {
        public override void Apply(DockPanel dockPanel)
        {
            if (dockPanel == null)
            {
                throw new NullReferenceException("dockPanel");
            }

            Measures.SplitterSize = 4;
            dockPanel.Extender.DockPaneCaptionFactory = null;
            dockPanel.Extender.AutoHideStripFactory = null;
            dockPanel.Extender.AutoHideWindowFactory = null;
            dockPanel.Extender.DockPaneStripFactory = null;
            dockPanel.Extender.DockPaneSplitterControlFactory = null;
            dockPanel.Extender.DockWindowSplitterControlFactory = null;
            dockPanel.Extender.DockWindowFactory = null;
            dockPanel.Extender.PaneIndicatorFactory = null;
            dockPanel.Extender.PanelIndicatorFactory = null;
            dockPanel.Extender.DockOutlineFactory = null;
            DockPanelThemeStyle.ControlStyle = new ControlStyle("Default");
            dockPanel.Skin = CreateDefaultSkin();            
        }

        public override void Apply(DockPanel dockPanel, string colorStyle)
        {
            DockPanelThemeStyle.ControlStyle = new ControlStyle("Default");
            dockPanel.Skin = CreateDefaultSkin();            
        }

        internal static DockPanelSkin CreateDefaultSkin()
        {
            DockPanelSkin skin = new DockPanelSkin();

            Color TabBaseColor = Color.FromArgb(214, 219, 233);
            Color TabOtherColor = Color.FromArgb(214, 219, 233);
            Color TabActiveColor = SystemColors.Control;

            Color ToolBaseColor = Color.FromArgb(214, 219, 233);
            Color ToolActiveColor = Color.FromArgb(51, 51, 255);


            skin.AutoHideStripSkin.DockStripGradient.StartColor = SystemColors.ControlLight;
            skin.AutoHideStripSkin.DockStripGradient.EndColor = SystemColors.ControlLight;
            skin.AutoHideStripSkin.TabGradient.TextColor = SystemColors.ControlDarkDark;

            //skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = SystemColors.Control;
            //skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = SystemColors.Control;
            //skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = SystemColors.ControlLightLight;
            //skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = SystemColors.ControlLightLight;
            //skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = SystemColors.ControlLight;
            //skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = SystemColors.ControlLight;

            skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = TabBaseColor;
            skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = TabBaseColor;
            skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = TabActiveColor;
            skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = TabActiveColor;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor = SystemColors.ControlText;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = TabOtherColor;
            skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = TabOtherColor;


            skin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.StartColor = SystemColors.ControlLight;
            skin.DockPaneStripSkin.ToolWindowGradient.DockStripGradient.EndColor = SystemColors.ControlLight;

            skin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.StartColor = SystemColors.Control;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveTabGradient.EndColor = SystemColors.Control;

            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.StartColor = Color.Transparent;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.EndColor = Color.Transparent;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveTabGradient.TextColor = SystemColors.ControlDarkDark;

            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.StartColor = SystemColors.GradientActiveCaption;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.EndColor = SystemColors.GradientActiveCaption;
            //skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
            skin.DockPaneStripSkin.ToolWindowGradient.ActiveCaptionGradient.TextColor = SystemColors.ActiveCaptionText;

            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.StartColor = TabBaseColor;// SystemColors.GradientInactiveCaption;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.EndColor = TabBaseColor;// SystemColors.InactiveCaption;
            //skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.LinearGradientMode = LinearGradientMode.Vertical;
            skin.DockPaneStripSkin.ToolWindowGradient.InactiveCaptionGradient.TextColor = SystemColors.InactiveCaptionText;

            return skin;
        }

    }
}

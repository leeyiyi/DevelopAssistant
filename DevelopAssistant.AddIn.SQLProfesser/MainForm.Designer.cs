﻿namespace DevelopAssistant.AddIn.SQLProfesser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.zTreeview = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonConnectionSetting = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.NProgressBar = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textEditorControl1 = new ICSharpCode.TextEditor.TextEditorControl();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.chkMeasureData = new System.Windows.Forms.CheckBox();
            this.chkConfigure = new System.Windows.Forms.CheckBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSQLFormat = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRebuild = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.NProgressBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(693, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(44, 17);
            this.toolStripStatusLabel1.Text = "状态：";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel2.Text = "完成";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 487);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.zTreeview);
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.NProgressBar);
            this.splitContainer1.Panel2.Controls.Add(this.textEditorControl1);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(690, 487);
            this.splitContainer1.SplitterDistance = 160;
            this.splitContainer1.TabIndex = 2;
            // 
            // zTreeview
            // 
            this.zTreeview.CheckBoxes = true;
            this.zTreeview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zTreeview.ImageIndex = 0;
            this.zTreeview.ImageList = this.imageList1;
            this.zTreeview.Location = new System.Drawing.Point(0, 25);
            this.zTreeview.Name = "zTreeview";
            this.zTreeview.SelectedImageIndex = 0;
            this.zTreeview.Size = new System.Drawing.Size(160, 462);
            this.zTreeview.TabIndex = 1;
            this.zTreeview.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.zTreeview_AfterCheck);
            this.zTreeview.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.zTreeview_AfterCollapse);
            this.zTreeview.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.zTreeview_AfterExpand);
            this.zTreeview.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.zTreeview_NodeMouseClick);
            this.zTreeview.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.zTreeview_NodeMouseDoubleClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "zTree02.png");
            this.imageList1.Images.SetKeyName(1, "zTree04.png");
            this.imageList1.Images.SetKeyName(2, "zTree05.png");
            this.imageList1.Images.SetKeyName(3, "fan_24px.png");
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonConnectionSetting,
            this.toolStripButtonRefresh});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(160, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonConnectionSetting
            // 
            this.toolStripButtonConnectionSetting.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.tool;
            this.toolStripButtonConnectionSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonConnectionSetting.Name = "toolStripButtonConnectionSetting";
            this.toolStripButtonConnectionSetting.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonConnectionSetting.Text = "设置连接";
            this.toolStripButtonConnectionSetting.Click += new System.EventHandler(this.toolStripButtonConnectionSetting_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonRefresh.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.all_refresh_reload_sync_16px;
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(52, 22);
            this.toolStripButtonRefresh.Text = "刷新";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // NProgressBar
            // 
            this.NProgressBar.Controls.Add(this.label1);
            this.NProgressBar.Controls.Add(this.progressBar1);
            this.NProgressBar.Location = new System.Drawing.Point(154, 189);
            this.NProgressBar.Name = "NProgressBar";
            this.NProgressBar.Size = new System.Drawing.Size(221, 82);
            this.NProgressBar.TabIndex = 3;
            this.NProgressBar.Visible = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(18, 15);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(187, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // textEditorControl1
            // 
            this.textEditorControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEditorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEditorControl1.IsReadOnly = false;
            this.textEditorControl1.Location = new System.Drawing.Point(0, 25);
            this.textEditorControl1.Name = "textEditorControl1";
            this.textEditorControl1.Size = new System.Drawing.Size(526, 401);
            this.textEditorControl1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.LeftRight;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.chkMeasureData);
            this.panel1.Controls.Add(this.chkConfigure);
            this.panel1.Controls.Add(this.btnExecute);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 426);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(526, 61);
            this.panel1.TabIndex = 1;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // chkMeasureData
            // 
            this.chkMeasureData.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.database_data;
            this.chkMeasureData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkMeasureData.Location = new System.Drawing.Point(140, 6);
            this.chkMeasureData.Name = "chkMeasureData";
            this.chkMeasureData.Size = new System.Drawing.Size(116, 46);
            this.chkMeasureData.TabIndex = 2;
            this.chkMeasureData.Text = "巡检数据库";
            this.chkMeasureData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMeasureData.UseVisualStyleBackColor = true;
            // 
            // chkConfigure
            // 
            this.chkConfigure.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.database_config;
            this.chkConfigure.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkConfigure.Location = new System.Drawing.Point(13, 6);
            this.chkConfigure.Name = "chkConfigure";
            this.chkConfigure.Size = new System.Drawing.Size(116, 46);
            this.chkConfigure.TabIndex = 1;
            this.chkConfigure.Text = "组态数据库";
            this.chkConfigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkConfigure.UseVisualStyleBackColor = true;
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.exe;
            this.btnExecute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExecute.Location = new System.Drawing.Point(418, 7);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(94, 46);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "执行命令 ";
            this.btnExecute.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpen,
            this.toolStripButtonSQLFormat,
            this.toolStripButtonSave,
            this.toolStripButtonRebuild});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(526, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.Enabled = false;
            this.toolStripButtonOpen.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources._new;
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonOpen.Text = "打开文件";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonSQLFormat
            // 
            this.toolStripButtonSQLFormat.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.document_editing;
            this.toolStripButtonSQLFormat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSQLFormat.Name = "toolStripButtonSQLFormat";
            this.toolStripButtonSQLFormat.Size = new System.Drawing.Size(64, 22);
            this.toolStripButtonSQLFormat.Text = "格式化";
            this.toolStripButtonSQLFormat.Click += new System.EventHandler(this.toolStripButtonSQLFormat_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Enabled = false;
            this.toolStripButtonSave.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.save_alt_16px;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonSave.Text = "保存文件";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonRebuild
            // 
            this.toolStripButtonRebuild.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonRebuild.Image = global::DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.indexes_16px;
            this.toolStripButtonRebuild.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRebuild.Name = "toolStripButtonRebuild";
            this.toolStripButtonRebuild.Size = new System.Drawing.Size(100, 22);
            this.toolStripButtonRebuild.Text = "重新创建索引";
            this.toolStripButtonRebuild.Click += new System.EventHandler(this.toolStripButtonRebuild_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(693, 509);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.NProgressBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView zTreeview;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonConnectionSetting;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ToolStripButton toolStripButtonSQLFormat;
        private ICSharpCode.TextEditor.TextEditorControl textEditorControl1;
        private System.Windows.Forms.ToolStripButton toolStripButtonSave;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.CheckBox chkMeasureData;
        private System.Windows.Forms.CheckBox chkConfigure;
        private System.Windows.Forms.Panel NProgressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripButton toolStripButtonRebuild;



    }
}
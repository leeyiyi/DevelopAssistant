﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.SQLProfesser
{
    public partial class SettingsForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        public NORM.Common.ConnectionString ConnectionString = new NORM.Common.ConnectionString();

        public SettingsForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        public SettingsForm(string connectionString)
            : this()
        {
            ConnectionString = new NORM.Common.ConnectionString(connectionString);
        }

        private void InitializeControls()
        {           
            this.txtPassword.Icon = DevelopAssistant.AddIn.SQLProfesser.Properties.Resources.blank;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Right;
            this.txtPassword.IsPasswordTextBox = true;
            this.txtPassword.UnlockSecret = new EventHandler(txtPassword_UnlockSecret);
        }

        private void LoadXTheme()
        {
            panel1.BorderColor = this.XTheme.FormBorderOutterColor;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            ConnectionString.Server = this.txtServer.Text.Trim();
            if (!string.IsNullOrEmpty(this.txtPort.Text) && this.txtPort.Text != "默认")
            {
                ConnectionString.Port = this.txtPort.Text.Trim();
            }
            ConnectionString.UserId = this.txtUid.Text.Trim();
            ConnectionString.Password = this.txtPassword.Text.Trim();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            LoadXTheme();

            this.txtServer.Text = ConnectionString.Server;
            this.txtPort.Text = ConnectionString.Port;
            this.txtUid.Text = ConnectionString.UserId;
            this.txtPassword.Text = ConnectionString.Password;

            if (string.IsNullOrEmpty(ConnectionString.Port))
            {
                this.txtPort.Text = "默认";
            }

        }

        private void txtPassword_UnlockSecret(object sender, EventArgs e)
        {
            if (txtPassword.PasswordChar == '*')
            {
                txtPassword.PasswordChar = txtUid.PasswordChar;
            }
            else
            {
                txtPassword.PasswordChar = '*';
            }
        }

    }
}

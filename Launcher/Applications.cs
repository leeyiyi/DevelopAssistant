﻿using System;
using System.Collections.Generic;
using System.Diagnostics; 
using System.Text; 

namespace Launcher
{
    public class Applications
    {
        public static void KillProcess(string name)
        {
            foreach (Process p in System.Diagnostics.Process.GetProcessesByName(name))
            {
                try
                {
                    p.Kill();
                    p.WaitForExit();
                }
                catch (Exception exp)
                {
                    //Console.WriteLine(exp.Message);
                    System.Diagnostics.EventLog.WriteEntry("AlchemySearch:KillProcess", exp.Message, System.Diagnostics.EventLogEntryType.Error);
                }
            }
        }
    }
}

﻿using System;
 
namespace ICSharpCode.WinFormsUI.Controls
{
    public enum DocumentType
    {
        None,
        Csharp,
        Javascript,
        Html,
        Aspx,
        Css,
        Python,
        Xml,
        Configuation,
        Java,
        Jsp,
        Text,
        Word,
        Json,
        TSql
    }
}

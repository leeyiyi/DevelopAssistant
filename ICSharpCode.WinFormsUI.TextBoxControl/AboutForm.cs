﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle,
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //左边
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //上边
    SystemColors.ControlDark, 1, ButtonBorderStyle.Solid, //右边
    SystemColors.ControlDark, 1, ButtonBorderStyle.None);//底边

        }
    }
}

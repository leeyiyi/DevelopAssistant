﻿
namespace ICSharpCode.WinFormsUI.Controls
{
    partial class CodeFormatControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textEditorControl1 = new ICSharpCode.TextEditor.TextEditorControl();
            this.EditorContainer = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripLabelFont = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxFontValue = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabelFontSize = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxFontSizeValue = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonLineNumber = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAddComment = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonRemoveComment = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonIndent = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonOutdent = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonFormat = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonClear = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonLanguage = new ICSharpCode.WinFormsUI.Controls.NToolStripDropDownButton();
            this.sqlToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.csharpToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.javascriptToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.pythonToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.cssToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.htmlToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.jsonToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.textToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripButtonOpen = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonSave = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonAbout = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.EditorContainer.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textEditorControl1
            // 
            this.textEditorControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textEditorControl1.BookMarkEnableToggle = true;
            this.textEditorControl1.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.textEditorControl1.ComparisonState = false;
            this.textEditorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEditorControl1.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.textEditorControl1.IsReadOnly = false;
            this.textEditorControl1.Location = new System.Drawing.Point(0, 1);
            this.textEditorControl1.Name = "textEditorControl1";
            this.textEditorControl1.ShowGuidelines = false;
            this.textEditorControl1.Size = new System.Drawing.Size(655, 354);
            this.textEditorControl1.TabIndex = 1;
            // 
            // EditorContainer
            // 
            this.EditorContainer.BackColor = System.Drawing.SystemColors.Control;
            this.EditorContainer.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.EditorContainer.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.EditorContainer.BottomBlackColor = System.Drawing.Color.Empty;
            this.EditorContainer.Controls.Add(this.textEditorControl1);
            this.EditorContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorContainer.Location = new System.Drawing.Point(1, 30);
            this.EditorContainer.MarginWidth = 0;
            this.EditorContainer.Name = "EditorContainer";
            this.EditorContainer.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.EditorContainer.Size = new System.Drawing.Size(655, 356);
            this.EditorContainer.TabIndex = 2;
            this.EditorContainer.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(248)))), ((int)(((byte)(250)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;            
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelFont,
            this.toolStripComboBoxFontValue,
            this.toolStripLabelFontSize,
            this.toolStripComboBoxFontSizeValue,
            this.toolStripButtonLineNumber,
            this.toolStripButtonAddComment,
            this.toolStripButtonRemoveComment,
            this.toolStripButtonIndent,
            this.toolStripButtonOutdent,
            this.toolStripButtonFormat,
            this.toolStripButtonClear,
            this.toolStripButtonLanguage,
            this.toolStripButtonOpen,
            this.toolStripButtonSave,
            this.toolStripButtonAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(655, 28);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabelFont
            // 
            this.toolStripLabelFont.Name = "toolStripLabelFont";
            this.toolStripLabelFont.Size = new System.Drawing.Size(44, 23);
            this.toolStripLabelFont.Text = "字体：";
            // 
            // toolStripComboBoxFontValue
            // 
            this.toolStripComboBoxFontValue.Items.AddRange(new object[] {
            "默认",
            "宋体",
            "仿宋",
            "雅黑"});
            this.toolStripComboBoxFontValue.Name = "toolStripComboBoxFontValue";
            this.toolStripComboBoxFontValue.Size = new System.Drawing.Size(121, 26);
            this.toolStripComboBoxFontValue.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxFontValue_SelectedIndexChanged);
            // 
            // toolStripLabelFontSize
            // 
            this.toolStripLabelFontSize.Name = "toolStripLabelFontSize";
            this.toolStripLabelFontSize.Size = new System.Drawing.Size(68, 23);
            this.toolStripLabelFontSize.Text = "字体大小：";
            // 
            // toolStripComboBoxFontSizeValue
            // 
            this.toolStripComboBoxFontSizeValue.Items.AddRange(new object[] {
            "10",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22"});
            this.toolStripComboBoxFontSizeValue.Name = "toolStripComboBoxFontSizeValue";
            this.toolStripComboBoxFontSizeValue.Size = new System.Drawing.Size(121, 26);
            this.toolStripComboBoxFontSizeValue.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxFontSizeValue_SelectedIndexChanged);
            // 
            // toolStripButtonLineNumber
            // 
            this.toolStripButtonLineNumber.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLineNumber.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.format_line_spacing;
            this.toolStripButtonLineNumber.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLineNumber.Name = "toolStripButtonLineNumber";
            this.toolStripButtonLineNumber.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonLineNumber.Text = "行号";
            this.toolStripButtonLineNumber.Click += new System.EventHandler(this.toolStripButtonLineNumber_Click);
            // 
            // toolStripButtonAddComment
            // 
            this.toolStripButtonAddComment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddComment.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.note;
            this.toolStripButtonAddComment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddComment.Name = "toolStripButtonAddComment";
            this.toolStripButtonAddComment.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonAddComment.Text = "添加注释";
            this.toolStripButtonAddComment.Click += new System.EventHandler(this.toolStripButtonAddComment_Click);
            // 
            // toolStripButtonRemoveComment
            // 
            this.toolStripButtonRemoveComment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRemoveComment.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.uncomment;
            this.toolStripButtonRemoveComment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemoveComment.Name = "toolStripButtonRemoveComment";
            this.toolStripButtonRemoveComment.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonRemoveComment.Text = "取消注释";
            this.toolStripButtonRemoveComment.Click += new System.EventHandler(this.toolStripButtonRemoveComment_Click);
            // 
            // toolStripButtonIndent
            // 
            this.toolStripButtonIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonIndent.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.indent;
            this.toolStripButtonIndent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonIndent.Name = "toolStripButtonIndent";
            this.toolStripButtonIndent.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonIndent.Text = "右移";
            this.toolStripButtonIndent.Click += new System.EventHandler(this.toolStripButtonIndent_Click);
            // 
            // toolStripButtonOutdent
            // 
            this.toolStripButtonOutdent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOutdent.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.outdent;
            this.toolStripButtonOutdent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOutdent.Name = "toolStripButtonOutdent";
            this.toolStripButtonOutdent.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonOutdent.Text = "左移";
            this.toolStripButtonOutdent.Click += new System.EventHandler(this.toolStripButtonOutdent_Click);
            // 
            // toolStripButtonFormat
            // 
            this.toolStripButtonFormat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFormat.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.formatting;
            this.toolStripButtonFormat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFormat.Name = "toolStripButtonFormat";
            this.toolStripButtonFormat.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonFormat.Text = "格式化";
            // 
            // toolStripButtonClear
            // 
            this.toolStripButtonClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClear.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.trash_empty;
            this.toolStripButtonClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClear.Name = "toolStripButtonClear";
            this.toolStripButtonClear.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonClear.Text = "清空";
            this.toolStripButtonClear.Click += new System.EventHandler(this.toolStripButtonClear_Click);
            // 
            // toolStripButtonLanguage
            // 
            this.toolStripButtonLanguage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLanguage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sqlToolStripMenuItem,
            this.csharpToolStripMenuItem,
            this.javascriptToolStripMenuItem,
            this.pythonToolStripMenuItem,
            this.cssToolStripMenuItem,
            this.htmlToolStripMenuItem,
            this.jsonToolStripMenuItem,
            this.textToolStripMenuItem});
            this.toolStripButtonLanguage.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.dropdown;
            this.toolStripButtonLanguage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLanguage.Name = "toolStripButtonLanguage";
            this.toolStripButtonLanguage.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.toolStripButtonLanguage.Size = new System.Drawing.Size(33, 23);
            this.toolStripButtonLanguage.Text = "设置";
            // 
            // sqlToolStripMenuItem
            // 
            this.sqlToolStripMenuItem.AddIn = null;
            this.sqlToolStripMenuItem.Index = 0;
            this.sqlToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.sqlToolStripMenuItem.Level = 0;
            this.sqlToolStripMenuItem.Name = "sqlToolStripMenuItem";
            this.sqlToolStripMenuItem.Position = null;
            this.sqlToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sqlToolStripMenuItem.Text = "Sql";
            this.sqlToolStripMenuItem.Click += new System.EventHandler(this.sqlToolStripMenuItem_Click);
            // 
            // csharpToolStripMenuItem
            // 
            this.csharpToolStripMenuItem.AddIn = null;
            this.csharpToolStripMenuItem.Index = 0;
            this.csharpToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.csharpToolStripMenuItem.Level = 0;
            this.csharpToolStripMenuItem.Name = "csharpToolStripMenuItem";
            this.csharpToolStripMenuItem.Position = null;
            this.csharpToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.csharpToolStripMenuItem.Text = "C#";
            this.csharpToolStripMenuItem.Click += new System.EventHandler(this.cToolStripMenuItem_Click);
            // 
            // javascriptToolStripMenuItem
            // 
            this.javascriptToolStripMenuItem.AddIn = null;
            this.javascriptToolStripMenuItem.Index = 0;
            this.javascriptToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.javascriptToolStripMenuItem.Level = 0;
            this.javascriptToolStripMenuItem.Name = "javascriptToolStripMenuItem";
            this.javascriptToolStripMenuItem.Position = null;
            this.javascriptToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.javascriptToolStripMenuItem.Text = "Javascript";
            this.javascriptToolStripMenuItem.Click += new System.EventHandler(this.javascriptToolStripMenuItem_Click);
            // 
            // pythonToolStripMenuItem
            // 
            this.pythonToolStripMenuItem.AddIn = null;
            this.pythonToolStripMenuItem.Index = 0;
            this.pythonToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.pythonToolStripMenuItem.Level = 0;
            this.pythonToolStripMenuItem.Name = "pythonToolStripMenuItem";
            this.pythonToolStripMenuItem.Position = null;
            this.pythonToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pythonToolStripMenuItem.Text = "Python";
            this.pythonToolStripMenuItem.Visible = false;
            this.pythonToolStripMenuItem.Click += new System.EventHandler(this.pythonToolStripMenuItem_Click);
            // 
            // cssToolStripMenuItem
            // 
            this.cssToolStripMenuItem.AddIn = null;
            this.cssToolStripMenuItem.Index = 0;
            this.cssToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.cssToolStripMenuItem.Level = 0;
            this.cssToolStripMenuItem.Name = "cssToolStripMenuItem";
            this.cssToolStripMenuItem.Position = null;
            this.cssToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cssToolStripMenuItem.Text = "Css";
            this.cssToolStripMenuItem.Visible = false;
            this.cssToolStripMenuItem.Click += new System.EventHandler(this.cssToolStripMenuItem_Click);
            // 
            // htmlToolStripMenuItem
            // 
            this.htmlToolStripMenuItem.AddIn = null;
            this.htmlToolStripMenuItem.Index = 0;
            this.htmlToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.htmlToolStripMenuItem.Level = 0;
            this.htmlToolStripMenuItem.Name = "htmlToolStripMenuItem";
            this.htmlToolStripMenuItem.Position = null;
            this.htmlToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.htmlToolStripMenuItem.Text = "Html";
            this.htmlToolStripMenuItem.Visible = false;
            this.htmlToolStripMenuItem.Click += new System.EventHandler(this.htmlToolStripMenuItem_Click);
            // 
            // jsonToolStripMenuItem
            // 
            this.jsonToolStripMenuItem.AddIn = null;
            this.jsonToolStripMenuItem.Index = 0;
            this.jsonToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.jsonToolStripMenuItem.Level = 0;
            this.jsonToolStripMenuItem.Name = "jsonToolStripMenuItem";
            this.jsonToolStripMenuItem.Position = null;
            this.jsonToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.jsonToolStripMenuItem.Text = "Json";
            this.jsonToolStripMenuItem.Click += new System.EventHandler(this.jsonToolStripMenuItem_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.AddIn = null;
            this.textToolStripMenuItem.Index = 0;
            this.textToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.textToolStripMenuItem.Level = 0;
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Position = null;
            this.textToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.open_file;
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonOpen.Text = "打开";
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.save_as;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonSave.Text = "保存";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAbout.Image = global::ICSharpCode.WinFormsUI.Controls.Properties.Resources.about;
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Size = new System.Drawing.Size(23, 23);
            this.toolStripButtonAbout.Text = "关于";
            this.toolStripButtonAbout.Click += new System.EventHandler(this.toolStripButtonAbout_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(248)))), ((int)(((byte)(250)))));
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.panel1.Size = new System.Drawing.Size(655, 27);
            this.panel1.TabIndex = 3;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // CodeFormatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.EditorContainer);
            this.Controls.Add(this.panel1);
            this.Name = "CodeFormatControl";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(657, 387);
            this.EditorContainer.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonOpen;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonFormat;
        private TextEditor.TextEditorControl textEditorControl1;

        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonSave;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAddComment;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonRemoveComment;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonClear;
        private System.Windows.Forms.ToolStripLabel toolStripLabelFont;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxFontValue;
        private System.Windows.Forms.ToolStripLabel toolStripLabelFontSize;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxFontSizeValue;


        #endregion
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonAbout;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonIndent;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonOutdent;
        private ICSharpCode.WinFormsUI.Controls.NPanel EditorContainer;
        private ICSharpCode.WinFormsUI.Controls.NToolStripDropDownButton toolStripButtonLanguage;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem csharpToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem javascriptToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem sqlToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonLineNumber;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem pythonToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem cssToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem htmlToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem jsonToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem textToolStripMenuItem;
    }
}

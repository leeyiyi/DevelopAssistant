﻿using System;
using System.Collections.Generic;
using System.Drawing; 

namespace ICSharpCode.TextEditor
{
    public interface IThemeColorMatcher
    {
        /// <summary>
        /// 默认文字颜色
        /// </summary>
        Color Color { get; }

        /// <summary>
        /// 处理颜色匹配
        /// </summary>
        /// <param name="color"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        Color Resolve(Color color, string name);
    }

    public class BlackThemeMatcher : IThemeColorMatcher
    {
        Color _color = Color.FromArgb(220, 220, 220);
        public Color Color
        {
            get { return _color; }
        }

        public Color Resolve(Color color, string name)
        {
            if (name.EndsWith("blue",
                    StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "blue");
            }
            if (name.EndsWith("0000FF",
                StringComparison.OrdinalIgnoreCase))
            {               
                color = ThemesMapManager.getThemeMapColor("Black", "blue");
            }

            if (name.EndsWith("green",
                StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "green");
            }
            if (name.EndsWith("00FF00",
                StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "green");
            }

            if (name.EndsWith("DarkMagenta",
                StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "purple");
            }
            if (name.EndsWith("Purple",
               StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "purple");
            }
            if (name.EndsWith("8B008B",
            StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "purple");
            }

            if (name.EndsWith("Navy",
                StringComparison.OrdinalIgnoreCase))
            {               
                color = ThemesMapManager.getThemeMapColor("Black", "purple");
            }
            if (name.EndsWith("000080",
                StringComparison.OrdinalIgnoreCase))
            {                
                color = ThemesMapManager.getThemeMapColor("Black", "purple");
            }

            if (name.Equals("black",
                StringComparison.OrdinalIgnoreCase) ||
                name.Equals("Transparent",
                StringComparison.OrdinalIgnoreCase))
            {                 
                color = ThemesMapManager.getThemeMapColor("Black", "black");
            }

            return color;
        }

        public BlackThemeMatcher(Dictionary<string, Color> map)
        {
            map.Add("blue", Color.FromArgb(086, 156, 214));
            map.Add("green", Color.FromArgb(0, 204, 0));
            map.Add("purple", Color.FromArgb(209, 95, 238));
            map.Add("black", Color.FromArgb(220, 220, 220));
        }
    }
}

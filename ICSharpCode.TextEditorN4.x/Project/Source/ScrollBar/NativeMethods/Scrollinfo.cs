﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class NativeMethods
    {
        internal const int SB_CTL = 2;       
        internal const int SIF_ALL = (0x0001 | 0x0002 | 0x0004 | 0x0010);

        internal const int WHEEL_DELTA = 120;

        // basically it's constant now, but could be changed later by MS
        internal const int WHEEL_MAX_DELTA = 120;

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern bool GetScrollInfo(HandleRef hWnd, int fnBar, NativeMethods.Scrollinfo si);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern int SetScrollInfo(HandleRef hWnd, int fnBar, NativeMethods.Scrollinfo si, bool redraw);

        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="wnd">The handle of the control.</param>
        /// <param name="msg">The message as int.</param>
        /// <param name="param">param - true or false.</param>
        /// <param name="lparam">Additional parameter.</param>
        /// <returns>0 or error code.</returns>
        /// <remarks>Needed for sending the stop/start drawing of the control.</remarks>
        [DllImport("user32.dll")]
        public static extern int SendMessage(
           IntPtr wnd,
           int msg,
           bool param,
           int lparam);

        [StructLayout(LayoutKind.Sequential)]
        public class Scrollinfo
        {
            public int fMask;
            public int nMin;
            public int nMax;
            public int nPage;
            public int nPos;
            public int cbSize;
            public int nTrackPos;

            public Scrollinfo()
            {
                cbSize = Marshal.SizeOf(typeof(Scrollinfo));
            }

            public Scrollinfo(int mask, int min, int max, int page, int pos) : this()
            {
                fMask = mask;
                nMin = min;
                nMax = max;
                nPage = page;
                nPos = pos;
            }

           

        }
    }    
}

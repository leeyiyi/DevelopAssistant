﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class TextEditorVScrollBar : TextEditorScrollBar
    {
        public TextEditorVScrollBar()
        {
            this.orientation = ScrollBarOrientation.Vertical;
            this.scrollOrientation = System.Windows.Forms.ScrollOrientation.VerticalScroll;
        }        
    }
}

﻿// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Mike Krüger" email="mike@icsharpcode.net"/>
//     <version>$Revision$</version>
// </file>

using ICSharpCode.TextEditor.Document;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ICSharpCode.TextEditor
{
	/// <summary>
	/// Horizontal ruler - text column measuring ruler at the top of the text area.
	/// </summary>
	public class HRuler : Control
	{
		TextArea textArea;

        StringFormat stringFormat;

        public HRuler(TextArea textArea)
		{
			this.textArea = textArea;
            stringFormat = new StringFormat() { Alignment = StringAlignment.Center };
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Invalidate();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            HighlightColor HRulerPainterColor = textArea.Document.HighlightingStrategy.GetColorFor("HRuler");

            int num = 0;
            int DrawingPositionLeft = textArea.TextView.DrawingPosition.Left + 3;
            int DrawingPositionWidth = textArea.TextView.DrawingPosition.Right - 3;

            //using (Brush brush = new LinearGradientBrush(new Rectangle(0, -20, this.Width, 60),
            //  Color.White, HRulerPainterColor.BackgroundColor, LinearGradientMode.Vertical))
            //{
            //    g.FillRectangle(brush, ClientRectangle);
            //}

            using (Brush brush = new SolidBrush(Color.FromArgb(160, HRulerPainterColor.BackgroundColor)))
            {
                g.FillRectangle(brush, ClientRectangle);
            }

            Color HRulerBorderColor = HRulerPainterColor.Color;
            switch (textArea.Document.ThemeName)
            {
                case "Black":
                    HRulerBorderColor = Color.FromArgb(093, 140, 201);
                    break;
                case "Default":
                    HRulerBorderColor = SystemColors.ControlLight;
                    break;
            }
            g.DrawLine(new Pen(HRulerBorderColor), new Point(0, 0),
               new Point(this.Width, 0));

            if (textArea.Document.TextEditorProperties.ShowLineNumbers)
            {
                int drawIconXpos = textArea.TextView.DrawingPosition.Left - 18;
                Rectangle iconRect = new Rectangle(drawIconXpos, 6, 16, 16);                
                g.DrawImage(ICSharpCode.TextEditor.Properties.Resources.FaceMonkey, iconRect);                 
            }

            //using (Brush brush = new SolidBrush(HRulerPainterColor.BackgroundColor))
            //{
            //    g.FillRectangle(brush, new Rectangle(textArea.TextView.DrawingPosition.Left, 0, textArea.TextView.DrawingPosition.Right, this.Height));
            //}

            using (Pen HRulerPainterPen = new Pen(HRulerPainterColor.Color, 1.0f))
            {
                for (float x = DrawingPositionLeft; x < DrawingPositionWidth; x += textArea.TextView.WideSpaceWidth)
                {
                    int offset = (Height * 2) / 3;
                    if (num % 5 == 0)
                    {
                        offset = (Height * 4) / 5;
                    }

                    if (num % 10 == 0)
                    {
                        offset = Height / 2;
                    }
                    ++num;
                    g.DrawLine(HRulerPainterPen,
                               (int)x, Height - 4, (int)x, offset - 4);
                }

                g.DrawLine(new Pen(Color.FromArgb(100, HRulerPainterPen.Color), 1.0f) { DashStyle = DashStyle.Solid }, textArea.TextView.DrawingPosition.Left, Height - 1, textArea.TextView.DrawingPosition.Right, Height - 1);

            }                

        }

        //protected override void OnPaintBackground(System.Windows.Forms.PaintEventArgs e)
        //{
        //    HighlightColor HRulerPainterColor = textArea.Document.HighlightingStrategy.GetColorFor("HRuler");

        //    Brush brush = BrushRegistry.GetBrush(HRulerPainterColor.BackgroundColor);

        //    if (textArea.backgroundImage == null)
        //    {
        //        e.Graphics.FillRectangle(brush,
        //                                new Rectangle(0,
        //                                              0,
        //                                              Width,
        //                                              Height));
        //    }
        //    else
        //    {
        //        e.Graphics.FillRectangle(brush,
        //                               new Rectangle(0,
        //                                             0,
        //                                             Width,
        //                                             Height));
        //    }

        //    //brush.Dispose();

        //}

	}
}

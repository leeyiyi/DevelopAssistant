﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ICSharpCode.TextEditor.Document
{
    public class CustomLineColor
    {
        public string Tag { get; set; }
        public Color ForeColor { get; set; }
        public Color BackgroundColor { get; set; }

        public CustomLineColor(Color foreColor,Color backgroundColor,string tag)
        {
            this.Tag = tag;
            this.ForeColor = foreColor;
            this.BackgroundColor = backgroundColor;
        }

        public static CustomLineColor Create(Color backgroundColor)
        {
            return new CustomLineColor(Color.Empty, backgroundColor, "");
        }

    }
}

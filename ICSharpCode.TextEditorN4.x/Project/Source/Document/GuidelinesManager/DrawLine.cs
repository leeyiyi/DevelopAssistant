﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor.Document
{
    public class DrawLine
    {
        public Point StartPoint { get; set; }
        public Point EndPoint { get; set; }

        public TextLocation StartLocation { get; set; }
        public TextLocation EndLocation { get; set; }

        public DrawLine()
        {

        }

        public DrawLine(TextLocation StartLocation, TextLocation EndLocation)
        {
            this.StartLocation = StartLocation;
            this.EndLocation = EndLocation;
        }

        public void Draw(Graphics g, Point start, Point end, Pen pen)
        {
            g.DrawLine(pen, start, end);
        }
    }
}

﻿using System;
using System.Drawing;

namespace ICSharpCode.TextEditor.Document
{
    /// <summary>
    /// 辅助线,对齐线，准线
    /// </summary>
    public class Guideline
    {
        public int LineNumber { get; set; }
        public TextLocation Location { get; set; }        
    }
}

// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 1971 $</version>
// </file>

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

using ICSharpCode.TextEditor.Document;

namespace ICSharpCode.TextEditor.Document
{
	/// <summary>
	/// Determines folds for an xml string in the editor.
	/// </summary>
	public class HtmlFoldingStrategy : IFoldingStrategy
	{
		public HtmlFoldingStrategy()
		{
		}

		#region IFoldingStrategy

		/// <summary>
		/// Adds folds to the text editor around each start-end element pair.
		/// </summary>
		/// <remarks>
        /// <param>If the xml is not well formed then no folds are created.</param> 
		/// <para>Note that the xml text reader lines and positions start 		 
		/// </remarks>
		public List<FoldMarker> GenerateFoldMarkers(IDocument document, string fileName, object parseInformation)
		{
            return GenerateFoldMarkers(document);
        }

        /// <summary>
        /// Adds folds to the text editor around each start-end element pair.
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public List<FoldMarker> GenerateFoldMarkers(IDocument document)
        {
            List<FoldMarker> foldMarkers = new List<FoldMarker>();
            //需要分开
            int start = 0;           
            //stack 先进先出
            var startLines = new Stack<int>();
            var startTextLines = new Stack<string>();

            int start_script = 0;
            var start_Script_Lines = new Stack<int>();
            var start_Script_TextLines = new Stack<string>();

            // Create foldmarkers for the whole document, enumerate through every line.
            for (int i = 0; i < document.TotalNumberOfLines; i++)
            {

                char c;
                int offs, end = document.TextLength;
                LineSegment seg = document.GetLineSegment(i);

                for (offs = seg.Offset; offs < end && ((c = document.GetCharAt(offs)) == ' ' || c == '\t'); offs++)
                {
                    //offs 增加
                }
                if (offs == end)
                    break;
                int spaceCount = offs - seg.Offset;

                // Get the text of current line.
                string text = document.GetText(document.GetLineSegment(i));

                //region
                if (text.Trim().StartsWith("//region", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().StartsWith("//endregion", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = regionLabel.Replace("//region", "").Trim();
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</endregion>".Length, FoldType.Unspecified, regionText));
                    }                    
                }              


                //<html> 
                if (text.Trim().StartsWith("<html", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {                    
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().StartsWith("</html>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<html>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.Length, i, spaceCount + regionLabel.Length * 2 + "</html>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                //<head> 
                if (text.Trim().IndexOf("<header")==-1 &&  text.Trim().StartsWith("<head", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().StartsWith("</head>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<head>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</head>".Length, FoldType.Unspecified, regionText));
                    }                    
                }


                //<style></style>
                if (text.Trim().StartsWith("<style", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<style", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</style>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<style>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</style>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                //body
                if (text.Trim().StartsWith("<body", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().StartsWith("</body>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<body>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</body>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                // form
                if (text.Trim().IndexOf("</form>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<form", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<form", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</form>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<form>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</form>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                // table
                if (text.Trim().IndexOf("</table>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<table", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<table", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</table>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<table>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</table>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                // table tr
                if (text.Trim().IndexOf("</tr>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<tr", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<tr", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</tr>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<tr>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</tr>".Length, FoldType.Unspecified, regionText));
                    }                    
                }


                // table tr td
                if (text.Trim().IndexOf("</td>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<td", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<td", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</td>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<td>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</td>".Length, FoldType.Unspecified, regionText));
                    }
                }


                //select
                if (text.Trim().IndexOf("</select>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<select", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<select", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</select>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<select>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</select>".Length, FoldType.Unspecified, regionText));
                    }                    
                }


                // div
                if (text.Trim().IndexOf("</div>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<div", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<div", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</div>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<div>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</div>".Length, FoldType.Unspecified, regionText));
                    }                    
                }


                // ul
                if (text.Trim().IndexOf("</ul>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<ul", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<ul", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</ul>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<ul>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</ul>".Length, FoldType.Unspecified, regionText));
                    }                    
                }


                // li  
                if (text.Trim().IndexOf("<link", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().IndexOf("</li>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<li", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("</link>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().IndexOf("<li", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</li>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<li>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</li>".Length, FoldType.Unspecified, regionText));
                    }
                }


                //a  aside
                if (text.Trim().IndexOf("<aside", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().IndexOf("</a>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<a", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("</aside>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().IndexOf("<a", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</a>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<a>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</a>".Length, FoldType.Unspecified, regionText));
                    }
                }


                // section
                if (text.Trim().IndexOf("</section>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<section", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<section", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</section>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<section>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</section>".Length, FoldType.Unspecified, regionText));
                    }
                }

                //header
                if (text.Trim().IndexOf("</header>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<header", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<header", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</header>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<header>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</header>".Length, FoldType.Unspecified, regionText));
                    }
                }


                // footer
                if (text.Trim().IndexOf("</footer>", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("<footer", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<footer", StringComparison.CurrentCultureIgnoreCase) == -1 && text.Trim().StartsWith("</footer>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<footer>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</footer>".Length, FoldType.Unspecified, regionText));
                    }                    
                }

                //script                
                if (text.Trim().IndexOf("</script>") == -1 && text.Trim().StartsWith("<script", StringComparison.CurrentCultureIgnoreCase)) // Look for method starts
                {
                    startLines.Push(i);
                    startTextLines.Push(text);
                }
                if (text.Trim().IndexOf("<script") == -1 && text.Trim().StartsWith("</script>", StringComparison.CurrentCultureIgnoreCase)) // Look for method endings
                {
                    if (startLines.Count > 0)
                    {
                        start = startLines.Pop();
                        string regionLabel = startTextLines.Pop();
                        string regionText = "<script>";
                        foldMarkers.Add(new FoldMarker(document, start, document.GetLineSegment(start).Length - regionLabel.TrimStart().Length, i, spaceCount + regionLabel.Length * 2 + "</script>".Length, FoldType.Unspecified, regionText));
                    }                   
                }

                // { ... }
                if (text.Trim().StartsWith("{") || text.Trim().EndsWith("{")) // Look for method starts
                    if (text.Trim().StartsWith("{") || (text.Trim().IndexOf("}") == -1 && text.Trim().EndsWith("{"))) // Look for method starts
                    {
                        start_Script_Lines.Push(i);
                    }
                if (text.Trim().StartsWith("}")) // Look for method endings
                {
                    if (start_Script_Lines.Count > 0)
                    {
                        int start0 = start_Script_Lines.Pop();
                        foldMarkers.Add(new FoldMarker(document, start0, document.GetLineSegment(start0).Length - "{".Length, i, 57, FoldType.TypeBody, "{...}"));
                    }
                }                

            }            

            return foldMarkers;
        }

		#endregion		 
	}     

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ICSharpCode.WinFormsUI
{
    internal partial class OwnerButton : UserControl
    {
        private bool mouseon = false;

        private bool _onFocus;
        public bool OnFocus
        {
            get { return _onFocus; }
            set
            {
                _onFocus = value;
                this.Invalidate();
            }
        }

        private string _text;
        public new string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                this.Invalidate();
            }
        }

        private int _radius = 0;
        public int Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        private bool _useVisualStyleBackColor = false;
        public bool UseVisualStyleBackColor
        {
            get { return _useVisualStyleBackColor; }
            set { _useVisualStyleBackColor = value; }
        }

        private Color _borderColor = SystemColors.ControlDark;
        public Color BorderColor
        {
            get { return _borderColor; }
            set { _borderColor = value; }
        }

        public OwnerButton()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawRender(e.Graphics);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            mouseon = true;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            mouseon = false;
            this.Invalidate();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            this.OnFocus = true;
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            this.OnFocus = false;
        }

        private GraphicsPath GetRoundedRectPath(Rectangle rect, int radius)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(rect.X, rect.Y, radius * 2, radius * 2, 180, 90);
            roundedRect.AddLine(rect.X + radius, rect.Y, rect.Right - radius * 2, rect.Y);
            roundedRect.AddArc(rect.X + rect.Width - radius * 2, rect.Y, radius * 2, radius * 2, 270, 90);
            roundedRect.AddLine(rect.Right, rect.Y + radius * 2, rect.Right, rect.Y + rect.Height - radius * 2);
            roundedRect.AddArc(rect.X + rect.Width - radius * 2, rect.Y + rect.Height - radius * 2, radius * 2, radius * 2, 0, 90);
            roundedRect.AddLine(rect.Right - radius * 2, rect.Bottom, rect.X + radius * 2, rect.Bottom);
            roundedRect.AddArc(rect.X, rect.Bottom - radius * 2, radius * 2, radius * 2, 90, 90);
            roundedRect.AddLine(rect.X, rect.Bottom - radius * 2, rect.X, rect.Y + radius * 2);
            roundedRect.CloseFigure();
            return roundedRect;
        }

        protected void DrawRender(Graphics graphic)
        {
            Rectangle clientRection = new Rectangle();
            clientRection.X = 1;
            clientRection.Y = 1;
            clientRection.Width = this.ClientSize.Width - 3;
            clientRection.Height = this.ClientSize.Height - 3;

            Color backgroundColor = SystemColors.ControlLight;
            if (mouseon)
                backgroundColor = Color.FromArgb(255, 229, 241, 251);
            if (_onFocus)
                backgroundColor = Color.FromArgb(255, 229, 241, 251);

            if (this.Radius > 0)
            {
                using (GraphicsPath graphicPath = GetRoundedRectPath(clientRection, this._radius))
                {
                    graphic.FillPath(new SolidBrush(backgroundColor), graphicPath);
                }
            }
            else
            {
                graphic.FillRectangle(new SolidBrush(backgroundColor), clientRection);
            }

            if (!string.IsNullOrEmpty(this.Text))
            {
                graphic.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), this.ClientRectangle, new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
            }

            if (this.Radius > 0)
            {
                using (GraphicsPath graphicPath = GetRoundedRectPath(clientRection, this._radius))
                {
                    graphic.DrawPath(new Pen(_borderColor, 1.0f), graphicPath);
                }
            }
            else
            {
                ControlPaint.DrawBorder(graphic, clientRection,
            _borderColor, 1, ButtonBorderStyle.Solid, //左边
            _borderColor, 1, ButtonBorderStyle.Solid, //上边
            _borderColor, 1, ButtonBorderStyle.Solid, //右边
            _borderColor, 1, ButtonBorderStyle.Solid);//底边
            }

        }

    }
}

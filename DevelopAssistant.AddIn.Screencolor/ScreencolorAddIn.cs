﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;
namespace DevelopAssistant.AddIn.Screencolor
{
    using DevelopAssistant.AddIn.Screencolor.Properties;
    public class ScreencolorAddIn : WindowAddIn 
    {
        public ScreencolorAddIn()
        {
            this.IdentityID = "6fbfe158-38bd-43af-95f8-e610094deaa5";
            this.Name = "屏幕取色器V2.0";
            this.Text = "屏幕取色器";
            this.Tooltip = "屏幕取色器";            
            this.Icon = Resources.plugin;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((Form)Parameter[0], this)
            { 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}

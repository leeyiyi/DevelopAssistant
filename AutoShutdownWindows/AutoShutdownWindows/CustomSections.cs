﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoShutdownWindows
{
    public class CustomSections
    {
        public bool Useable { get; set; }

        public string StartTime { get; set; }

        public Shutdown Shutdown { get; set; }

        public System.Xml.XmlElement Node { get; set; }
    }

    public class Shutdown
    {
        public string Type { get; set; }
        public string Text { get; set; }
    }
}

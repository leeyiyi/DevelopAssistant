﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Drawing; 
using System.Windows.Forms;

namespace AutoShutdownWindows
{
    public partial class MainForm : Form
    {
        long interval = 0;

        configure config;

        public MainForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        public void InitializeControls()
        {
            dateTimePicker1.MaxDate = DateTime.Now.AddDays(7);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (btnApply.Text == "关机")
            {
                System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
                xmldoc.Load(config.Path);
                var node = xmldoc.SelectSingleNode("//customSection");
                ((System.Xml.XmlElement)node).SetAttribute("useable", "true");

                if (dateTimePicker1.Checked)
                {
                    System.Xml.XmlElement set = null;
                    set = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "date");
                    set.SetAttribute("useable", "true");
                    set.InnerText = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss");

                    System.Xml.XmlElement other = null;
                    other = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "time");
                    other.SetAttribute("useable", "false");

                    if (Math.Abs(dateTimePicker1.Value.Subtract(DateTime.Now).TotalMilliseconds) > 2 * 24 * 60 * 60 * 1000)
                    {
                        MessageBox.Show("请选择" + 2 + "天之内的时间");
                        return;
                    }

                    double value = dateTimePicker1.Value.Subtract(DateTime.Now).TotalMilliseconds;

                    if (value < 0)
                    {
                        MessageBox.Show("选择的时间必须大小当前时间");
                        return;
                    }

                    interval = uint.Parse(Math.Round(value, 0).ToString());
                }
                else
                {
                    System.Xml.XmlElement set = null;
                    set = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "time");
                    set.SetAttribute("useable", "true");
                    set.InnerText = ((int)(numericUpDown1.Value * 60 + numericUpDown2.Value) * 60 * 1000).ToString();

                    System.Xml.XmlElement other = null;
                    other = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "date");
                    other.SetAttribute("useable", "false");

                    interval = uint.Parse(set.InnerText);
                }

                if (interval > 0)
                    WindowsCore.Shutdown(false, (uint)interval);

                this.label5.Visible = true;
              

                string Hours = (interval / 1000 / 60 / 60).ToString("00");
                string Minutes = (interval % (1000 * 60 * 60) / 1000 / 60).ToString("00");
                string Seconds = (interval % (1000 * 60) / 1000).ToString("00");

                this.label6.Text = Hours + "小时" + Minutes + "分" + Seconds + "秒";
                    
                ((System.Xml.XmlElement)node).SetAttribute("starttime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                xmldoc.Save(config.Path);

                btnApply.Text = "取消关机";

                if (interval > timer1.Interval)
                {
                    this.timer1.Enabled = true;
                }

            }
            else
            {
                System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
                xmldoc.Load(config.Path);
                var node = xmldoc.SelectSingleNode("//customSection");
                ((System.Xml.XmlElement)node).SetAttribute("useable", "false");

                if (dateTimePicker1.Checked)
                {
                    System.Xml.XmlElement set = null;
                    set = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "date");
                    set.SetAttribute("useable", "true");
                    set.InnerText = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss");

                    System.Xml.XmlElement other = null;
                    other = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "time");
                    other.SetAttribute("useable", "false");

                    double value = dateTimePicker1.Value.Subtract(DateTime.Now).TotalMilliseconds;
  
                }
                else
                {
                    System.Xml.XmlElement set = null;
                    set = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "time");
                    set.SetAttribute("useable", "true");
                    set.InnerText = "0";

                    System.Xml.XmlElement other = null;
                    other = (System.Xml.XmlElement)configure.SelectSingleNode(node, "shutdown", "type", "date");
                    other.SetAttribute("useable", "false");
 
                }

                this.label5.Visible = false;
                this.label6.Text = "";

                WindowsCore.Shutdown(true, 0);

                ((System.Xml.XmlElement)node).SetAttribute("starttime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                xmldoc.Save(config.Path);

                btnApply.Text = "关机";

                this.timer1.Enabled = false;
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            config = configure.Load(Application.StartupPath + "\\AutoShutdownWindows.EXE.XML");
            string StartTime = config.CustomSections["customSection1"].StartTime;
            bool Useable = config.CustomSections["customSection1"].Useable;
            switch (config.CustomSections["customSection1"].Shutdown.Type)
            {
                case "date":
                    this.dateTimePicker1.Checked = true;
                    this.panel1.Enabled = false;
                    this.dateTimePicker1.Value = DateTime.Parse(config.CustomSections["customSection1"].Shutdown.Text);

                    interval = (long)this.dateTimePicker1.Value.Subtract(DateTime.Now).TotalMilliseconds;

                    break;
                case "time":

                    interval = long.Parse(config.CustomSections["customSection1"].Shutdown.Text);

                    long v1 = interval / (3600 * 1000);
                    long v2 = interval % (3600 * 1000) / (1000 * 60);
                    this.dateTimePicker1.Checked = false;
                    this.panel1.Enabled = true;
                    this.numericUpDown1.Value = v1;                   
                    this.numericUpDown2.Value = v2;

                    break;
            }
           
            interval = interval - (uint)DateTime.Now.Subtract(DateTime.Parse(StartTime)).TotalMilliseconds;

            if (Useable && interval > 0)
            {
                this.label5.Visible = true;
                this.label6.Text = interval / 1000 + " 秒";

                string Hours = (interval / 1000 / 60 / 60).ToString("00");
                string Minutes = (interval % (1000 * 60 * 60) / 1000 / 60).ToString("00");
                string Seconds = (interval % (1000 * 60) / 1000).ToString("00");

                this.label6.Text = Hours + "小时" + Minutes + "分" + Seconds + "秒";

                this.btnApply.Text = "取消关机";                
            }
            else
            {
                this.label5.Visible = false;
                this.label6.Text = "";
                this.btnApply.Text = "关机";   
            }

            if (Useable && interval > timer1.Interval)
            {
                this.timer1.Enabled = true;
            }

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Checked)
            {
                this.panel1.Enabled = false;
            }
            else
            {
                this.panel1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (interval > timer1.Interval)
            {
                interval = interval - (uint)timer1.Interval;

                string Hours = (interval / 1000 / 60 / 60).ToString("00");
                string Minutes = (interval % (1000 * 60 * 60) / 1000 / 60).ToString("00");
                string Seconds = (interval % (1000 * 60) / 1000).ToString("00");

                this.label6.Text = Hours + "小时" + Minutes + "分" + Seconds + "秒";
            }
           
        }


    }
}

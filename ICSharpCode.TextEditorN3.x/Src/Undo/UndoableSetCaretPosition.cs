﻿using ICSharpCode.TextEditor.Undo;
using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.TextEditor.Undo
{
    class UndoableSetCaretPosition : IUndoableOperation
    {
        UndoStack stack;
        TextLocation pos;
        TextLocation redoPos;

        public UndoableSetCaretPosition(UndoStack stack, TextLocation pos)
        {
            this.stack = stack;
            this.pos = pos;
        }

        public void Undo()
        {
            redoPos = stack.TextEditorControl.ActiveTextAreaControl.Caret.Position;
            stack.TextEditorControl.ActiveTextAreaControl.Caret.Position = pos;
        }

        public void Redo()
        {
            stack.TextEditorControl.ActiveTextAreaControl.Caret.Position = redoPos;
        }
    }
}

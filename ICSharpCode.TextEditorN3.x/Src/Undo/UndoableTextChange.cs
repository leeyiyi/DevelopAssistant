﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.TextEditor.Undo
{
    public class UndoableTextChange : IUndoableOperation 
    {

        ICSharpCode.TextEditor.Document.IDocument document;

        private string UndoText { set; get; }
        private string RedoText { set; get; }

        public UndoableTextChange(string Text, ICSharpCode.TextEditor.Document.IDocument Document)
        {
            this.UndoText = Document.TextContent;
            this.RedoText = Text;
            this.document = Document;
        }

        public void Undo()
        {
            document.UndoStack.AcceptChanges = false;
            document.TextContent = UndoText;
            //document.CommitUpdate();
            document.UndoStack.AcceptChanges = true;
        }

        public void Redo()
        {
            document.UndoStack.AcceptChanges = false;
            document.TextContent = RedoText;
            //document.CommitUpdate();
            document.UndoStack.AcceptChanges = true;
        }
    }
}

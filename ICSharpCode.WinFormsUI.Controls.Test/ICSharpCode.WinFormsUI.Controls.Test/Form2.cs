﻿using ICSharpCode.WinFormsUI.Controls.Chart3D;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls.Test
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            nChart3DControl1.RotateStyle = RotateStyle.XY;
            nChart3DControl1.ViewPort.Coordinate.FillingFace = true;
            nChart3DControl1.ViewPort.Coordinate.ScaleIsVisable = true;

            nChart3DControl1.ShowCursorValue = true;
            nChart3DControl1.CursorValueFormat = "{name}: ({0} HZ,{1} m/s^2,{2})";
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int len = 512; //16384; //8192; 262144
            double fs = 2560.0;
            double pi = 3.14159267;

            #region 制作输入信息

            Random rd = new Random();

            double[] wave1 = new double[len];
            for (int i = 0; i < len; i++)
            {
                wave1[i] = 1 * Math.Sin(2 * pi * 50 * (i + 1) / fs + pi / 6) + rd.NextDouble() +
                    3 * Math.Sin(2 * pi * 100 * (i + 1) / fs + pi / 4) + rd.NextDouble() +
                    3 * Math.Sin(2 * pi * 150 * (i + 1) / fs + pi / 3) + rd.NextDouble() +
                    3 * Math.Sin(2 * pi * 200 * (i + 1) / fs + pi / 2) + rd.NextDouble();
            }

            double[] wave2 = new double[len];
            for (int i = 0; i < len; i++)
            {
                wave2[i] = 1 * Math.Sin(2 * pi * 50 * (i + 1) / fs + pi / 4) + rd.NextDouble() +
                    1 * Math.Sin(2 * pi * 100 * (i + 1) / fs + pi / 6) + rd.NextDouble() +
                    3 * Math.Sin(2 * pi * 150 * (i + 1) / fs + pi / 3) + rd.NextDouble() +
                    3 * Math.Sin(2 * pi * 200 * (i + 1) / fs + pi / 2) + rd.NextDouble();
            }

            #endregion

            nChart3DControl1.BeginUpdate();

            SmoothCurve3D shape = null;
            Point3D[] points = null;

            for (int n = 0; n < 100; n++)
            {
                shape = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("图元" + n, new Shape3D() { LineWidth = 1.5f });
                //shape.DashStyleIsVisable = true;
                shape.DashStyleColor = Color.Black;
                shape.LineColor = Color.Black;

                points = new Point3D[wave1.Length];

                for (int i = 0; i < points.Length; i++)
                {
                    double x = i * 1.0 / 10;
                    double y = wave2[i];
                    points[i] = new Point3D((float)x, (float)y, n);
                }

                shape.Points = points;
            }

            nChart3DControl1.EndUpdate();

        }
    }
}

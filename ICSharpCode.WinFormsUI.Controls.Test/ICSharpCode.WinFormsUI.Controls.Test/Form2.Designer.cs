﻿namespace ICSharpCode.WinFormsUI.Controls.Test
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nChart3DControl1 = new ICSharpCode.WinFormsUI.Controls.Chart3D.NChart3DControl();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nChart3DControl1
            // 
            this.nChart3DControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nChart3DControl1.Location = new System.Drawing.Point(8, 6);
            this.nChart3DControl1.Name = "nChart3DControl1";
            this.nChart3DControl1.RotateStyle = ICSharpCode.WinFormsUI.Controls.Chart3D.RotateStyle.X;
            this.nChart3DControl1.ShowCursorValue = false;
            this.nChart3DControl1.Size = new System.Drawing.Size(382, 362);
            this.nChart3DControl1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(311, 378);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "应用";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 413);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nChart3DControl1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private Chart3D.NChart3DControl nChart3DControl1;
        private System.Windows.Forms.Button button1;
    }
}
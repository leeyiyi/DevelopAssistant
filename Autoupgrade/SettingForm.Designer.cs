﻿namespace Autoupgrade
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingForm));
            this.ToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUrl = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPath = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ToolBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.ToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.ToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.ToolBar.Controls.Add(this.btnCancel);
            this.ToolBar.Controls.Add(this.btnApply);
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ToolBar.Location = new System.Drawing.Point(6, 140);
            this.ToolBar.MarginWidth = 2;
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(288, 60);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancel.FouseColor = System.Drawing.Color.White;
            this.btnCancel.Foused = false;
            this.btnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancel.Icon = null;
            this.btnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(201, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Radius = 0;
            this.btnCancel.Size = new System.Drawing.Size(75, 36);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UnableIcon = null;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(118, 7);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(75, 36);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "确定";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "地址：";
            // 
            // txtUrl
            // 
            this.txtUrl.BackColor = System.Drawing.Color.White;
            this.txtUrl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUrl.Enabled = true;
            this.txtUrl.FousedColor = System.Drawing.Color.Orange;
            this.txtUrl.Icon = null;
            this.txtUrl.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUrl.IsButtonTextBox = false;
            this.txtUrl.IsClearTextBox = false;
            this.txtUrl.IsPasswordTextBox = false;
            this.txtUrl.Location = new System.Drawing.Point(73, 8);
            this.txtUrl.MaxLength = 32767;
            this.txtUrl.Multiline = false;
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.PasswordChar = '\0';
            this.txtUrl.Placeholder = null;
            this.txtUrl.ReadOnly = false;
            this.txtUrl.Size = new System.Drawing.Size(159, 24);
            this.txtUrl.TabIndex = 2;
            this.txtUrl.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "名称：";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.Enabled = true;
            this.txtName.FousedColor = System.Drawing.Color.Orange;
            this.txtName.Icon = null;
            this.txtName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtName.IsButtonTextBox = false;
            this.txtName.IsClearTextBox = false;
            this.txtName.IsPasswordTextBox = false;
            this.txtName.Location = new System.Drawing.Point(73, 38);
            this.txtName.MaxLength = 32767;
            this.txtName.Multiline = false;
            this.txtName.Name = "txtName";
            this.txtName.PasswordChar = '\0';
            this.txtName.Placeholder = null;
            this.txtName.ReadOnly = false;
            this.txtName.Size = new System.Drawing.Size(124, 24);
            this.txtName.TabIndex = 4;
            this.txtName.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "启动文件：";
            // 
            // txtPath
            // 
            this.txtPath.BackColor = System.Drawing.Color.White;
            this.txtPath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPath.Enabled = true;
            this.txtPath.FousedColor = System.Drawing.Color.Orange;
            this.txtPath.Icon = global::Autoupgrade.Properties.Resources.add_16px;
            this.txtPath.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Right;
            this.txtPath.IsButtonTextBox = true;
            this.txtPath.IsClearTextBox = false;
            this.txtPath.IsPasswordTextBox = false;
            this.txtPath.Location = new System.Drawing.Point(73, 68);
            this.txtPath.MaxLength = 32767;
            this.txtPath.Multiline = false;
            this.txtPath.Name = "txtPath";
            this.txtPath.PasswordChar = '\0';
            this.txtPath.Placeholder = null;
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(159, 24);
            this.txtPath.TabIndex = 6;
            this.txtPath.UseSystemPasswordChar = false;
            this.txtPath.Click += new System.EventHandler(this.txtPath_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPath);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtUrl);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(30, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 100);
            this.panel1.TabIndex = 7;
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 206);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "设置";
            this.Load += new System.EventHandler(this.SettingForm_Load);
            this.ToolBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel ToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancel;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUrl;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtName;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPath;
        private System.Windows.Forms.Panel panel1;

    }
}